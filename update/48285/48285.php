<?php
use skewer\components\config\PatchPrototype;

class PatchInstall extends PatchPrototype {

    public $sDescription = 'Активация компрессии файлов';

    public $bUpdateCache = false;

    public function execute() {

        $this->addParameter(3, 'compression', 1);

    }

}
