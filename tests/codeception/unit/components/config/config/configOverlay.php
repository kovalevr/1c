<?php

return array(
    'test_val_123' => 456,
    'test_val_222' => 222,
    'test_arr' => array(
        'b' => 45,
        'c' => 456
    )
);
