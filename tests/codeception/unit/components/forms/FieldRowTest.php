<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.12.2016
 * Time: 15:55
 */

namespace unit\components\forms;


use skewer\components\forms\FieldRow;


class FieldRowTest extends \PHPUnit_Framework_TestCase {

    public function providerShowDefaultPlaceholder() {
        return [
            ['placeholder="Naim"', 'left', false],
            ['placeholder="Naim"', 'none', false],
            ['placeholder="Org" data-hide-placeholder="1"', 'left', false],
            ['placeholder="Org" data-hide-placeholder="1"', 'none', false],
            ['class="placeholder"', 'none', true],
            ['class="simple"', 'none', true],
            ['class="placeholder"', 'left', false],
            ['class="simple"', 'left', false],
            ['', 'left', false],
            ['', 'none', true],
            ['data-hide-placeholder="1"', 'none', true],
            ['data-hide-placeholder="1"', 'left', false],
        ];
    }

    /**
     * @dataProvider providerShowDefaultPlaceholder
     */
    public function testShowDefaultPlaceholder($sParams, $sPos, $bRes) {
        $o = new FieldRow();
        $o->label_position = $sPos;
        $o->param_man_params = $sParams;

        $this->assertSame($bRes, $o->showDefaultPlaceholder(),
            "not [".($bRes?'true':'false')."] on: $sPos, $sParams");
    }

}
