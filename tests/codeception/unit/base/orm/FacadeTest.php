<?php

namespace unit\base\orm;

use skewer\base\orm\Query;

/**
 * Тест на фасад
 */

class FacadeTest extends \PHPUnit_Framework_TestCase {

    public function testQuery() {

        $mRes = Query::SQL('SHOW TABLES');
        $this->assertNotEmpty( $mRes->fetchArray() );

    }

}
 