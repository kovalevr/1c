<?php

namespace skewer\controllers;

use skewer\base\router\Router;
use skewer\base\section;
use skewer\base\section\Parameters;
use skewer\base\site\Site;
use skewer\base\SysVar;
use skewer\build\Design\Zones\Api;
use skewer\build\Page\Main;
use skewer\components\auth\Auth;
use skewer\components\auth\CurrentUser;
use skewer\components\design\Design;
use skewer\base\site_module;
use skewer\components\seo;
use skewer\helpers\Adaptive;
use \yii\helpers\ArrayHelper;
use skewer\base\section\Page;
use skewer\base\section\Tree;
use skewer\base\section\models\TreeSection;
use yii\web\ServerErrorHttpException;

/**
 * Контроллер лицавой части сайта
 */
class SiteController extends Prototype  {

    /**
     * Дополнителные действия
     * @return array
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    /**
     * отдает robots.txt
     */
    public function actionRobots(){

        $host = $_SERVER['HTTP_HOST'];
        $patterns = array();

        $patterns[] = '/^[\S]*sk[\d]{3}.ru$/';
        $patterns[] = '/^[\S]*sktest.ru$/';
        $patterns[] = '/^[\S]*twinslab.ru$/';

        $bDenyAll = false;

        foreach($patterns as $pattern){
            if (preg_match($pattern,$host)){
                // запретим всё
                $bDenyAll = true;
            }
        }

        $testDomain = \Yii::$app->getParam('test_domains');

        if ($testDomain && is_array($testDomain)){
            if (array_search($host, $testDomain)!==false){
                $bDenyAll = true;
            };
        }

        header("Content-Type: text/plain");

        // если домен тестовый или файла robots.txt нет в корне, то доступ запрещаем
        if ($bDenyAll || !file_exists(WEBPATH.'robots.txt')){
            exit("User-agent: *\nDisallow: /");
        } else {
            exit(file_get_contents(WEBPATH.'robots.txt'));
        }

    }

    /**
     * Отдает sys.php
     */
    public function actionSys(){

        \skewer\components\redirect\Api::execute();

        if (USECLUSTERBUILD){
            /*Неотцепленный*/
            $sScriptPath = RELEASEPATH.'components/tokensAuth/sys.php';
        } else {
            /*Отцепленный*/
            $sScriptPath = ROOTPATH.'skewer/components/tokensAuth/sys.php';
        }

        if (file_exists($sScriptPath)){
                $_SERVER['SCRIPT_NAME'] = '/sys.php';
                include_once $sScriptPath;
                exit;
        }
    }

    /**
     * Лицевая страница сайта
     * @return string
     * @throws ServerErrorHttpException
     * @throws \Exception
     */
    public function actionIndex() {

        \skewer\components\redirect\Api::execute();

        $this->addHelpers();

        /* Определяем стартовый раздел */
        $mainSection = Auth::getMainSection();

        \Yii::$app->router->sectionId = \Yii::$app->router->getSection($mainSection);

        if(!\Yii::$app->router->sectionId)
            \Yii::$app->router->setPage(page404);

        // блок проверки на закрытие остраницы т индексации (возможно должен находиться не здесь)
        $page = TreeSection::findOne(\Yii::$app->router->sectionId);
        if ( $page ) {
            if ( $page->visible == section\Visible::HIDDEN_NO_INDEX ) {
                \Yii::$app->getResponse()->redirect('/', '301')->send();
            }
        }

        /** @var $oPage site_module\Process */
        $oPage = null;

        /** Сперва определяем язык, а затем нужные параметры */
        $this->initLang();

        $this->initParameters();

        \Yii::$app->on('reload_page_id', [$this, 'initParameters']);

        /** Редирект после языков! */
        $this->checkSiteRedirect();

        $iCnt = 0;

        do {

            // Проверяем доступ к разделу
            if ( !CurrentUser::canRead(\Yii::$app->router->sectionId) )
                \Yii::$app->router->setPage(pageAuth);

            \Yii::$app->processList->removeProcess('out');

            $oPage = $this->initRootProcess();

            $iStatus = $this->executeRootProcess();

            if ( ++$iCnt>10 )
                throw new ServerErrorHttpException( 'Infinite loop in page id determination'.
                    \Yii::$app->processList->getCurrentStateText() );

        } while($iStatus == psExit);

        if (SysVar::get('site.enableLastModifiedHeader')){

            // Дата модификации сайта
            \Yii::$app->router->setLastModifiedDate(SysVar::get('site.last_modified_date', time()));

            // Даты модификации текущего раздела, его родителей и его шаблона
            $iTemplateId = (int)Parameters::getValByName(\Yii::$app->router->sectionId, Parameters::settings, 'template');
            $aSections = Tree::getSections([$iTemplateId, \Yii::$app->router->sectionId ]);
            \Yii::$app->router->setLastModifiedDate( $aSections + Tree::getSections(Tree::getSectionParents(\Yii::$app->router->sectionId)));

            // Для исключенных от индексирования разделов отдаём текущую дату
            $aNoIndexedSection = seo\Data::find()->where('none_index',1)->asArray()->get();
            if (in_array(\Yii::$app->router->sectionId,ArrayHelper::getColumn($aNoIndexedSection,'row_id')))
                \Yii::$app->router->setLastModifiedDate(time());


            \Yii::$app->router->sendHeaderLastModified();
        }
        
        $oPage->render();

        return \Yii::$app->router->modifyOut($oPage->getOuterText());
    }


    /**
     * Инициализировать корневой процесс
     * @throws ServerErrorHttpException
     * @return site_module\Process
     */
    private function initRootProcess() {

        $aParams = Page::getByGroup( Parameters::settings );

        $aParams = ArrayHelper::map($aParams, 'name', 'value');
        if ( isset($aParams['object']) and $aParams['object'] )
            $sClassName = $aParams['object'];
        else
            throw new ServerErrorHttpException( sprintf(
                'No root module found for section [%d]',
                \Yii::$app->router->sectionId
            ) );

        $aParams['_params'] = $aParams;
        $aParams['sectionId'] = \Yii::$app->router->sectionId;

        $oProcess = \Yii::$app->processList->addProcess( new site_module\Context('out', $sClassName, ctModule, $aParams) );

        if ( !$oProcess )
            throw new ServerErrorHttpException(
                sprintf('Root process [%s] not inited for section [%d]'),
                $sClassName,
                \Yii::$app->router->sectionId
            );

        return $oProcess;
    }

    /**
     * Выполнить коневой процесс
     * @return bool|int
     */
    private function executeRootProcess() {

        $iStatus = \Yii::$app->processList->executeProcessList();

       if ( $this->check404() ){
           \Yii::$app->router->setPage( page404 );
           $iStatus = psExit;
       }

        return $iStatus;

    }

    /**
     * Инициализация языков
     * @return void
     */
    protected function initLanguage(){
        /** Перекрываем родительский метод. Инициализация будет проведена позже в initLang */
    }

    /**
     * Инициализация языков
     */
    private function initLang(){
        $sLanguage = Parameters::getLanguage(\Yii::$app->router->sectionId);
        \Yii::$app->language = ($sLanguage)?:\Yii::$app->language;
    }

    /**
     * Инициализация параметров
     * @return void
     */
    public function initParameters(){

        // Пересобираем кэш параметров страницы
        Page::init(\Yii::$app->router->sectionId);

        // восстанавливаем дефолтное состояние страницы
        \Yii::$app->router->setStatePage( Api::DEFAULT_LAYOUT );

        // Пересобираем зоны
        $oRootProcess = \skewer\base\site\Page::getRootModule();

        if ( $oRootProcess instanceof site_module\page\ModulePrototype ){
            /** @var Main\Module $oRootModule */
            $oRootModule = $oRootProcess->getModule();
            $oRootModule->initParameters();
        }

    }


    /**
     * Проверка на редиректы раздела
     */
    private function checkSiteRedirect(){

        $oSection = Tree::getSection( \Yii::$app->router->sectionId );

        if (in_array(\Yii::$app->router->sectionId, [\Yii::$app->sections->getValue(Page::LANG_ROOT), \Yii::$app->sections->getValue('main')]) && \Yii::$app->router->getURLTail() != '')
            return;

        if ( $oSection ) {
            if ( $oSection->link ) {
                if ( preg_match('/^\[\d+\]$/', $oSection->link) )
                    $sRedirectUrl = \Yii::$app->router->rewriteURL( $oSection->link );
                else
                    $sRedirectUrl = $oSection->link;
                if ( $sRedirectUrl !== $_SERVER['REQUEST_URI'] ) {
                    \Yii::$app->getResponse()->redirect($sRedirectUrl,'301')->send();
                }
            }
        }
    }

    /**
     * Добавление хелперов для парсера
     */
    private function addHelpers()
    {
        /* Добавялем класс Design для доступа в шаблонах */
        $oDesign = new Design();
        site_module\Parser::setParserHelper($oDesign, 'Design');

        $oAdaptive = new Adaptive();
        site_module\Parser::setParserHelper($oAdaptive, 'Adaptive');

        $oSite = new Site();
        site_module\Parser::setParserHelper($oSite, 'Site');
    }


    /**
     * Метод проверяет нужно ли выкинуть 404ю
     * @return bool true - нужно, false - нет
     */
    protected function check404(){

        if ( \Yii::$app->router->sectionId == \Yii::$app->sections->main() ){

            // Неразобранный остаток урл
            $sTail = \Yii::$app->router->getURLTail();

            // Флаг, указывающий на непустой остаток урл
            $bTailExist = (bool)$sTail;

            if ( $bTailExist ) {

                // Выводимые метки
                $aActiveLabels = Main\Module::getShowLabels();

                /** @var site_module\Process $oProcess */
                foreach (\Yii::$app->processList->aProcessesPaths as $oProcess) {

                    // Пропускаем метки которые не выводятся
                    if ( !in_array($oProcess->getLabel(), $aActiveLabels) )
                        continue;

                    $oRouter = new Router( $sTail, $_GET );

                    $aDecodedRules = Router::getRulesExclusionTails4MainPage( $oProcess->getModuleClass(), $oProcess->getModule()->getBaseActionName() );

                    $oRouter->getParams( $aDecodedRules );

                    if ( !$oRouter->getURLTail() ){

                        $bTailExist = false;
                        break;
                    }

                }

            }

            if ( $bTailExist )
                return true;

        } else {

            // Если урл на внутренних страницах разобран не полностью
            if ( !\Yii::$app->router->getUrlParsed())
                return true;

        }

        return false;

    }

}
