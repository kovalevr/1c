<?php

namespace skewer\modules\rest\controllers;

use skewer\base\SysVar;
use skewer\components\auth\Auth;
use skewer\components\catalog\Card;
use skewer\components\catalog\GoodsSelector;
use skewer\components\search\Selector;
use skewer\components\search\Type;
use yii\helpers\ArrayHelper;


/**
 * Каталожный поиск
 * Class SearchController
 * @package skewer\modules\rest\controllers
 */
class SearchController extends PrototypeController {

	/** @var string Набор полей для выдачи списка товара (без пробелов) */
	private static $sListFields = 'id,title,article,price,currency,announce,gallery';
	/** @var int Каталожный поиск */
	private $catalogType = 2;

	const MAX_VALUE = 500;
	const MIN_VALUE = 20;

	public function actionIndex() {

		$iPage = (int)\Yii::$app->request->get( 'page', 1 );
		$sSearchText = \Yii::$app->request->get( 'search_text' );
		$iOnPage = (int)\Yii::$app->request->get( 'on_page', 10 );
		$search_type = (int)\Yii::$app->request->get( 'search_type', -1 );

		if ($iOnPage > self::MAX_VALUE)
			$iOnPage = self::MAX_VALUE;
		elseif ($iOnPage <= 0)
			$iOnPage = self::MIN_VALUE;

		if ($search_type < 0 || $search_type > 2)
			$search_type = (int)SysVar::get('Search.default_type');

		if ( !empty( $sSearchText ) ) {

			/* Если есть запрещенные политикой разделы - исключаем из выборки */
			$aDenySections = ( $res = Auth::getDenySections('public') ) ? $res : array();

			/** Делаем выборку из поисковой таблицы */
			$aItems = Selector::create()
				->searchText( $sSearchText )
				->limit( $iOnPage, $iPage)
				->type( Type::inCatalog )
				->searchType( $search_type )
				->type( $this->catalogType )
				->denySection( $aDenySections )
				->subsections( false )
				->find();


			// ! Постраничник должен устанавливаться для списков даже при пустой выборке
			$iTotalCount = $aItems['count'];
			$this->setPagination($iTotalCount, ceil($iTotalCount / $iOnPage), $iPage, $iOnPage);

			if ( is_array($aItems) && isSet($aItems['count']) && $aItems['count'] and isset($aItems['items']) ) {

				/** Делаем выборка из каталога */
				$aObjects = ArrayHelper::map($aItems['items'], 'object_id', 'object_id');
				$aResult = $this->getGoods($aObjects);
				if ($aResult) {
					return $aResult;
				}
			}
		}

		return [];
	}//func

	/**
	 * Получениу товаров
	 * @param array $aObjects
	 * @return array|bool
	 */
	private function getGoods( $aObjects = array() ){

		$aGoods = GoodsSelector::getList( Card::DEF_BASE_CARD, false )
			->condition( 'id IN ?',$aObjects )
			// @todo Возможен случай, если товар уже неактивен, но в поиске еще присутствует. На странице будет меньше товаров!
			->condition( 'active', 1 )
			->parse();

		if (!$aGoods){
			return [];
		}

		$aGoods = ArrayHelper::index( $aGoods, 'id' );
		$aResult = array();
//		/** Сортировка в соответствии с результатами поиска */
		foreach ( $aObjects as $iObjectId ){
			if ( isSet($aGoods[$iObjectId]) ) {
				$aResult[] = $aGoods[$iObjectId];
				unset($aGoods[$iObjectId]);
			}
		}
		$aFields = array_flip(explode(',', self::$sListFields));

		$aGoodsOut = [];
		foreach ($aResult  as &$paData) {
			$paGood = &$aGoodsOut[];
			CatalogController::parseFields($paGood = $aFields, $paData, true);
		}

		return $aGoodsOut;
	}//func

}//class