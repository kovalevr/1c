<?php

namespace skewer\modules\rest\controllers;

use skewer\components\catalog\GoodsSelector;
use yii\helpers\ArrayHelper;
use skewer\components\catalog\Card;

/**
 * Работа с каталогом через rest
 * Class CatalogController
 * @package skewer\modules\rest\controllers
 */
class CatalogController extends PrototypeController {

    /** @var string Набор полей для выдачи детальной информации товара (без пробелов) */
    private static $sDetailFields = 'id,title,article,section,price,currency,announce,obj_description,gallery';

    /** @var string Набор полей для выдачи списка товара (без пробелов) */
    private static $sListFields = 'id,title,article,price,currency,announce,gallery';

    /** @var bool $bFormatted Форматировать значения полей? @todo В след. версиях по соглосованию этот параметр должен быть включён всегда и убрать его из GET */
    private static $bFormatted = false;

    public function actionView( $id ) {

        // добавочные поля
        $sAddFields = trim(\Yii::$app->request->get('add_fields'), ',');
        self::$bFormatted = (bool)\Yii::$app->request->get('formatted');

        $aGood = GoodsSelector::get($id, Card::DEF_BASE_CARD, true);
        if (!$aGood) return '';

        // Если = * то получить все поля каталожного объекта
        if ($sAddFields)
            self::$sDetailFields .= ($sAddFields == '*') ? "," . join(',', array_keys($aGood['fields'])) : ",$sAddFields";

        $aFields = array_flip(explode(',', self::$sDetailFields));

        self::parseFields($aFields, $aGood);

        return $aFields;
    }

    public function actionIndex() {
        // (!) При успешной обработке запроса, но при отсутвии позиций, списковые методы должны отдавать пустой массив, а не строку
        // (!) Заголовки постраничника нужно отдавать всегда при успешной обработке запроса

        $sSortField = \Yii::$app->request->get('sort');

        $iPage   = abs( (int)\Yii::$app->request->get('page', 0) ) ?: 1;
        $iOnPage = abs( (int)\Yii::$app->request->get('onpage', 0) ) ?: 0;
        $iOnPage or $iOnPage = abs( (int)\Yii::$app->request->get('per-page', 0) ) ?: 10; // @todo фильтр per-page выпилить после обновления мобильного приложения. Оставить onpage
        ($iOnPage <= 100) or ($iOnPage = 100); // Ограничить до 100 позиций на одной странице

        $iSection     = \Yii::$app->request->get('section', 0);
        $sTitleFilter = \Yii::$app->request->get('title', '');
        $sFieldFilter = \Yii::$app->request->get('show-type', '');

        // добавочные поля (если = *, то получить все поля объекта)
        $sAddFields       = trim(\Yii::$app->request->get('add_fields'), ',');
        self::$bFormatted = (bool)\Yii::$app->request->get('formatted');

        if ($iPage < 1) $iPage = 1;

        $sSortWay = 'up';
        if ( strpos($sSortField, '-') === 0 ) {
            $sSortWay = 'down';
            $sSortField = substr($sSortField, 1);
        }

        $oQuery = ($iSection) ? GoodsSelector::getList4Section($iSection) : GoodsSelector::getList();
        $oQuery->parseAllActiveFields()->condition('active', true);
        $sTitleFilter and $oQuery->condition( 'title LIKE ?', '%' . $sTitleFilter . '%' );
        $sFieldFilter and $oQuery->condition( $sFieldFilter );

        if (in_array($sSortField, ['price','title']))
            $oQuery->sort($sSortField, ($sSortWay == 'down') ? 'DESC' : 'ASC');

        $iTotalCount = 0;
        $oQuery->limit($iOnPage, $iPage, $iTotalCount);

        // ! Постраничник должен устанавливаться для списков даже при пустой выборке
        $this->setPagination($iTotalCount, ceil($iTotalCount / $iOnPage), $iPage, $iOnPage);

        $aGoods = $oQuery->parse();
        if (!$aGoods) return [];

        if ($sAddFields)
            self::$sListFields .= ($sAddFields == '*') ? "," . join(',', array_keys($aGoods[0]['fields'])) : ",$sAddFields";

        $aFields = array_flip(explode(',', self::$sListFields));

        $aGoodsOut = [];

        foreach ($aGoods  as &$paData) {
            $paGood = &$aGoodsOut[];
            self::parseFields($paGood = $aFields, $paData, true);
        }

        return $aGoodsOut;
    }

    /**
     * Обработать экспортируемые поля
     * @param array &$aFields Набор полей
     * @param array &$aData Значения полей
     * @param boolean $bFirstImg Брать только первое изображение галереи
     */
    public static function parseFields(&$aFields, &$aData, $bFirstImg = false) {

        foreach ($aFields as $sField => &$mVal) {

            /** Оригинальное значение без форматирования @todo Упростить алгоритм его получения как исчезнет GET-параметр formatted в след. версиях */
            $mOriginVal = '';

            switch ($sField) {
                case 'section':
                    $sField = 'main_section';
                case 'id':
                    $mVal = $aData[$sField];
                    break;

                case 'currency':
                    $mVal = ArrayHelper::getValue($aData, 'fields.price.attrs.measure', '');
                    break;

                default:
                    if (isset($aData['fields'][ $sField ]['type']) and ($aData['fields'][ $sField ]['type'] == 'gallery'))
                        $mVal = $bFirstImg ? ArrayHelper::getValue($aData, "fields.$sField.gallery.images.0.images_data", '') : ArrayHelper::getValue($aData, "fields.$sField.gallery.images", '');
                    else {
                        $mVal       = (self::$bFormatted) ? ArrayHelper::getValue($aData, "fields.$sField.html", '') : ArrayHelper::getValue($aData, "fields.$sField.value", '');
                        $mOriginVal = ArrayHelper::getValue($aData, "fields.$sField.value", '');
                    }
            }

            if (self::$bFormatted)
                $mVal = [
                    'title'  => ArrayHelper::getValue($aData, "fields.$sField.title", '') ?: $sField,
                    'value'  => ($mVal) ? (is_array($mVal) ? $mVal : "$mVal") : '', // Здесь всегда должна возвращаться либо строка либо массив. Не числовой тип.
                    'origin' => $mOriginVal ?: $mVal,
                ];
        }
    }
}