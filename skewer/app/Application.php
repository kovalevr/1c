<?php

namespace skewer\app;

use skewer\components\auth\Auth;
use skewer\components\i18n;

use skewer\base\log\Logger;
use skewer\base\SysVar;
use skewer\base\site_module\Request;
use skewer\base\Twig;
use yii\helpers\ArrayHelper;

require_once 'CacheDropTrait.php';

/**
 * Основной класс приложения в замен стандартному для проброса имен переменных в IDE
 *
 * @property i18n\I18N $i18n the internationalization application component
 * @property i18n\SectionsPrototype $sections component to a service section
 * @property \skewer\components\config\BuildRegistry $register registry of installed modules and parts
 * @property \skewer\base\site_module\Environment $environment набор переменных среды для передачи между модулями
 * @property \skewer\base\site_module\ProcessList $processList список процессов
 * @property \skewer\base\router\Router $router менеджер для работы с url (временный, потом будет urlManager)
 * @property \skewer\base\site_module\JsonResponse $jsonResponse менеджер для работы с json посылками (временный)
 * @property \skewer\components\seo\Manager $seo менеджер для работы с SEO
 * @method i18n\I18N getI18n Returns the internationalization application component
 *
 */
class Application extends \yii\web\Application {
    use CacheDropTrait;

    /**
     * @inheritDoc
     */
    public $layout = false;

    /**
     * @inheritDoc
     */
    public function init() {

        self::defineWebProtocol();

        \Yii::$app->session->open();
        parent::init();
        $this->setViewPath('@skewer/views');
    }

    /**
     * @inheritdoc
     */
    public function handleRequest($request) {

        mb_internal_encoding("UTF-8");

        /** Регистрация skewer-автолоадера */
        $this->skAutoloaderInit();

        /** Инициализация языка */
        $this->languageInit();

        /** Редиректы */
        $this->redirect();

        /** Инициализация парсеров */
        $this->parserInit();

        /** Инициализация политик */
        $this->authInit();

        /** Инициализация Request */
        $this->skRequestInit();

        /** Инициализация логгера */
        $this->loggerInit();

        return parent::handleRequest($request);
    }

    /**
     * @return i18n\SectionsPrototype $sections component to a service section
     */
    public function getSections(){
        return $this->getComponents('section')[0];
    }

    /**
     * Редиректы
     */
    private function redirect()
    {
        // #stb_note можно взять из вызывавшего метода
        $request = \Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $response = $this->getResponse();

        // Редирект на https
        if (SysVar::get('Redirect.toHttps') and strcasecmp(WEBPROTOCOL, 'https://') !== 0 ) {
            $response->redirect('https://' . WEBROOTPATH . ltrim($request->getUrl(), '/'), 301)->send();
            die();
        }

        $sUrl = $request->getUrl();

        $sRequestUri = explode("?", $sUrl)[0];

        // #42836
        // Тут можно было бы использовать \Yii::$app->urlManager->scriptUrl для получения
        // имени скрипта, но площадка часто настроена на выкладку в директорию выше web,
        // из-за чего получаем /web/index.php. Надо либо на уровне инициализации отрезать
        // web папку или здесь убирать её
        // + редирект тут делать не нужно. Это должно быть настроено в конфиге параметром showScriptName
        // !!!! showScriptName - при false убирает имя скрипта при вызове createUrl т.е. редирект нужен

        $sEntryScript = 'index.php';

        if(strpos( ltrim($sUrl, '/'), $sEntryScript) === 0){
            $iPos = strpos($sUrl, $sEntryScript) + strlen($sEntryScript);
            $sRedirectUrl = substr($sUrl,$iPos);
            $sRedirectUrl = '/' . ltrim($sRedirectUrl, '/');

            \Yii::$app->getResponse()->redirect($sRedirectUrl, '301')->send();
        }

        // для файлов с расширениями для роутера нужно убирать суффикс
        preg_match('/^(.*)\.(.*)$/', $sRequestUri, $return);

        if ($return) {
            \Yii::$app->getUrlManager()->suffix = '';
            return;
        } else {

            /** @var string $sRawUrl Необработанный урл*/
            $sRawUrl = $sRequestUri;

            /** @var string $sCultivatedUrl  Обработанный урл */
            $sCultivatedUrl = $sRawUrl . '/';
            $sCultivatedUrl = preg_replace('/\/{2,}/','/',$sCultivatedUrl);

            if ($sCultivatedUrl !== $sRawUrl){
                $sRedirectUrl = empty($params)? $sCultivatedUrl : $sCultivatedUrl . '?' . $request->getQueryString();
                $response->redirect($sRedirectUrl,301)->send();
                exit;
            }

        }
    }

    /**
     * Отдает значенеие параметра конфигурации по заданному адресу
     * Метод работает по принципу ArrayHelper::getValue
     * @param string|array $sPath
     * @param mixed|null $sDefault
     * @return mixed
     */
    public function getParam( $sPath, $sDefault=null ) {
        return ArrayHelper::getValue( \Yii::$app->params, $sPath, $sDefault );
    }

    private function skAutoloaderInit()
    {
        require_once RELEASEPATH . 'base/Autoloader.php';
        \skewer\base\Autoloader::init();
    }

    private function languageInit()
    {
        /** Язык по-умолчанию */
        if (!is_null(SysVar::get('language'))) {
            $this->language = SysVar::get('language');
        }
    }

    private function parserInit()
    {
        Twig::Load(
            array(),
            \Yii::$app->getParam(['cache','rootPath']) . 'Twig/',
            \Yii::$app->getParam(['debug','parser']) or YII_DEBUG or YII_ENV_DEV
        );
    }

    private function authInit()
    {
        Auth::init();
    }

    private function skRequestInit()
    {
        Request::init();
    }

    private function loggerInit()
    {
        Logger::init(ROOTPATH . 'log/access.log');
    }

    /**
     * Определяет константу WEBPROTOCOL в зависимости от порта с которого пришли
     * или SysVar enableHTTPS
     */
    public static function defineWebProtocol()
    {
        // Протокол доступа к сайту: http:// или https://
        if (!defined('WEBPROTOCOL'))
            define('WEBPROTOCOL', ((isset($_SERVER['SERVER_PORT']) and ($_SERVER['SERVER_PORT'] == '443')) or (\skewer\base\SysVar::get('enableHTTPS'))) ? 'https://' : 'http://');
    }

}
