<?php

namespace skewer\components\One;

use skewer\base\ft\Cache;
use skewer\base\orm\Query;
use skewer\base\SysVar;
use skewer\build\Adm\Order\Api;
use skewer\build\Adm\Order\ar\Goods;
use skewer\build\Adm\Order\ar\Order;
use skewer\build\Adm\Order\ar\OrderRow;
use skewer\build\Adm\Order\ar\TypePayment;
use skewer\build\Adm\Order\model\ChangeStatus;
use skewer\build\Adm\Order\model\Status;
use skewer\build\Adm\Order\Service;
use skewer\components\catalog\Card;
use skewer\components\import\provider\XmlReader;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * Class ExchangeSales
 * @link http://v8.1c.ru/edi/edi_stnd/131/ - Протокол обмена между системой "1С:Предприятие" и сайтом
 * @package skewer\components\One
 */
class ExchangeSales extends ExchangePrototype{

    /** Приём файла с заказами от 1с */
    protected function cmdFile(){

        if ( ($sFullNameFile = $this->loadFileImport()) !== false ){
            $sResponse = "success";
        } else {
            $sResponse = "failure";
        }

        // Если файл полностью загружен и стоит галочка "Обновлять заказы в Cms"
        if ( self::validateXmlFile( $sFullNameFile ) && SysVar::get('1c.updateStatusesCms') ){
            self::updateOrdersStatusFromFile( $sFullNameFile );
        }

        $this->sendResponse( $sResponse );
    }

    /** Вернёт файл с заказами (с сайта в 1с) */
    protected function cmdQuery(){
        $this->sendResponse( self::createOrdersCommerceMl() );
    }

    /** 1c успешно принял и записал файл с заказами */
    protected function cmdSuccess(){
        $this->sendResponse( 'success' );
    }

    /**
     * Формирует массив данных заказов
     * @param $aStatuses
     * @return array|bool
     */
    private static function getOrdersData($aStatuses ){

        if ( !$aStatuses )
            return false;

        $aOrders = Order::find()
            ->where('status', $aStatuses)
            ->getAll();

        if ( !$aOrders )
            return false;

        $aDocuments = $aCommonContragent = $aContragents = [];

        // использовать общего контрагента?
        $bUseCommonContragent = (bool) SysVar::get('1c.useCommonContragent');

        // контрагенты заказов
        if ( $bUseCommonContragent )
            $aCommonContragent = self::getCommonContragent();
        else{
            $aContragents = self::getContragents( $aOrders );
        }

        // товары заказов
        $aGoods = self::getOrderGoods( $aOrders );

        // реквизиты заказов
        $Properties = self::getProperiesOrders( $aOrders );

        // Суммы заказов
        $aSumOrders = Api::getGoodsStatistic( ArrayHelper::getColumn($aOrders, 'id', []) );

        /** @var OrderRow $oOrder */
        foreach ($aOrders as $oOrder) {

            $aDocuments[] = [
                'Документ' => [
                    'Номер'       => $oOrder->id,
                    'Дата'        => date('Y-m-d', strtotime($oOrder->date)),
                    'ХозОперация' => 'Заказ товара',
                    'Роль'        => 'Продавец',
                    'Валюта'      => 'руб',
                    'Курс'        => '1',
                    'Сумма'       => $aSumOrders[$oOrder->id]['sum'],
                    'Контрагенты' => ($bUseCommonContragent)? $aCommonContragent: $aContragents[$oOrder->id],
                    'Время'       => date('H:i:s', strtotime($oOrder->date)),
                    'Комментарий' => self::buildOrderComment($oOrder, $bUseCommonContragent),
                    'Товары'      => $aGoods[$oOrder->id],
                    'ЗначенияРеквизитов' => $Properties[$oOrder->id]

                ],
            ];
        }

        return $aDocuments;
    }


    /**
     * Создать файл с заказами в формате CommerceMl
     * @return string
     */
    private static function createOrdersCommerceMl(){
        $oXml = new \XMLWriter();
        $oXml->openMemory();
        $oXml->startDocument( '1.0', 'windows-1251' );

        $aMainDocument = [
            'КоммерческаяИнформация' => [
                'attributes' => [
                    'ВерсияСхемы'      => ArrayHelper::getValue(self::getCommerceMlSchemaVersions(),SysVar::get('1c.schema_version', 0)),
                    'ДатаФормирования' => date( 'Y-m-d\TH:i:s', time() )
                ],
            ]
        ];

        // Экспортируемые статусы
        $aExportStatuses = StringHelper::explode(SysVar::get('1c.export_statuses',''), ',', true, true);

        // Документы заказов
        $aDocumentOrders = self::getOrdersData( $aExportStatuses );

        if ( $aDocumentOrders )
            $aMainDocument['КоммерческаяИнформация'][] = $aDocumentOrders;

        self::arrayToXml($oXml, $aMainDocument);

        return $oXml->outputMemory();
    }



    /**
     * Запишет массив в xml
     *
     * [
     *  'Элемент1' => [
     *      'attributes' => [
     *          'имя_атрибута1' => 'значение1',
     *          'имя_атрибута2' => 'значение2'
     *      ],
     *      'элемент1_1' =>[...],
     *      'элемент1_2' =>[...]
     *       .....
     *       .....
     *       .....
     *  ]
     *
     * ]
     *
     * @param \XMLWriter $oXml
     * @param array $aData
     */
    private static function arrayToXml(\XMLWriter $oXml, $aData ){

        if (empty($aData) || !is_array($aData)){
            return ;
        }

        foreach ($aData as  $sKey => $mValue) {
            if (is_numeric($sKey)){
                self::arrayToXml($oXml, $mValue);
            }elseif ( $sKey == 'attributes' ){

                foreach ($mValue as $sNameAttr => $sValueAttr) {
                    $oXml->writeAttribute($sNameAttr, $sValueAttr);
                }

            } else {

                if ( is_array($mValue) ){
                    $oXml->startElement($sKey);
                    self::arrayToXml( $oXml, $mValue );
                    $oXml->endElement();
                } else {
                    if ( $sKey == 'value' ){
                        $oXml->text($mValue);
                    } else {
                        $sKey = trim($sKey);
                        $oXml->writeElement($sKey, $mValue);
                    }
                }

            }

        }

    }

    /**
     * Вернёт массив вида [ 'id заказа' => 'данные контрагента']
     * @param $aOrders OrderRow[]
     * @return array
     */
    private static function getContragents( $aOrders ){
        $aContragents = [];

        foreach ($aOrders as $oOrder) {

            $aAgent = [
                'Контрагент' => [
                    'Наименование'       => $oOrder->person,
                    'ПолноеНаименование' => $oOrder->person,
                    'Роль'               => 'Покупатель',
                    'Контакты' => [
                        [
                            'Контакт' => [
                                'Тип'      => 'Электронная почта',
                                'Значение' => $oOrder->mail
                            ]
                        ],
                        [
                            'Контакт' => [
                                'Тип'      => 'Телефон рабочий',
                                'Значение' => $oOrder->phone
                            ]
                        ]
                    ]
                ]
            ];

            $aContragents[$oOrder->id] = $aAgent;

        }

        return $aContragents;

    }

    /**
     * Вернет товары заказов, массив вида [ 'id_заказа' => [ 'товар1', 'товар2', ... ] ]
     * @param $aOrders OrderRow[]
     * @return array
     */
    private static function getOrderGoods( $aOrders ){

        // товары
        $aOut = [];

        // список id заказов
        $aListOrdersId = ArrayHelper::getColumn($aOrders, 'id', []);

        // товары заказов
        $aOrderGoods = Goods::find()
            ->where('id_order', $aListOrdersId)
            ->asArray()
            ->getAll();

        $aListGoodsId = ArrayHelper::getColumn($aOrderGoods, 'id_goods', []);

        $oModel = Cache::get( Card::DEF_BASE_CARD );

        // данные товаров из таблицы co_base_card
        $aGoodsData = Query::SelectFrom( $oModel->getTableName(), $oModel )
            ->where( $oModel->getPrimaryKey(), $aListGoodsId )
            ->index('id')
            ->asArray()->getAll();

        foreach ($aOrderGoods as $aGood) {

            $iCurrentGoodId = $aGood['id_goods'];

            $aCurrentGoodData = [
                'Товар' => [
                    'Ид'           => ArrayHelper::getValue($aGoodsData[$iCurrentGoodId], 'id_1s',0),
                    'Артикул'      => ArrayHelper::getValue($aGoodsData[$iCurrentGoodId], 'article'),
                    'Наименование' => $aGoodsData[$iCurrentGoodId]['title'],
                    'БазоваяЕдиница' => [
                        'attributes' => [
                            'Код' => 796,
                            'НаименованиеПолное' => 'Штука',
                            'МеждународноеСокращение' => 'PCE'
                        ],
                        'value' => 'шт'
                    ],
//                        'СтавкиНалогов' => [
//                            'СтавкаНалога' => [
//                                'Наименование' => 'НДС',
//                                'Ставка' => 18
//                            ]
//                        ],
                    //todo Передавай-непередавай 1с всё равно учитывает НДС в соответствии со своими настройками цен и валют
                    'ЗначенияРеквизитов' => [
                        [
                            'ЗначениеРеквизита' => [
                                'Наименование' => 'ВидНоменклатуры',
                                'Значение' => 'Товар'
                            ]
                        ],
                        [
                            'ЗначениеРеквизита' => [
                                'Наименование' => 'ТипНоменклатуры',
                                'Значение' => 'Товар'
                            ]
                        ]
                    ],
                    'ЦенаЗаЕдиницу' => $aGood['price'],
                    'Количество'    => $aGood['count'],
                    'Сумма'         => $aGood['total'],
//                    'Единица'       => 'шт',
//                    'Коэффициент'   => 1,
//                        'Налоги' => [
//                            'Налог' => [
//                                'Наименование' => 'НДС',
//                                'УчтеноВСумме' => 'true',
//                                'Сумма'        => 120.43,
//                                'Ставка'       => 18
//                            ]
//
//                        ]
                    //todo Передавай-непередавай 1с всё равно учитывает НДС в соответствии со своими настройками цен и валют
                ]
            ];


            $aOut[$aGood['id_order']][] = $aCurrentGoodData;

        }

        return $aOut;

    }


    /**
     * @param $aOrders OrderRow[]
     * @return array
     */
    private static function getProperiesOrders( $aOrders ){

        $aOut = [];

        $aTypePayment = TypePayment::find()
            ->index('id')
            ->asArray()
            ->getAll();

        // Заголовки статусов
        $aTitleStatuses = ArrayHelper::map(Status::getList(),'id','title');

        foreach ($aOrders as $oOrder) {

            //todo Возможно передаваемые статусы надо расчитывать исходя из текущего статуса заказа, а не из истории
            $aChangeStatuses = self::getListChangeStatuses( $oOrder->id, true );

            $aOut[$oOrder->id] = [
                [
                    'ЗначениеРеквизита' => [
                        'Наименование' => 'Статус заказа',
                        'Значение'     => $aTitleStatuses[$oOrder->status]
                    ]
                ],
                [
                    'ЗначениеРеквизита' => [
                        'Наименование' => 'Метод оплаты',
                        'Значение'     => $aTypePayment[$oOrder->type_payment]['title']
                    ]
                ],
                [
                    'ЗначениеРеквизита' => [
                        'Наименование' => 'Заказ оплачен',
                        'Значение'     => in_array(Status::getIdByPaid(),$aChangeStatuses) ? 'true' : 'false'
                    ]
                ],
                [
                    'ЗначениеРеквизита' => [
                        'Наименование' => 'Финальный статус',
                        'Значение'     => in_array(Status::getIdByClose(),$aChangeStatuses) ? 'true' : 'false'
                    ]
                ],
                [
                    'ЗначениеРеквизита' => [
                        'Наименование' => 'Отменен',
                        'Значение'     => in_array(Status::getIdByCancel(),$aChangeStatuses) ? 'true' : 'false'
                    ]
                ],
                [
                    'ЗначениеРеквизита' => [
                        'Наименование' => 'Доставка разрешена',
                        'Значение'     => in_array(Status::getIdBySend(),$aChangeStatuses) ? 'true' : 'false'
                    ]
                ],

            ];

        }

        return $aOut;

    }

    /**
     * Обновить статусы заказов информацией из файла
     * @param $sFilePath string -  путь к файлу
     */
    private static function updateOrdersStatusFromFile( $sFilePath ){

        $oDom = new \DOMDocument();
        $oDom->load( $sFilePath );

        $xpath = new \DOMXPath( $oDom );
        $aEntries = XmlReader::queryXPath( $xpath, '//КоммерческаяИнформация/Документ' );

        $i = 0;
        while ( $item = $aEntries->item($i++) ){

            $oSimpleXml = simplexml_import_dom($item);

            $iCurrentOrderId = $oSimpleXml->Номер;

            /** @var OrderRow $oOrder */
            if ( $oOrder = Order::findOne( ['id' => $iCurrentOrderId] ) ){

                $bIsPaid = $bIsSenden = false;

                foreach ($oSimpleXml->ЗначенияРеквизитов->ЗначениеРеквизита as $item) {
                    // Заказ оплачен?
                    if ( in_array($item->Наименование, ['Номер оплаты по 1С', 'Дата оплаты по 1С']) )
                        $bIsPaid = true;

                    // Заказ отгружен?
                    if ( in_array($item->Наименование, ['Дата отгрузки по 1С', 'Номер отгрузки по 1С']) )
                        $bIsSenden = true;

                }

                if ( $bIsPaid || $bIsSenden ) {

                    $iNewStatus = false;

                    if ($bIsPaid && $bIsSenden) {
                        $iNewStatus = SysVar::get('1c.status_after_delivery_and_paid');
                    } else {
                        $iNewStatus = ($bIsPaid)
                            ? SysVar::get('1c.status_after_paid')
                            : SysVar::get('1c.status_after_delivery');
                    }

                    if ( $iNewStatus !== false ){
                        $oOrder->status = $iNewStatus;
                        $oOrder->save();
                        Service::sendMailChangeOrderStatus( $oOrder->id, $oOrder->status, $iNewStatus );
                    }

                }

            }

        }

    }

    /**
     * Вернёт общего контрагента
     * @return array
     */
    private static function getCommonContragent(){
        return [
            'Контрагент' => [
                'Наименование'       => 'Web-caйт',
                'ПолноеНаименование' => 'Web-caйт',
                'Роль'               => 'Покупатель',
            ]
        ];
    }

    /**
     * Построит комментарий заказа
     * @param $oOrder OrderRow - заказ
     * @param $bUseCommonContragent bool - использовать общего контрагента
     * @return string
     */
    private static function buildOrderComment( $oOrder, $bUseCommonContragent = false ){
        if ( $bUseCommonContragent ){
            return sprintf('Контрагент: %s; Роль: Покупатель; Электронная почта: %s; Телефон рабочий: %s;', $oOrder->person, $oOrder->mail, $oOrder->phone);
        } else {
            return $oOrder->text;
        }

    }

    /**
     * Версии схем CommerceML
     * @return array
     */
    public static function getCommerceMlSchemaVersions(){
        return [
            '2.05',
            '2.07'
        ];
    }

    /**
     * Вернёт все статусы в которых когда-либо находился заказ
     * @param int $iOrderId - ид заказа
     * @param bool $bWithStartStatus - включить стартовый статус(новый)?
     * @return array
     */
    private static function getListChangeStatuses($iOrderId, $bWithStartStatus = true ){

        $aChangeStatuses = ChangeStatus::find()
            ->where(['id_order' => $iOrderId])
            ->asArray()->all();

        $aChangeStatuses = ArrayHelper::getColumn($aChangeStatuses, 'id_new_status');

        if ( $bWithStartStatus ){
            array_merge($aChangeStatuses, [Status::getIdByNew()]);
        }

        return array_unique($aChangeStatuses);

    }

    /**
     * Проверяет xml-файл на валидность. Функция используется для проверки полной загрузки файла
     * @param string $sFilePath - путь к файлу
     * @return bool
     */
    private static function validateXmlFile( $sFilePath ){

        $xml = new \DOMDocument();
        $result =  @$xml->load( $sFilePath );

        if ( $result === false )
            return false;
        else
            return true;

    }

}