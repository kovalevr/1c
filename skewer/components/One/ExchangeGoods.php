<?php

namespace skewer\components\One;

use skewer\base\queue;
use skewer\base\queue\ar\Task;
use skewer\base\site_module\Request;
use skewer\base\SysVar;
use skewer\components\import\Api;
use skewer\components\import\ar;
use skewer\components\import\Config;
use yii\helpers\FileHelper;

/**
 * Class ExchangeGoods
 * @link http://v8.1c.ru/edi/edi_stnd/131/ - Протокол обмена между системой "1С:Предприятие" и сайтом
 * @package skewer\components\One
 */
class ExchangeGoods extends ExchangePrototype {

    /**
     * Прием файлов от 1с
     */
    protected function cmdFile(){

        if ( $bRes = $this->loadFileImport() ){
            $sResponse = "success";
        } else {
            $sResponse = "failure";
        }

        $this->sendResponse( $sResponse );
    }


    /**
     * Импорт товаров и цен
     */
   protected function cmdImport(){

       $aRes = $this->importGoodsAndPrices();

       if ( $aRes['repeat'] ){
           $sResponse = "progress\n" . $aRes['message'];
       } else{
           $sResponse = "success";
       }

       $this->sendResponse( $sResponse );

   }


    /**
     * Импорт товаров и цен
     * @return array
     */
    private function importGoodsAndPrices(){

        // Сообщение статуса выполнения операции для 1с
        $sMessageStatus = '';

        // Флаг необходимости повторить этот же запрос
        $bNeedRepeatRequest = false;

        // Результат работы метода
        $aOut = array( 'repeat' => &$bNeedRepeatRequest, 'message' => &$sMessageStatus );

        $sFileName = Request::getStr('filename');

        if ( strpos($sFileName, 'import') !== false )
            $sMode = 'import';
        elseif ( strpos($sFileName, 'offers') !== false )
            $sMode = 'offers';
        else
            return $aOut;

        // 1. Проверка наличия необходимых шаблонов импорта
        /** @var ar\ImportTemplateRow $oImportTemplate */
        if ( !$oImportTemplate = self::getImportTemplateFor1c($sMode) )
            return $aOut;

        // 2. Установка путей к файлам импорта
        $sFullNameImportFile = $this->getFullNameCurrentFile($sFileName);
        self::replacePaths( $oImportTemplate, $sFullNameImportFile );

        // 3. Импорт
        $aRes = self::runImport($sMode);

        if ( in_array($aRes['status'], [queue\Task::stFrozen, queue\Task::stWait]) ){
            $sMessageStatus = ( $sMode == 'import' )
                ? 'Goods are imported into the CMS'
                : 'Prices are imported into the CMS';
            $bNeedRepeatRequest = true;
        } elseif( $sMode == 'offers' ){ //todo Непонятна проверка

            // 4. Удаление изображений и перенос файлов импорта в другую директорию
            FileHelper::removeDirectory( dirname($sFullNameImportFile) . '/import_files' );
        }

        return $aOut;
    }

    /**
     * Получить шаблон импорта для обмена с 1с
     * @param string $type режим обмена
     * @return bool|\skewer\base\orm\ActiveRecord
     */
    private static function getImportTemplateFor1c( $type ){

        if ( $type == 'import' ){
            $sParamName = '1c.id_import_template_goods';
        } elseif ( $type == 'offers' ){
            $sParamName = '1c.id_import_template_prices';
        } else {
            return false;
        }

        if ( !($iId = SysVar::get($sParamName)) )
            return false;

        return  ar\ImportTemplate::find( $iId );

    }

    /**
     * Установка путей к файлам импорта(к самому файлу импорта + к директории с изображениями)
     * @param ar\ImportTemplateRow $oImportTemplate - шаблон импорта
     * @param string $sFullNameImportFile - полный путь к файлу импорта
     */
    private static function replacePaths(ar\ImportTemplateRow $oImportTemplate, $sFullNameImportFile ){

        // Подмена пути к директории с изображениями
        $oConfig = new Config( $oImportTemplate );
        $oConfig->setFieldsParam( ['params_gallery:imagedir' => str_replace(ROOTPATH, '', dirname($sFullNameImportFile)) ] );
        $oImportTemplate->settings = $oConfig->getJsonData();

        // Подмена пути к файлу обмена
        $oImportTemplate->source = str_replace(ROOTPATH, '', $sFullNameImportFile);
        $oImportTemplate->type = Api::Type_Path;
        $oImportTemplate->save();

    }

    /**
     * Запуск импорта. Возвращает статус задачи
     * @param string $type
     * @return array
     * @throws \Exception
     */
    private static function runImport( $type ){

        $iTask = null;

        /** @var queue\ar\TaskRow $oTask */
        $oTask = Task::find()
            ->where('status IN ? ', [ queue\Task::stNew, queue\Task::stFrozen, queue\Task::stWait ] )
            ->where('mutex', 0)
            ->where('class', '\skewer\components\import\Task') //todo сделать className
            ->order('upd_time', 'DESC')
            ->getOne();

        if ( $oTask )
            $iTask = $oTask->id;

        if (!$iTask){

            /** @var ar\ImportTemplateRow $oImportTpl*/
            if ( !$oImportTpl = self::getImportTemplateFor1c( $type ) )
                throw new \Exception('Не найден шаблон импорта');

            $iTask = queue\Api::addTask([
                'class'         => '\skewer\components\import\Task',
                'priority'      => queue\Task::priorityHigh,
                'resource_use'  => queue\Task::weightHigh,
                'title'         => \Yii::t( 'import', 'task_title', $oImportTpl->title),
                'parameters'    => ['tpl' => (int)$oImportTpl->id],
            ]);

        }

        /** @var queue\Task $oTask */
        $oTask = queue\Api::getTaskById( $iTask );

        $oManager = queue\Manager::getInstance();
        $oManager->executeTask( $oTask );


        return ['status' => $oTask->getStatus(), 'id' => $iTask];

    }


}