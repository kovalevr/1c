<?php

namespace skewer\components\catalog;

use skewer\base\orm\Query;
use skewer\base\ft;


/**
 * API для работы с сущностями
 * Class Entity
 * @package skewer\components\catalog
 */
class Entity {

    /** тип - словарь */
    const TypeDictionary = 0;

    /** тип - базовая карточка */
    const TypeBasic = 1;

    /** тип - расширенная карточка */
    const TypeExtended = 2;


    /**
     * Отдает модель по имени карточки или будет выброшено исключение
     * @param string|int $card Имя или идентификатор сущности
     * @return ft\Model
     */
    public static function getModel( $card ) {
        return ft\Cache::get( $card );
    }


    /**
     * Карточка товара по идентификатору
     * @param int|string|null $id Идентификатор карточки
     * @return model\EntityRow
     */
    public static function get( $id = null ) {

        if ( !$id )
            return model\EntityTable::getNewRow();

        if ( is_numeric( $id ) ) {
            return model\EntityTable::find( $id );
        } else {
            return model\EntityTable::findOne( ['name'=>$id] );
        }

    }


    /**
     * Сброс кеша для всех сущностей
     * @return bool
     */
    public static function clearCache() {
        return model\EntityTable::update()->set( 'cache', '' )->get();
    }


    /**
     * Сборка - создание модели и кеширование
     * @param int|string $id Идентификатор сущности
     * @return bool
     */
    public static function build( $id ) {

        if ( !$id )
            return false;

        if (!$oCard = self::get( $id ))
            return false;

        $oCard->updCache();

        return true;
    }


    /**
     * Метод получения ActiveRecord записи по имени сущности
     * @param string $card
     * @param array $condition
     * @return CatalogGoodRow
     * @throws \Exception
     * @throws ft\Exception
     * todo стоит поискать более подходящее место
     */
    public static function getItemRow( $card, $condition = [] ) {

        $oModel = ft\Cache::get( $card );

        if ( !$oModel )
            throw new \Exception('Не найдена модель для карточки.');

        $oQuery = Query::SelectFrom( $oModel->getTableName(), $oModel );

        if ( !$oQuery )
            throw new \Exception('Не найдена модель для карточки.');

        if ( $condition )
            $oQuery->where( $condition );

        return $oQuery->getOne();
    }

    /**
     * Получить приставку имени таблицы для нужного типа каталожной сущности
     * @param int $iType Тип каталожной сущности
     * @return string
     */
    public static function getTablePreffix($iType) {
        return \Yii\helpers\ArrayHelper::getValue( ['cd_', 'co_', 'ce_'], $iType, 'co_' );
    }
}