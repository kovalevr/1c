<?php

namespace skewer\components\catalog\field;

use skewer\base\site\Layer;
use skewer\base\site_module;
use skewer\build\Page\CatalogMaps\models\GeoObjects;
use skewer\build\Page\CatalogMaps;
use yii\helpers\ArrayHelper;

class MapSingleMarker extends Prototype{

    /**
     * @inheritdoc
     */
    protected function build($value, $rowId, $aParams) {

        $aOut = [
            'value' => $value,
            'html'  => $value
        ];

        if ( $value && ($oGeoObject = GeoObjects::findOne($value)) ){
            $aOut['latitude']  = $oGeoObject->latitude;
            $aOut['longitude'] = $oGeoObject->longitude;
            $aOut['map_id']    = $oGeoObject->map_id;
            $aOut['address']   = $oGeoObject->address;
        }

        return $aOut;
    }

    /**
     * @inheritdoc
     */
    public function afterParseGood($aGoodData, $aFieldData){

        if ( empty($aFieldData['latitude']) && empty($aFieldData['longitude']) && empty($aFieldData['map_id']) ){
            $aFieldData['html'] = $aFieldData['value'] = '';
            return $aFieldData;
        }

        $sCatMapsClassName = site_module\Module::getClass(CatalogMaps\Module::getNameModule(), Layer::PAGE);
        $sCatMapsDir = dirname( \Yii::getAlias('@' . str_replace('\\', '/', $sCatMapsClassName) . '.php') ) . DIRECTORY_SEPARATOR;
        $sCatMapsTplDir = $sCatMapsDir . 'templates'. DIRECTORY_SEPARATOR;

        $aMarker = [
            'latitude'      => ArrayHelper::getValue($aFieldData, 'latitude'),
            'longitude'     => ArrayHelper::getValue($aFieldData, 'longitude'),
            'title'         => ArrayHelper::getValue($aGoodData, 'title', ''),
            'popup_message' => \Yii::$app->getView()->renderPhpFile(
                $sCatMapsTplDir . 'infoWindow.php',
                [ 'aGood' => $aGoodData, 'reedMore' => false ]
            )
        ];


        $iMapId = ArrayHelper::getValue($aFieldData, 'map_id');

        $sHtml = CatalogMaps\Api::buildMap( [ $aMarker ], $iMapId );

        if ( $sHtml === false )
            $aFieldData['html'] = $aFieldData['value'] = '';
        else
            $aFieldData['html'] = $sHtml;

        return $aFieldData;
    }

}