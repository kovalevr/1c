<?php

namespace skewer\components\catalog\field;

use skewer\base\ft;
use yii\helpers\ArrayHelper;
use skewer\base\ft\Editor;
use skewer\components\catalog\Card;

/**
 * Прототип для типизированного объекта парсинга поля сущности
 * Class Prototype
 * @package skewer\components\catalog\field
 */
abstract class Prototype {

    protected $type = '';

    protected $name = '';

    protected $title = '';

    protected $card = '';

    protected $attr = [];


    protected $subEntity = '';

    protected $subData = []; // todo перенести в Cache


    /** @var ft\model\Field */
    protected $ftField = null;


    public function getName() {
        return $this->name;
    }


    /**
     * Создание поля для парсера на основе ft-поля модели
     * @param ft\model\Field $field
     * @return Prototype
     */
    public static function init( $field ) {

        $sEditorName = $field->getEditorName();

        // для php 7.0
        if ( in_array($sEditorName, ['string']) )
            $sEditorName .= 'Field';

        $class = __NAMESPACE__ . '\\' . ucfirst($sEditorName);

        $obj = class_exists( $class ) ? new $class() : new StringField();

        $obj->ftField = $field;
        $obj->type = $field->getEditorName();
        $obj->name = $field->getName();
        $obj->title = $field->getTitle();
        $obj->card = $field->getModel()->getName();
        $obj->attr = $field->getAttrs();

        $obj->load();
        $obj->loadSubData();

        return $obj;
    }


    protected function load() {

    }


    protected function loadSubData() {

        $oModel = ft\Cache::get( $this->card );

        if ( !$oModel )
            return;

        if ( $oRel = $oModel->getOneFieldRelation( $this->name ) ) {

            $this->subEntity = $oRel->getEntityName();

            $query = ft\Cache::getMagicTable( $this->subEntity )->find()->asArray();

            // Сортировка значений справочника мультисписка
            if ($this->type == Editor::MULTISELECT)
                $query->order(Card::FIELD_SORT);

            while ( $row = $query->each() )
                $this->subData[ $row['id'] ] = $row;

        }
    }


    protected function getSubDataValue( $key ) {
        return ArrayHelper::getValue( $this->subData, $key, false );
    }


    /**
     * Классовая функция преобразования значения
     * @param string $value значение поля из записи
     * @param int $rowId идентификатор записи
     * @param array $aParams Дополнительные параметры
     * @return mixed
     */
    abstract protected function build( $value, $rowId, $aParams );


    /**
     * Функция преобразования значения
     * @param string $value значение поля из записи
     * @param int $rowId идентификатор записи
     * @param array $aParams Дополнительные параметры
     * @return mixed
     */
    public function parse( $value, $rowId, $aParams = [] ) {

        $out = $this->build( $value, $rowId, $aParams );

        $out['attrs'] = $this->attr;
        $out['name'] = $this->name;
        $out['card'] = $this->card;
        $out['title'] = $this->title;
        $out['type'] = $this->type;

        // ед измерения
        if ( isSet($out['attrs']['measure']) && $out['attrs']['measure'] && $out['html'] ) {
            $out['measure'] = $out['attrs']['measure'];
            $out['html'] .= ' ' . $out['attrs']['measure'];
        }

        return $out;
    }

    /**
     * Действие, выполняемое после парсинга товара
     * @param array $aGoodData  - данные товара
     * @param array $aFieldData - данные поля
     * @return null
     */
    public function afterParseGood( $aGoodData, $aFieldData ){
        return null;
    }
}