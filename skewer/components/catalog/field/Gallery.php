<?php

namespace skewer\components\catalog\field;

use skewer\components\gallery\Photo;


class Gallery extends Prototype {

    protected function build( $value, $rowId, $aParams ) {

        $aImages = Photo::getFromAlbum( $value, true ) ?: [];

        return [
            'value' => $value,
            'gallery' => ['images' => &$aImages],
            'first_img' => reset($aImages),
            'html' => $value,
        ];
    }
}