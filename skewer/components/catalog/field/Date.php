<?php

namespace skewer\components\catalog\field;


class Date extends Prototype {

    protected function build( $value, $rowId, $aParams ) {
        return [
            'value' => ( $value == '0000-00-00' ) ? '' : $value,
            'html' => $value ? date( 'd.m.Y', strtotime( $value ) ) : ''
        ];
    }
}