<?php

namespace skewer\components\catalog\cache;

/**
 * Статическое хранилище для кешированных данных
 */
class Storage {

    /** @var array Хранилище данных */
    private static $aData = array();


    /**
     * Сохранение параметра
     * @param string $sName Имя параметра
     * @return null
     */
    public static function get( $sName ) {

        return isSet( static::$aData[ $sName ] ) ? static::$aData[ $sName ] : null;
    }


    /**
     * Получение параметра
     * @param $sName
     * @param $mValue
     */
    public static function set( $sName, $mValue ) {

        static::$aData[ $sName ] = $mValue;
    }


    /**
     * Сохранение в базу данных
     * @return bool
     */
    public static function save() {

        // todo сохранение в таблицу

        return true;
    }




} 