<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 12.05.2016
 * Time: 9:15
 */

namespace skewer\components\redirect;

use skewer\base\site\Server;
use skewer\base\SysVar;
use skewer\build\Tool\Domains\models\Domain;
use skewer\components\redirect\models\Redirect;
use yii\base\Exception;

class Api {

    public static $sNewDomain = null;

    /**
     * Выполнение редиректа
     */
    public static function execute(){

        if (self::useRedirect()){
            $sUrl = self::selectRedirect($_SERVER['REQUEST_URI']);

            if ($sUrl){
                header("HTTP/1.1 301 Moved Permanently");
                header( 'Location: '.$sUrl );
                exit;
            }
        }

    }

    /**
     * Метод прослойка для тестирования
     * @return mixed|string
     */
    public static function getRedirect($sUrl = '',$iRedirectId=0){

        return self::selectRedirect($sUrl,$iRedirectId);

    }

    /**
     * Выбор редиректа
     * @param string $sUri
     * @return string
     */
    private static function selectRedirect($sUri = '',$iRedirectId=0){

        /*Обработаем редирект по домену*/
        $sUrl = self::tryDomainRedirect($sUri);
        /*Попробуем по регулярке*/
        $sUrl = self::tryRegExpRedirect($sUrl,$iRedirectId);
        /*Приведение URL в нормальный вид*/
        $sUrl = self::createValidURL(str_replace('//','/',$sUrl));

        return $sUrl;
    }

    /**
     * Формирует валидный URL с именем домена и https если нужно
     * @param $sUrl
     * @return mixed|string
     */
    private static function createValidURL($sUrl){

        /*Если редирект на другой домен(из редиректов 301), то сразу редиректим*/
        if ((strpos($sUrl,'http:/')!==false) or (strpos($sUrl,'https:/')!==false)){
            $sUrl = str_replace('http:/','http://',$sUrl);
            $sUrl = str_replace('https:/','https://',$sUrl);
            return $sUrl;
        }

        if ((('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] === 'http://'.self::$sNewDomain.$sUrl))
            or ('https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] === 'https://'.self::$sNewDomain.$sUrl)){
            /*Страница на которой НЕТ редиректа*/
            return '';
        } else {
            $sOutUrl = 'http://'.self::$sNewDomain.$sUrl;

            if (\Yii::$app->request->isSecureConnection) {
                $sOutUrl = str_replace('http://', 'https://', $sOutUrl);
            }

            return $sOutUrl;
        }

    }

    /**
     * Проверка использовать редирект или нет
     * @return int
     */
    private static function useRedirect(){

        if (Server::isApache())
            return (int)SysVar::get('useApacheRedirect');
        if (Server::isNginx())
            return (int)SysVar::get('useNginxRedirect');

        /*Если вообще неизвестный сервер, будем считать что редиректы нужны*/
        return 1;
    }

    /********Обработчики вариантов редиректов******/
    /**
     * Реализует редирект с домена на домен
     * в том числе www to no www и наоборот
     */
    private static function tryDomainRedirect($sUrl = ''){

        $aDomain = Domain::find()
            ->where(['prim'=>'1'])
            ->orderBy('d_id')
            ->asArray()
            ->one();

        if (!is_null($aDomain)){
            self::$sNewDomain = $aDomain['domain'];
            return $aDomain['domain'].'/'.$sUrl;
        } else {
            self::$sNewDomain = $_SERVER['HTTP_HOST'];
            return $_SERVER['HTTP_HOST'].'/'.$sUrl;
        }

    }

    /**
     * Обработать 1 правило
     * @param $sInRule
     * @param $sOutRule
     * @param $sHost
     * @param $sRequestUri
     * @return mixed
     * @throws Exception
     */
    public static function checkRule($sInRule,$sOutRule,$sRequestUri,$sHost=null){

        if (is_null($sHost)) $sHost = $_SERVER['HTTP_HOST'];

        /*По точному соответствию*/
        /*Отрежем у URL в редиректе последний слэш*/
        if (substr($sInRule,-1)=='/')
            $sTempUrl = substr($sInRule, 0, strlen($sInRule)-1);
        else
            $sTempUrl = $sInRule;

        if (
            ('http://'.str_replace('//','/',$sHost.$sRequestUri) === $sTempUrl)
            or ('https://'.str_replace('//','/',$sHost.$sRequestUri) === $sTempUrl)
            or ($sRequestUri === $sTempUrl)
        ){
            return $sOutRule;
        }

        /*Проверим похоже ли правило на регулярку и не заканчивается ли оно на /*/
        if (((strpos($sInRule,'+')!==false) or (strpos($sInRule,'*')!==false)) and (substr($sInRule,-1)=='/') ){
            $sInRule = substr($sInRule, 0, -1);
        }

        /*По регулярке*/
        if (strpos($sInRule,'$')===false){
            $sInRule = '/'.str_replace('/','\/',$sInRule).'$/';
        } else {
            $sInRule = '/'.str_replace('/','\/',$sInRule).'/';
        }

        /**
         * @ и error_get_last нужны чтобы корректно отлавливать ошибки при разборе регулярки
         */
        @preg_match($sInRule,$sRequestUri, $matches, PREG_OFFSET_CAPTURE);

        $aError = error_get_last();
        if ((!is_null($aError)) and ($aError['file']==__FILE__) and (strpos($aError['message'],'preg_match')!==false)){
            throw new Exception ($aError['message']);
        }
        if (!empty($matches)) {
            return preg_replace($sInRule, $sOutRule, $sRequestUri);
        }
        return null;
    }

    /**
     * Применяет к URL редиректы с регулярками
     * Так же обрабатывает внутресайтовые редиректы
     */
    private static function tryRegExpRedirect($sUrl = null, $iRedirectId = 0){

        $sUrl = str_replace('http://','',$sUrl);

        $aUrl = explode('/',$sUrl);

        $sHost = $aUrl[0];
        unset($aUrl[0]);
        $sRequestUri = implode('/',$aUrl);


        if($iRedirectId)
            $aRedirects = Redirect::find()
                ->where(['id'=>$iRedirectId])
                ->orderBy('priority')
                ->asArray()
                ->all();
        else
            $aRedirects = Redirect::find()
                ->orderBy('priority')
                ->asArray()
                ->all();


        foreach ($aRedirects as &$item){

            $sRedirect = self::checkRule($item['old_url'],$item['new_url'],$sRequestUri,$sHost);

            if (!is_null($sRedirect)) return $sRedirect;

        }
        return $sRequestUri;
    }

    /**
     * Тестирование правил
     * @param $aUrls
     * @param int $sRedirectId
     * @return string
     */
    public static function testUrls($aUrls, $sRedirectId = 0){

        $aData = [];

        if ((count($aUrls)=='1') and (!$aUrls[0])){
            $aData['items']=[];
        } else {
            /*Запишем в файл тестовые УРЛы если тестим по всем редиректам*/
            if (!$sRedirectId) self::setTestUrls($aUrls);

            foreach ($aUrls as &$url){
                $sStartUrl  = $url;
                $url = str_replace($_SERVER['HTTP_HOST'],'',$url);
                $url = str_replace('http://','',$url);
                $url = str_replace('https://','',$url);

                if (substr($url,-1)=='/')
                    $url = substr($url, 0, strlen($url)-1);

                for ($i = 1; $i <= 10; $i++){
                    if ((strpos($url,$_SERVER['HTTP_HOST'])!==false) or ((strpos($url,str_replace('www.','',$_SERVER['HTTP_HOST']))!==false))) {
                        $url = str_replace($_SERVER['HTTP_HOST'], '', $url);
                        $url = str_replace('www' . $_SERVER['HTTP_HOST'], '', $url);
                        $url = str_replace(str_replace('www.', '', $_SERVER['HTTP_HOST']), '', $url);
                        $url = str_replace('http://', '', $url);
                        $url = str_replace('https://', '', $url);
                    }
                    $url = str_replace('//','/',$url);
                    $url = self::getRedirect($url,$sRedirectId);

                    if (substr($url,-1)=='/')
                        $url = substr($url,0,strlen($url)-1);
                }
                /*Роутер всем урлам добавит последний слэш. мы тоже добавим*/
                $aData['items'][] = [
                    'old'=>$sStartUrl,
                    'new'=>$url.'/'
                ];
            }
        }

        $sOut = \Yii::$app->getView()->renderFile(__DIR__.'/template/test_redirect.php',$aData);
        return $sOut;
    }

    public static function getDir(){
        return __DIR__;
    }

    /**
     * Сохраняет тестовые URL в файл
     * @param $aUrls
     */
    private static function setTestUrls($aUrls){

        $sUrls = implode("\n",$aUrls);

        if ($sUrls!==''){
            SysVar::set('testUrls',$sUrls);
        }

    }

    /**
     * Достает тестовые URL из файла
     * @return string
     */
    public static function getTestUrls(){

        if (!is_null(SysVar::get('testUrls')))
            return SysVar::get('testUrls');
        else
            return '';

    }

}