<?php

namespace skewer\components\seo;


use skewer\base\queue as QM;
use skewer\base\section\Tree;
use skewer\base\site\Server;
use skewer\base\SysVar;
use skewer\build\Tool\RobotsTxt;
use skewer\components\search;
use skewer\base\site\ServicePrototype;
use skewer\base\site_module\Parser;
use skewer\base\section\models\TreeSection;
use yii\helpers\ArrayHelper;


/**
 * Сервис для работы с СЕО компонентами
 */
class Service extends ServicePrototype {


    /**
     * Алиас к которому инкрементов будет примещиваться цифра при создании
     * @var null
     */
    public static $sOperatingAlias = null;

    /**
     * Флаг о том что был изменен алиас при сохранении.
     * @var bool
     */
    public static $bAliasChanged = false;

    /** @var array() Cлужебные слова */
    protected static $aFunctionSections = [
        '/design/',
        '/admin/',
        '/ajax/',
        '/download/',
        '/cron/',
        '/contentgenerator/',
        '/gateway/',
        '/local/',
        '/assets/',
        '/export/',
        '/files/',
        '/fonts/',
        '/images/',
        '/sitemap_files/'
    ];

    /**
     * Очищаем поисковый индекс
     * todo перенести в поиск
     */
    public static function rebuildSearchIndex(){
        search\SearchIndex::delete()->get();

        $aResourseList = search\Api::getResourceList();

        foreach($aResourseList as $name => $item){
            /** @var search\Prototype $oEngine */
            $oEngine = new $item();
            $oEngine->provideName( $name );
            $oEngine->restore();
        }
    }

    /**
     * Формат например aData = [
     *                  'alias'=>'/test',
     *                  'class_name'=>'CatalogViewer',
     *                  'object_id'=>5
     *                  ]
     * @param array $aData
     * @return array
     */
     public static function findUrlCollisions($aData = []){

        if (empty($aData)){
            /*Поиск любых дублей*/
            $sQuery =
                "SELECT href FROM search_index
            WHERE href
            IN (
            SELECT href
            FROM search_index
            WHERE href!=''
            GROUP BY href
            HAVING COUNT( href ) > 1
            )
            GROUP BY href";
        } else {
            /*Поиск по конкретной ссылке*/
            $sQuery =
                "
            SELECT href
            FROM search_index
            WHERE href='{$aData['alias']}'";

            /*Тут происходит исключение из выборки того объекта который проверям*/
            if (isset($aData['object_id']))
              $sQuery .= " AND object_id!='{$aData['object_id']}'";

            $sQuery .= " GROUP BY href";
        }

        $aCollisions = \Yii::$app->db->createCommand($sQuery)->queryAll();

         // служебные слова
         if (isset($aData['alias']) and  in_array($aData['alias'],self::$aFunctionSections)) {
             $aCollisions[]['href'] = $aData['alias'];
         }

        return $aCollisions;
    }

    /**
     * @param $sAlias
     * @param $iParent
     * @param $iId
     * @param $sEntity
     * @return bool true - есть коллизия / false - нет коллизии
     */
    public static function checkCollision($sAlias,$iParent,$iId,$sEntity){

        $sParentPath = Tree::getSectionAliasPath($iParent);

        $aData = [
            'alias'=>$sParentPath.$sAlias.'/',
            'class_name'=>$sEntity
        ];

        if ($iId)
            $aData['object_id']=$iId;

        $aCollisions = self::findUrlCollisions($aData);

        if (!empty($aCollisions)){
            self::$bAliasChanged = true;
            return true;
        }

        return false;

    }

    /**
     * Формирует уникальный алиас
     * @param $sAlias
     * @param $iId
     * @param $iParentId
     * @param $sEntity
     * @param int $iIteration
     * @return bool|null|string
     */
    public static function generateAlias($sAlias, $iId, $iParentId, $sEntity, $iIteration=0){

        if (!$sAlias)
            $sAlias = date('d-m-Y-H-i');

        if (is_null(self::$sOperatingAlias))
            self::$sOperatingAlias = $sAlias;

        $sAlias = self::$sOperatingAlias;

        if ($iIteration)
            $sAlias .= '-'.$iIteration;

        $bIsCollision = self::checkCollision($sAlias,$iParentId,$iId,$sEntity);

        if ($bIsCollision){
            $sAlias = self::generateAlias($sAlias, $iId, $iParentId, $sEntity, $iIteration+1);
            self::$bAliasChanged = true;
        }

        //Приборка после обработки
        self::$sOperatingAlias = null;

        return $sAlias;

    }

    /**
     * Обновляет поисковый индекс
     * @param $iTask
     * @throws \Exception
     * @return int
     */
    public static function makeSearchIndex( $iTask = 0 ){

        $oManager = QM\Manager::getInstance();

        /*Если есть ID задачи найдет задачу по ID, Если нет то по классу*/
        if ($iTask)
            $oTask = QM\Api::getTaskById($iTask);
        else
            $oTask = QM\Api::getTaskByClassName('\skewer\components\seo\SearchTask');

        if (!$oTask){
            /** Добавим новую задачу */
            $iTask = QM\Api::addTask([
                'class' => '\skewer\components\seo\SearchTask',
                'priority' => QM\Task::priorityHigh,
                'title' => 'search index update'
            ]);

            $oTask = QM\Api::getTaskById( $iTask );

        }

        if (!$oTask){
            throw new \Exception('Task not found');
        }

        $oManager->executeTask( $oTask );

        return (in_array( $oTask->getStatus(), [QM\Task::stFrozen, QM\Task::stWait]))?$iTask:0;

    }

    /**
     * Постановка задачи на обновление search index
     * @static
     * @return bool
     */
    public static function updateSearchIndex() {

        return QM\Api::addTask([
            'class' => '\skewer\components\seo\SearchTask',
            'priority' => QM\Task::priorityHigh,
            'title' => 'search index update'
        ]);

    }


    /**
     * Постановка задачи на обновление sitemap.xml
     * @static
     * @return bool
     */
    public static function updateSiteMap() {

        return QM\Api::addTask([
            'class' => '\skewer\components\seo\SitemapTask',
            'priority' => QM\Task::priorityHigh,
            'title' => 'sitemap update'
        ]);

    }


    /**
     * Обновляет карту сайта
     * @param $iTask
     * @throws \Exception
     * @return array
     */
    public static function makeSiteMap( $iTask = false){

        $oManager = QM\Manager::getInstance();

        if (!$iTask){

            $oTask = QM\Api::getTaskByClassName('\skewer\components\seo\SitemapTask');

            if (!$oTask){
                /** Добавим новую задачу */
                $iTask = QM\Api::addTask([
                    'class' => '\skewer\components\seo\SitemapTask',
                    'priority' => QM\Task::priorityHigh,
                    'title' => 'sitemap update'
                ]);

                $oTask = QM\Api::getTaskById( $iTask );
            } else {
                $iTask = $oTask->getId();
            }

        } else {
            $oTask = QM\Api::getTaskById( $iTask );
        }

        if (!$oTask){
            throw new \Exception('Task not found');
        }

        $oManager->executeTask( $oTask );

        return ['status' => $oTask->getStatus(), 'id' => $iTask];

    }


    public static function setNewDomainToSiteMap(){

        self::updateSiteMap();

        return true;
    }


    public static function updateRobotsTxt($sDomain){

        $out = self::getContentRobotsTxtFile( $sDomain );

        // -- save - rewrite file

        $filename = WEBPATH."robots.txt";

        if (file_exists($filename) && !is_writable($filename))
            throw new \Exception('Can\'t write robots.txt');

        if (!$handle = fopen($filename, 'w+'))   return false;

        if (fwrite($handle, $out) === FALSE)    return false;

        fclose($handle);

        return true;

    }

    /**
     * Получить содержимое robots.txt
     * (!!!контент, записываемый в файл при его перестроении, но не контент считанный из файла)
     * @param string - домен
     * @return string
     */
    public static function getContentRobotsTxtFile( $sDomain ){

        $aConfigPaths = \Yii::$app->getParam(['parser', 'default', 'paths']);
        if (is_array($aConfigPaths) && isset($aConfigPaths[0])) {
            $aConfigPaths = $aConfigPaths[0];
        }

        if ( ( RobotsTxt\Api::getSysVar('content_ovveriden', '0') === '1' ) ){

            $sRobotsRules = RobotsTxt\Api::getSysVar('robots_content', '');
            $sRobotsRules = $sRobotsRules . "\r\n" . Parser::parseTwig('robots_host.twig', [
                'site_url'  => WEBPROTOCOL . $sDomain,
                'site_host' => SysVar::get('enableHTTPS') ? WEBPROTOCOL . $sDomain : $sDomain
            ], $aConfigPaths);

            $sRobotsContent = $sRobotsRules;

        } else {

            $sRobotsContent = self::generateDefaultContentRobotsTxtFile( $sDomain ) ;

        }

        return $sRobotsContent;

    }

    /**
     * Пути системных разделов
     * @return array
     */
    private static function getSystemPaths()
    {
        $aPaths = [];
        $aServices = [];
        foreach(['search', 'card', 'auth', 'profile'] as $key)
            $aServices[$key] = \Yii::$app->sections->getValues($key);

        if ($aServices)
            $aPaths = TreeSection::find()
                ->where(['id' => $aServices])
                ->asArray()
                ->all();

        return ArrayHelper::map($aPaths, 'alias_path', 'alias_path');
    }

    /**
     * Сгенирирует содержимое (по умолчанию) файла robots.txt
     * @param string $sDomain  - домен сайта
     * @param bool $bOnlyRules - вернуть только правила (disallow/allow)
     * @return string
     */
    public static function generateDefaultContentRobotsTxtFile($sDomain, $bOnlyRules = false){

        // набор предустановленных в конфиге путей для парсинга
        $aConfigPaths = \Yii::$app->getParam(['parser', 'default', 'paths']);
        if (is_array($aConfigPaths) && isset($aConfigPaths[0])) {
            $aConfigPaths = $aConfigPaths[0];
        }

        $bExistDomain = false;

        if ($sDomain)
            $bExistDomain = true;

        if (!Server::isProduction())
            $bExistDomain = false;

        $aData = [
            'domain_exist' => $bExistDomain,
            'site_host'    => SysVar::get('enableHTTPS') ? WEBPROTOCOL . $sDomain : $sDomain,
            'site_url'     => WEBPROTOCOL . $sDomain,
            'pattern'      => Api::getRobotsPattern(),
            'system_service' => self::getSystemPaths(),
            'bOnlyRules'   => $bOnlyRules
        ];

        $sOut = Parser::parseTwig('robots.twig', $aData, $aConfigPaths);

        return $sOut;

    }

}
