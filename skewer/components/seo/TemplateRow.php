<?php

namespace skewer\components\seo;


use skewer\base\SysVar;
use skewer\components\i18n\Messages;
use skewer\base\orm;

class TemplateRow extends orm\ActiveRecord {

    public $id = 'NULL';

    /** @var string Псевдоним шаблона */
    public $alias = '';

    /** @var string Динамическая часть псевдонима */
    public $extraalias = '';
    public $name = '';
    public $title = '';
    public $description = '';
    public $keywords = '';
    public $info = '';

    /** @var string $altTitle
     * Альтернативный текст (для изображений)
     */
    public $altTitle = '';

    /** @var string $nameImage Название изображения */
    public $nameImage = '';

    function __construct() {
        $this->setTableName( 'seo_templates' );
    }


    /**
     * @todo @fixme ПЕРЕПИСАТЬ
     * Парсинг поля $sField по параметрам $aData
     * @param string $sField Имя поля
     * @param array $aData Метки для замены
     * @return mixed
     */
    public function parseTpl( $sField, $aData = array() ) {

        $sOut = $this->$sField;

        $list = Messages::getSEOLabels();

        if ( $aData ) {
            foreach ( $aData as $key => $val ) {

                if ( preg_match('/^[A-z_\.]+$/', $key) ) {
                    if ( isSet($list[$key]) && $list[$key] ) {
                        $aLabels = $list[$key];
                        foreach ( $aLabels as &$sLabel )
                            $sLabel = '[' . $sLabel . ']';
                    } else {
                        $aLabels = [ '[' . $key . ']' ];
                    }
                } else {
                    $aLabels = [ '[' . $key . ']' ];
                }


                $sOut = str_replace( $aLabels, $val, $sOut );
            }
        }

        return $sOut;
    }
    
    public function preSave(){
        \Yii::$app->router->updateModificationDateSite();        
    }
    
    public function preDelete(){
        \Yii::$app->router->updateModificationDateSite();
    }


}