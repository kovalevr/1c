<?php

namespace skewer\components\i18n\command\delete_branch;


use skewer\components\search\SearchIndex;
use skewer\components\seo\Service;

/**
 * Перестроение поиска
 */
class SearchUpdate extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        Service::rebuildSearchIndex();
        SearchIndex::update()->set('status',0)->where('status',1)->get();
        Service::updateSearchIndex();
        Service::updateSiteMap();
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }

}