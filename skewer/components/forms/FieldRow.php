<?php

namespace skewer\components\forms;


use skewer\base\orm;
use skewer\helpers\Files;
use skewer\helpers\Validator;

/**
 * Class FieldRow
 * @package skewer\components\forms
 *
 * @property string $label_class Виртуальное свойства для шаблона, появляющееся в методе \skewer\components\forms\Row::getFields()
 */
class FieldRow extends orm\ActiveRecord {

    public $param_id = 'NULL';
    public $form_id = '';
    public $param_section_id = '';
    public $param_name = '';
    public $param_title = '';
    public $param_description = '';
    public $param_type = '';
    public $param_required = '';
    public $param_default = '';
    public $param_maxlength = '';
    public $param_validation_type = '';
    public $param_depend = '';
    public $param_man_params = '';
    public $param_priority = '';
    public $label_position = 'left';
    public $new_line = 1;
    public $width_factor = 1;
    public $no_send = 0;

    function __construct() {
        $this->setTableName( 'forms_parameters' );
        $this->setPrimaryKey( 'param_id' );
        $this->param_title = \Yii::t( 'forms', 'new_param');
        $this->param_type = 1;
        $this->param_validation_type = 'text';
        $this->param_maxlength = 255;
    }

    public function getType() {
        return isSet( FieldTable::$aTypes[$this->param_type] ) ? FieldTable::$aTypes[$this->param_type] : '';
    }

    public function getType4ExtJS() {
        return isSet( FieldTable::$aTypes4ExtJS[$this->param_type] ) ? FieldTable::$aTypes4ExtJS[$this->param_type] : 'string';
    }

    public function getViewItems() {
        return FieldTable::parse( $this );
    }

    public function getClassVal() {
        return isSet($this->label_class) ? $this->label_class : '1';
    }

    /**
     * Возвращает максимальный размер загружаемого файла для поля в мегабайтах
     * @return int
     */
    public function getMaxFileSize(){
        if (!$this->param_maxlength){
            $iMaxUploadSize = FieldTable::getUploadMaxSize();
        }else{
            $iMaxUploadSize = (FieldTable::getUploadMaxSize())?min(FieldTable::getUploadMaxSize(), $this->param_maxlength):$this->param_maxlength;
        }

        return $iMaxUploadSize;
    }

    /**
     * Приведение имени поля к уникальному значению в рамках формы
     */
    private function setUniqueName() {

        $this->param_name = Files::makeURLValidName( $this->param_name, false );

        $iNum = 0;

        $bFlag = true;

        do {

            $sCurAlias = $this->param_name . ( $iNum ? $iNum : '' );

            $iCount = FieldTable::find()
                ->where( 'param_name', $sCurAlias )
                ->where( 'form_id', $this->form_id )
                ->where( 'param_id<>?', $this->param_id )
                ->getCount();

            if ( !$iCount ) {

                $this->param_name = $sCurAlias;

                $bFlag = false;

            }

            $iNum++;

            if ( $iNum > 300 ) $bFlag = false;

        } while( $bFlag );

    }


    private function setPosition() {

        if ( !$this->param_priority ) {

            // todo реализовать max() дла запросов
            $aItems = FieldTable::find()
                ->where( 'form_id', (int)$this->form_id )
                ->getAll();

            $iPos = 0;

            /* @var $oFieldRow FieldRow */
            foreach ( $aItems as $oFieldRow )
                $iPos = ( $iPos < $oFieldRow->param_priority ) ? (int)$oFieldRow->param_priority : $iPos;

            $this->param_priority = $iPos + 1;
        }

    }


    public function preSave() {

        if ( !$this->param_maxlength )
            $this->param_maxlength = 255;

        if ( !$this->param_name )
            $this->param_name = $this->param_title;

        $this->setUniqueName();

        $this->setPosition();

    }


    public function validate( $sValue ) {

        if ( is_array($sValue) ) return false;

        $iMin = 1;
        $iMax = $this->param_maxlength;

        if ( trim($sValue)=='' ) // Если значение пустое
            if (!$this->param_required) // .. и поле не обязательное, то простить валидацию
                return true;
            else { // ... и поле обязательное, то ошибка заполнения
                $this->addError(\Yii::t('forms', 'err_empty_field'));
                return false;
            }

        switch ( $this->param_validation_type ) {

            case 'text':
                /*Отрезание служебных символов*/
                $sValue = str_replace("\r",'',$sValue);
                $sValue = str_replace("\n",'',$sValue);
                $sValue = str_replace("\t",'',$sValue);

                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true;
                break;

            case 'digits':
                if (preg_match("/^\d{".$iMin.','.$iMax."}$/",$sValue)) return true;
                break;

            case 'number':
                if (preg_match("/^[\-]?\d+[\.]?\d*$/",$sValue)) return true;
                break;

            case 'email':
                if ( Validator::isEmail($sValue) ) return true;
                $this->addError( \Yii::t('forms', 'err_incorrect_email') );
                break;

            case 'url':
                if (filter_var($sValue, FILTER_VALIDATE_URL) !== false)
                    if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true;
                break;

            case 'phone':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true; # сейчас только как текст
                break;

            case 'date':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true; # сейчас только как текст
                break;

            case 'creditcard':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true; # сейчас только как текст
                break;

            case 'login':
                if (preg_match("/^[\da-z\_\-]{".$iMin.','.$iMax."}$/i",$sValue)) return true;
                break;

            case 'md5':
                if (preg_match("/^[\da-z]{32}$/i",$sValue)) return true;
                break;
        }// switch

        // Если ошибка ещё не задана, то задать
        $this->getError() or $this->addError( rtrim(\Yii::t('forms', 'ans_validation'), '!') );

        return false;
    }


    /**
     * Формерует массив для списка из значений по умолчанию
     * @return array
     */
    public function parseDefaultAsList() {

        $aRows = explode( ';', trim( preg_replace( '/\x0a+|\x0d+/Uims', '', $this->param_default ) ) );
        $aRows = array_diff( $aRows, array('') );
        $aItems = array();

        foreach ( $aRows as $sRow ) {

            if ( strpos( $sRow, ':' ) )
                list( $sValue, $sTitle ) = explode( ':', $sRow );
            else
                $sValue = $sTitle = $sRow;

            $aItems[ $sValue ] = $sTitle ;
        }

        return $aItems;
    }

    /**
     * Отдает true если нужно показать стандартный placeholder
     */
    public function showDefaultPlaceholder() {
        $param_man_params = str_replace('data-hide-placeholder', '', $this->param_man_params);
        return (!preg_match('/\bplaceholder=[\'"]/', $param_man_params) and $this->label_position=='none' );
    }

}