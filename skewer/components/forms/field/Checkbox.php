<?php

namespace skewer\components\forms\field;


class Checkbox extends Prototype {

    public $type = 'checkbox';
    public $clsSpec = 'form__checkbox';

    protected $sTpl = 'checkbox.twig';

} 
