<?php

namespace skewer\components\forms;


use skewer\base\orm;
use skewer\base\ft;
use skewer\helpers\Files;


class Row extends orm\ActiveRecord {

    public $form_id = 'NULL';
    public $form_title = '';
    public $form_name = '';
    public $form_handler_type = 'toBase';
    public $form_handler_value = '';
    public $form_active = 1;
    public $form_captcha = 1;
    public $form_is_template = 1;
    public $form_answer = 0;
    public $form_redirect = '';
    public $form_agreed = 0;
    public $form_target = '';
    public $form_target_google = '';
    public $form_send_crm = 0;
    public $form_template = '';
    public $form_succ_answer = '';
    public $form_sys = '';
    public $form_button = '';
    public $last_modified_date = '';
    public $replyTo = 0;

    /** @var int Выводить фразу "* - обязательные для заполнения поля"  */
    public $form_show_required_fields = 1;

    /** @var int Выводить заголовок формы?  */
    public $form_show_header = 0;

    function __construct() {
        $this->setTableName( 'forms' );
        $this->setPrimaryKey( 'form_id' );
        $this->form_title = \Yii::t( 'forms', 'new_form');
    }


    public function getId() {
        return $this->form_id;
    }

    public function getAddData() {

        $aValues = orm\Query::SelectFrom( 'forms_add_data' )
            ->where( 'form_id', $this->form_id )
            ->asArray()->getOne();

        if ( !$aValues )
            return '';

        return $aValues;
    }

    /**
     * Список полей формы
     * @return FieldRow[]
     */
    public function getFields() {

        $aOut = [];

        /** @var FieldRow[] $aFieldList */
        $aFieldList = FieldTable::find()
            ->where( 'form_id', $this->form_id )
            ->order( 'param_priority' )
            ->getAll();

        // класс для сетки
        $sum = 1;

        foreach ( $aFieldList as $iKey => $oFieldRow ) {

            if ( $oFieldRow->new_line ) {
                $i = $iKey + 1;
                $sum = $oFieldRow->width_factor;

                // Здесь высчитывается максимальное число полей формы, расположенных на одной строке, на базе чего формируется имя css-класса
                while ( $i < count($aFieldList) ) {
                    if ( $aFieldList[$i]->new_line )
                        break;
                    $sum += $aFieldList[$i]->width_factor;
                    $i++;
                }

                if ( $sum > 5 ) $sum = 5;
            }

            $oFieldRow->label_class       = ($sum > 1) ? $oFieldRow->width_factor . '-' . $sum : '1';
            $oFieldRow->param_title       = \Yii::tSingleString($oFieldRow->param_title);
            $aOut[$oFieldRow->param_name] = $oFieldRow;
        }


        return $aOut;
    }


    public function preDelete() {

        // удаление полей
        $aFieldList = FieldTable::find()->where( 'form_id', $this->form_id )->getAll();

        /* @var $oFieldRow FieldRow */
        foreach ( $aFieldList as $oFieldRow )
            $oFieldRow->delete();

        // todo удаление таблицы для toBase
    }


    public function preSave() {

        $this->last_modified_date = date( "Y-m-d H:i:s", time() );

        if ( $this->form_handler_type == 'toBase' ) {

            if ( !$this->form_name )
                $this->form_name = $this->form_title;

            // todo формировать уникальное имя
            $this->form_name = Files::makeURLValidName( $this->form_name, false );


            $oFormModel = ft\Entity::get( $this->form_name )
                ->clear()
                ->setTablePrefix( 'frm_' );

            $aFieldList = $this->getFields();

            foreach ( $aFieldList as $oFieldRow ) {
                // todo
                $sFieldType = sprintf(
                    'varchar(%s)',
                    $oFieldRow->param_maxlength ? $oFieldRow->param_maxlength : 255
                );

                $oFormModel->addField( $oFieldRow->param_name, $sFieldType, $oFieldRow->param_title );
                //->setDefaultVal( 1 )
            }

            $oFormModel
                ->addField( '__add_date', 'date', 'Дата добавления' )
                ->addField( '__status', 'varchar(255)', 'Статус' )
                ->addField( '__section', 'varchar(255)', 'Раздел' )
            ;

            $oFormModel->save()->build();

        }

    }


    /**
     * Создание уникального имени для новой записи (при клонировании)
     * @return bool
     */
    public function setUniqName() {

        $sName = $this->form_name;

        $i = 0;

        while ( $i < 100 ) {

            $aItems = Table::find()->where( 'form_name', $sName . ( $i ? $i : '' ) )->getAll();

            if ( !is_array($aItems) || !count($aItems) ) {

                $this->form_name = $sName . ( $i ? $i : '' );

                return true;
            }

            $i++;
        }

        return false;
    }




}