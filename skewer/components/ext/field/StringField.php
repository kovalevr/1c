<?php

namespace skewer\components\ext\field;

/**
 * Редактор "строка"
 */
class StringField extends Prototype {

    function getView() {
        return 'str';
    }

}