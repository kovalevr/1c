<?php

namespace skewer\components\ext\field;

use skewer\components\auth\Auth;
use skewer\controllers\ContentgeneratorController;
use skewer\helpers\ImageResize;

/**
 * Редактор текста CKEditor
 */
class Wyswyg extends Text {

    const addConfigParamName = 'addConfig';

    public function getView() {
        return 'wyswyg';
    }

    /** @inheritdoc */
    public function getDesc() {

        // Обновить значение value поля, для дальнейшей обработки
        $this->processParams();

        $aAddConfig =  $this->getDescVal(self::addConfigParamName) ?: [];

        $aUser = Auth::getUserData('admin');

        $aCongig = $aAddConfig+[
                //'bodyClass' => '',
                'contentsCss' => [
                    \Yii::$app->getAssetManager()->getBundle(\skewer\build\Page\Main\Asset::className())->baseUrl . '/css/typo.css',
                    \Yii::$app->getAssetManager()->getBundle(\skewer\build\Cms\Frame\Asset::className())->baseUrl . '/css/wyswyg.css',
                    \Yii::$app->getAssetManager()->getBundle(\skewer\components\content_generator\Asset::className())->baseUrl. '/css/font-awesome.min.css',
                    \Yii::$app->getAssetManager()->getBundle(\skewer\components\content_generator\Asset::className())->baseUrl.'/css/blocks.compile.css',
                    \Yii::$app->getAssetManager()->getBundle(\skewer\libs\CKEditor\Asset::className())->baseUrl.'/css/only_wys.css'
                ],
                'addLangParams' => \skewer\build\Adm\Editor\Api::getAddLangParams4Wyswyg(),
                'sysmode'=>$aUser['systemMode']
            ];

        $this->setDescVal(self::addConfigParamName, $aCongig);

        // Отменить оборачивание картинок с размерами
        $this->setValue(ImageResize::restoreTags($this->getValue()));

        return parent::getDesc();
    }
}