/**
 * Класс для работы с историей
 */
Ext.define('Ext.sk.History',{

    extend: 'Ext.Component',
    path: 'history',

    tokenElementDelimiter: ';',
    tokenValueDelimiter: '=',

    initComponent: function(){

        var self=this;

        this.callParent();

        processManager.addProcess( this.path, this );

        // инициализация работы с историей браузера
        Ext.History.init();

        // добавить обработчик на изменение истории
        Ext.History.on('change', function(token) {

            if (token) {

                var data = self.getTokenData( token );

                // блок отправки
                processManager.setBlocker();

                // выполнить событие для сбора данных
                processManager.fireEvent( 'location_render', data );

                // снять блок
                processManager.unsetBlocker();

                // отослать данные, если есть
                processManager.postDataIfExists();

            }

        });

        // добавить подписку на изменение контрольной точки истории
        processManager.addEventListener('location_change',this.path,'onLocationChange');

    },

    /**
     * Возвращает объект разобранных данных токена
     */
    getTokenData: function( token ){

        if ( !token ) return {};

        var self = this,
            parts, length, i, data={}
            ;

        parts = token.split(self.tokenElementDelimiter);
        length = parts.length;

        for (i = 0; i < length ; i++) {
            var valParts = parts[i].split(self.tokenValueDelimiter);
            data[valParts[0]] = valParts[1];
        }

        return data;

    },

    /**
     * Возвращает объект разобранных данных текущего токена
     */
    getNowTokenData: function(){

        var me = this;

        return me.getTokenData( Ext.History.getToken() );

    },

    /**
     * при обновлении по F5 и прямом переходе
     */
    afterRender: function() {

        var oldToken = Ext.History.getToken();
        if ( oldToken )
            Ext.History.fireEvent('change',oldToken);

    },

    // обработчик изменения контрольной точки истории
    onLocationChange: function() {

        // инициализация параменных
        var elements = [],
            oldToken, newToken, pack;

        // выполнить событие для сбора данных
        processManager.fireEvent( 'location_set_value' );

        // взять их и запаковать в токен
        pack = processManager.getBuffer('locPack');

        // проверить, есть ли данные для отправки
        if ( pack ) {

            for ( var path in pack ) {
                if ( typeof pack[path] != 'object' )
                    elements.push(path+this.tokenValueDelimiter+pack[path])
            }

            // очистить очередь отправки
            processManager.clearBuffer('locPack');

        } else return false;

        // новый токен
        newToken = elements.join(this.tokenElementDelimiter);

        // старый
        oldToken = Ext.History.getToken();

        /**
         * если старый токен пуст или
         * новый является частью старого (но разным по набору параметров - включаем разделитель,
         *  чтобы отсечь включение по части значения)
         */
        if (oldToken === null || oldToken.search(newToken+this.tokenValueDelimiter) === -1) {
            Ext.History.add(newToken);
        }

    },

    /**
     * Вызывает перестроение url кода состояния
     */
    locationChange: function() {
        processManager.fireEvent('location_change');
    }

});
