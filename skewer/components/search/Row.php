<?php

namespace skewer\components\search;

use skewer\base\orm;

/**
 * AR класс для работы с записями поискового индекса
 */
class Row extends orm\ActiveRecord {

    function __construct() {
        $this->setTableName( 'search_index' );
        $this->setPrimaryKey( 'id' );
    }

    /** @var int id записи */
    public $id = 0;

    /** @var string текст для поиска */
    public $search_text = '';

    /** @var string Текст, показываемый пользователю */
    public $text = '';

    /** @var string заголовок */
    public $search_title = '';

    /**
     * @var int статус
     * 0 - новая запись (стоит в очереди на обработку)
     * 1 - обработана
     */
    public $status = 0;

    /** @var bool флаг использования записи при поиске */
    public $use_in_search = false;

    /** @var bool флаг использования записи в sitemap */
    public $use_in_sitemap = false;

    /** @var string ссылка */
    public $href = '';

    /** @var string имя класса */
    public $class_name = '';

    /** @var int id объекта для класса */
    public $object_id = 0;

    /** @var string Язык */
    public $language = '';

    /** @var int id раздела для вывода */
    public $section_id = 0;

    /** @var string дата модификации */
    public $modify_date = '';

    /** @var float Приоритет(используется в Sitemap)
     * Вынесен сюда потому что формируется динамичиски */
    public $priority = 0.0;

    /** @var string Частота(используется в Sitemap)
     * Вынесен сюда потому что формируется динамичиски
     */
    public $frequency = '';

    public function save() {

        $this->text = $this->search_text;
        // Добавить в поиск заголовок
        $this->search_text = $this->search_title . ' ' . $this->search_text;

        Api::prepareSearchText($this->search_text, true);

        return parent::save();
    }

    /**
     * @inheritDoc
     */
    public function delete() {
        $res = parent::delete();
        if ( $res )
            $this->id = 0;
        return $res;
    }

} 