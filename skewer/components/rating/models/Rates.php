<?php

namespace skewer\components\rating\models;

use Yii;

/**
 * Модель для таблицы rates
 *
 * @property integer $id
 * @property string $rating_name Идентификатор голосования = Имени модуля
 * @property integer $object_id Идентификатор объекта голосования
 * @property integer $rate Величина голоса
 * @property string $ip IP адрес проголосовавшего
 * @property string $date Дата голосования
 * @property string url URL адрес с которого производилось голосование
 *
 * @method static Rates|null findOne($condition)
 */
class Rates extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'rates';
    }
}