<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 03.06.2016
 * Time: 16:10
 */
namespace skewer\components\targets\types;

use skewer\base\ui\StateBuilder;
use skewer\components\targets\models\Targets;

class Google extends Prototype{

    public function getType(){
        return 'google';
    }

    public function getFormBuilder($oTargetRow){

        // создаем форму

        $aNameParams = [];
        if (!is_null($oTargetRow->id))
            $aNameParams['disabled']='disabled';

        $oFormBuilder = StateBuilder::newEdit();
        $oFormBuilder->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('ReachGoal', 'field_title'), 'string')
            ->field('category', \Yii::t('ReachGoal', 'field_category'), 'string')
            ->field('name', \Yii::t('ReachGoal', 'field_name'), 'string',$aNameParams)
            ->field('type', \Yii::t('ReachGoal', 'field_type'), 'hide')

            ->buttonSave()
            ->buttonBack();

        if ( $oTargetRow->id ) {
            $oFormBuilder
                ->buttonSeparator('->')
                ->buttonDelete();
        }

        $oFormBuilder->setValue($oTargetRow->getAttributes());

        return $oFormBuilder;

    }

    public function getNewTargetRow($aData = array()) {

        return Targets::getNewRow($aData,$this->getType());
    }

    public function getParams(){
        return [];
    }

    public function setParams($aData){}

}