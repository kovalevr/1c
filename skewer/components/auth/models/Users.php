<?php

namespace skewer\components\auth\models;

use skewer\base\site;
use skewer\components\auth\Auth;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property integer $global_id
 * @property string $login
 * @property string $pass
 * @property integer $group_policy_id
 * @property integer $active
 * @property string $reg_date
 * @property string $lastlogin
 * @property string $name
 * @property string $email
 * @property string $postcode
 * @property string $address
 * @property string $phone
 * @property string $user_info
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const ERR_USER = 'auth_err';
    
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public static function defaultLogin()
    {
        return 'default';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ( !site\Type::isShop() ) {
            return [
                [['login'], 'required'],
                [['global_id', 'group_policy_id', 'active', ], 'integer'],
                [['lastlogin'], 'safe'],
                [['login', 'name', 'email'], 'string', 'max' => 40],
                [['pass'], 'string', 'max' => 32],
                [['group_policy_id'], 'default', 'value' => 3],
            ];
        }
         else {
             return [
                 [['login'], 'required'],
                 [['global_id', 'group_policy_id', 'active'], 'integer'],
                 [['reg_date', 'lastlogin'], 'safe'],
                 [['user_info'], 'string'],
                 [['login', 'name', 'email', 'postcode'], 'string', 'max' => 40],
                 [['pass'], 'string', 'max' => 32],
                 [['address'], 'string', 'max' => 255],
                 [['phone'], 'string', 'max' => 20],
                 [['group_policy_id'], 'default', 'value' => 3],
             ];
         }

    }

    public static function updFieldsFilter(){

        return ['id','group_policy_id','name','email','active'];

    }

    public static function insertFieldsFilter(){

        return ['id','login','pass','pass2','group_policy_id','name','email','active'];

    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'global_id' =>  \Yii::t('auth', 'global_id'),
            'login' =>      \Yii::t('auth', 'login'),
            'pass' =>       \Yii::t('auth', 'pass'),
            'group_policy_id' => \Yii::t('auth', 'group_policy_id'),
            'active' =>     \Yii::t('auth', 'active'),
//            'reg_date' =>   \Yii::t('auth', 'reg_date'),
            'lastlogin' =>  \Yii::t('auth', 'lastlogin'),
            'name' =>       \Yii::t('auth', 'name'),
            'email' =>      \Yii::t('auth', 'email'),
            'postcode' =>   \Yii::t('auth', 'postcode'),
            'address' =>    \Yii::t('auth', 'address'),
            'phone' =>      \Yii::t('auth', 'phone'),
            'user_info' =>  \Yii::t('auth', 'user_info'),
        ];
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        $this->login = strtolower($this->login);
        $this->email = ($this->login)?$this->login:'';
        if ( site\Type::isShop() )
            $this->reg_date = date('Y-m-d H:i:s');
        return parent::save($runValidation, $attributeNames);
    }//func


    public function validate($attributeNames = null, $clearErrors = true)
    {
        $this->login = strtolower($this->login);
        $oItem = ($this->id)?self::findOne(['login' => $this->login,['not', ['id' => $this->id]]]):self::findOne(['login' => $this->login]);
        $aError = [];
        
        if ($oItem) {
            $aError[] = \Yii::t('auth', 'alredy_taken');
        }

        if (!$this->pass) {
            $aError[] = \Yii::t('auth', 'bad_password');
        }

        // проверка сложности пароля
        if (mb_strlen($this->pass) < 6) {
            $aError[] = \Yii::t('auth', 'err_short_pass');
        }

        if ($aError) {
            parent::addError(self::ERR_USER,$aError);
            return false;
        }

        return parent::validate($attributeNames,$clearErrors);
	}//func

    public function getValidateErrorMessage()
    {
        return parent::getErrors(self::ERR_USER);
    }//ready
}
