<?php

namespace skewer\base\ft;

/**
 * Класс для хранения констант для модификаторов
 * Class Modificator
 * @package skewer\base\ft
 */
class Modificator {

    const beforeAdd = 10;
    const afterAdd = 11;

    const beforeUpd = 20;
    const afterUpd = 21;

    const beforeDel = 30;
    const afterDel = 31;

}