<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 28.02.2017
 * Time: 18:15
 */

namespace skewer\base\ui\builder;
use skewer\components\ext\ShowView;

class ShowBuilder extends Prototype
{
    /** @var ShowView  */
    protected $oForm = null;

    /**
     * Конструктор
     * @param null $oInterface
     */
    function __construct($oInterface = null ) {

        if ( is_null($oInterface) )
            $this->oForm = new ShowView();
        else
            $this->oForm = $oInterface;

    }
}