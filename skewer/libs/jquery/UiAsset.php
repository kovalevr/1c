<?php
namespace skewer\libs\jquery;

use yii\web\AssetBundle;
use yii\web\View;

class UiAsset extends  AssetBundle{

    public $sourcePath = '@skewer/libs/jquery/web/';
    public $css = [
    ];
    public $js = [
        'jquery-ui.min.js',
        'jquery-ui-tabs.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\libs\jquery\Asset'
    ];
}