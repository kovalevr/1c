<?php
namespace skewer\libs\datepicker;


use yii\web\AssetBundle;
use yii\web\View;

class Asset extends  AssetBundle{

    public $sourcePath = '@skewer/libs/datepicker/web/';
    public $css = [
        'jquery.datepicker.css',
    ];
    public $js = [
        'jquery.datepicker.js',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\libs\jquery\Asset'
    ];

    public function init(){

        $this->js[] = 'jquery.datepicker-' . \Yii::$app->language . '.js';

        parent::init();

    }
}