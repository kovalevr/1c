<?php

namespace skewer\libs\Compress;

use JShrink\Minifier;
use skewer\base\section\Parameters;
use skewer\build\Page\Main\Asset;

class ChangeAssets {

    static $compile = ".compile.css";
    const NAMEPARAM ='compression';
    //папки с файлами, пути к которым менять не нужно
    static $aFolder = ['files','fonts'];

    
    //изменение путей к картинкам
    public static function parseDataCSS($to) {
        $bCompress = Parameters::getValByName(\Yii::$app->sections->root(),'.',ChangeAssets::NAMEPARAM,true);
        $sContent = file_get_contents($to);
        if ($bCompress) {
            $sContent = self::changePath($sContent,$to);
        }
        return $sContent;

    }
    public static function jsMin($to) {
        $bCompress = Parameters::getValByName(\Yii::$app->sections->root(),'.',ChangeAssets::NAMEPARAM,true);
        $sContent = file_get_contents($to);
        if ($bCompress) {
            require_once('Minifier.php');

            $sContent = Minifier::minify($sContent, array('flaggedComments' => false));
        }
        return $sContent;
    }

    public static function cssMin($sContent) {
        $bCompress = Parameters::getValByName(\Yii::$app->sections->root(),'.',ChangeAssets::NAMEPARAM,true);
        if ($bCompress) {
            require_once('CssMin.php');

            $sContent = \CssMin::minify($sContent);
        }
        return $sContent;
    }

    public static function changePath($content,$allPath) {
        $sPattern = "/url?\(([^\)]+)+/i";
        $aUrls = array();
        preg_match_all($sPattern,$content,$aUrls, PREG_PATTERN_ORDER);

        if ($aUrls[1]) {
            $aUrls[1] = array_unique($aUrls[1]);
            foreach ($aUrls[1] as $sValue) {
                $sPathShort = '';
                //случай, когда файлы лежат прямо в папке assets/number_folder
                $sPattern = "/\/+[^+\/]+\//";
                preg_match($sPattern,$sValue,$aFolder);
                $sFolder = ($aFolder)?str_replace('/','',$aFolder[0]):'';

                if (stristr($sValue,'../')||(!($sFolder))) {
                        $aPatternPath = "/assets\/([^\)]+)/i";
                        $aPathLong = array();
                        preg_match($aPatternPath,$allPath,$aPathLong);
                        $iPathShort = strrpos($aPathLong[1],'/');
                        $sPathShort = ($iPathShort)?"/assets/".substr($aPathLong[1],0,$iPathShort): "/assets/".$aPathLong[1];
                        $sPathShort .='/';
                        $sPathShort = str_replace('css/','',$sPathShort);
                }
                $sValue = str_replace('"','',$sValue);
                $sValue = str_replace('\'','',$sValue);
                $sValueChange = str_replace('../','',$sValue);
                $strChange = $sPathShort.$sValueChange;
                $content = str_replace($sValue,$strChange,$content);

            }
        }
        return $content;
    }

    /**
     * Получение только имени файла. 
     * Фиксация внешних путей до файлов
     * @param $strFullName - путь до файла ('(css|js|http://)/...')
     * @param $strType - тип файла
     * @return bool
     */
    public static function getOnlyName(&$strFullName,$strType='') {
        if ($strType) {
            if (strstr($strFullName,'http')||strstr($strFullName,'https')) {
                switch ($strType) {
                    case 'js':
                        Asset::$aOutLinkJs[] = $strFullName;
                        break;
                    case 'css':
                        Asset::$aOutLinkCss[] = $strFullName;
                        break;
                }
            return false;
            }
        }
            
        $symbol = strpos($strFullName,'/');
        if ($symbol) {
            $sNameWith = substr($strFullName,0,strrpos($strFullName,'.'));
            $strFullName = substr($sNameWith,$symbol+1);
        } else
            $strFullName = substr($strFullName,0,strrpos($strFullName,'.'));

        return true;
    }


    /**
     * Cбор путей js и css
     * @param $sPathFull - путь до файла
     * @param $strType - тип файла
     */
    public static function getOnlyPath($sPathFull,$strType='') {
        if ($sPathFull) {
            $sBasePath = \Yii::$app->basePath;
            $sPathFile = $sBasePath.'/web'.explode('?',$sPathFull)[0];
            switch ($strType) {
                case 'js':
                    if (ChangeAssets::getOnlyName($sAssetPath,$strType)) {
                        Asset::$jsHash .= $sAssetPath;
                        Asset::$jsPath[] = $sPathFile;
                    }
                    break;
                case 'css':
                    $sName = $sPathFull;
                    if (ChangeAssets::getOnlyName($sName,$strType)) {
                        if ((in_array($sPathFull,Asset::$cssIgnore))&&(!in_array($sPathFile,Asset::$cssIgnorePath))) {
                            Asset::$cssIgnorePath[] =  $sPathFile;
                        } elseif ((!in_array($sPathFull,Asset::$cssPath))) {
                            Asset::$cssHash .= $sName;
                            Asset::$cssPath[] = $sPathFile;
                        }
                    }
                    break;
            }
        }
     }


}