$.extend($.fancybox.defaults.tpl, {
    closeBtn: '<a title="Schließen" class="fancybox-item fancybox-close" href="javascript:;"></a>',
    next:'<a title="Weiter" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
    prev:'<a title="Zurück" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
});
