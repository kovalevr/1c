$.extend($.fancybox.defaults.tpl, {
    closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
    next:'<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
    prev:'<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
});
