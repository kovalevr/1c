<?php
namespace skewer\build\Adm\FAQ;

use skewer\base\section\Tree;
use skewer\base\site\Site;
use skewer\build\Adm\FAQ\ar;
use skewer\build\Page\FAQ;
use skewer\components\seo\SeoPrototype;
use yii\helpers\ArrayHelper;

class Seo extends SeoPrototype{

    public static function getGroup(){
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public static function getAlias(){
        return 'faqDetail';
    }

    /**
     * @return array
     */
    public function extractReplaceLabels(){

        return array(
            'label_faq_title_upper' => strip_tags(ArrayHelper::getValue($this->aDataEntity, 'content', '')),
            'label_faq_title_lower' => mb_strtolower(strip_tags(ArrayHelper::getValue($this->aDataEntity, 'content', '')))
        );
    }

    public function loadDataEntity(){
        $this->aDataEntity = FAQ\Api::getFAQById($this->iEntityId);
    }

    /**
     * @inheritdoc
     */
    protected function getSearchClassName(){
        return Search::className();
    }

    /**
     * @inheritdoc
     */
    public function getRecordWithinEntityByPosition($iPosition){

        $aResult = ar\FAQ::find()
            ->where(['parent' => $this->iSectionId])
            ->order('date_time', 'DESC')
            ->limit(1, $iPosition)
            ->get();

        if (!isset($aResult[0]))
            return false;

        /** @var ar\FAQRow $oCurrentRecord */
        $oCurrentRecord = $aResult[0];
        $this->setDataEntity($oCurrentRecord->getData());

        $aRow = array_merge($oCurrentRecord->getData(),[
            'url' => \Yii::$app->router->rewriteURL($oCurrentRecord->getUrl()),
            'seo' => $this->parseSeoData( ['sectionId' => $this->iSectionId] )
        ]);

        return $aRow;

    }


    /**
     * @inheritdoc
     */
    public function doExistRecord($sPath){

        $sTail = '';
        $iSectionId = Tree::getSectionByPath($sPath, $sTail);
        $sTail = trim($sTail, '/');

        return ( $oRecord = ar\FAQ::getByAlias($sTail, $iSectionId) )
            ? $oRecord->id
            : false
        ;

    }


}
