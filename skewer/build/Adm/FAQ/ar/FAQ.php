<?php

namespace skewer\build\Adm\FAQ\ar;


use skewer\base\orm;
use skewer\base\ft;
use skewer\base\section\Tree;
use skewer\build\Adm\FAQ\Api;
use skewer\components\search\GetModificationEvent;
use skewer\components\seo\Service;
use \yii\base\ModelEvent;
use skewer\build\Component;
use skewer\build\Adm\FAQ\Search;

class FAQ extends orm\TablePrototype{

    /** @var string Имя таблицы */
    protected static $sTableName = 'faq';

    protected static $sKeyField = 'id';

    public static function getNewRow($aData = array()) {
        $oRow = new FAQRow();
        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }

    protected static function initModel(){
        ft\Entity::get( static::$sTableName )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )

            ->addField('parent', 'int(11)', \Yii::t('faq', 'parent'))
            ->addField('date_time', 'datetime', \Yii::t('faq', 'date_time'))
            ->setDefaultVal( 'now' )

            ->addField('name', 'varchar(255)', \Yii::t('faq', 'name'))
            ->addField('email', 'varchar(255)', \Yii::t('faq', 'email'))
            ->addField('content', 'text', \Yii::t('faq', 'content'))
            ->addField('status', 'int(3)', \Yii::t('faq', 'status'))
            ->addField('city', 'varchar(255)', \Yii::t('faq', 'city'))
            ->addField('answer', 'text', \Yii::t('faq', 'answer'))

            ->addField('alias', 'varchar(255)', \Yii::t('faq', 'alias'))

            ->addDefaultProcessorSet()
            ->addColumnSet(
                'list',
                array( 'name','date_time','content', 'answer', 'status' )
            )

            ->save()
        ;
    }

    /**
     * Получить запись по alias
     * @param $aAlias          - alias записи
     * @param bool $iSectionId - id раздела
     * @return array|bool|orm\ActiveRecord
     */
    public static function getByAlias($aAlias, $iSectionId = false){
        return self::find()->where(['alias' => $aAlias] + (($iSectionId !== false)? ['parent' => $iSectionId]: []))->getOne();
    }

    /**
     * Удаление всех вопросов для раздела
     * @param ModelEvent $event
     */
    public static function removeSection( ModelEvent $event ) {
        FAQ::delete()->where( 'parent', $event->sender->id )->get();
    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param \skewer\components\search\GetEngineEvent $event
     */
    public static function getSearchEngine(\skewer\components\search\GetEngineEvent $event ) {
        $event->addSearchEngine( Search::className() );
    }

    public static function getLastMod(\skewer\components\modifications\GetModificationEvent $event ) {

        $event->setLastTime( Api::getLastModification() );
    }

    /**
     * Возвращает максимальную дату модификации сущности
     * @return array|bool
     */
    public static function getMaxLastModifyDate(){
        return (new \yii\db\Query())->select('MAX(`last_modified_date`) as max')->from(self::$sTableName)->one();
    }

}