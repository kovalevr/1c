<?php


namespace skewer\build\Adm\FAQ;


use skewer\base\orm\Query;
use skewer\build\Adm\FAQ\Seo as SeoFaq;
use skewer\components\search\Prototype;
use skewer\components\search\Row;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\components\seo;

class Search extends Prototype {
    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return 'FAQ';
    }

    /**
     * @inheritdoc
     */
    protected function update(Row $oSearchRow) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        /** @var ar\FAQRow $oFAQRow */
        $oFAQRow = ar\FAQ::find($oSearchRow->object_id);
        if (!$oFAQRow)
            return false;

        $bHidden = false;

        // нет данных - не добавлять в индекс
        if (!$oFAQRow->content)
            $bHidden = true;

        if ($oFAQRow->status != Api::statusApproved)
            $bHidden = true;

        $oSearchRow->search_text = $this->stripTags($oFAQRow->content);
        $oSearchRow->search_title = $this->stripTags($oFAQRow->content);
        $oSearchRow->section_id = $oFAQRow->parent;
        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = !$bHidden;
        $oSearchRow->use_in_sitemap = !$bHidden;

        $oSearchRow->language = Parameters::getLanguage($oFAQRow->parent);

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        $oSearchRow->href  = $sURL = \Yii::$app->router->rewriteURL(sprintf(
            '[%s][FAQ?%s=%s]',
            $oFAQRow->parent,
            $oFAQRow->alias ? 'alias' : 'id',
            $oFAQRow->alias ? $oFAQRow->alias : $oFAQRow->id
        ));
        
        $oSearchRow->modify_date = $oFAQRow->last_modified_date;

        $oSeoComponent = new SeoFaq($oFAQRow->id, $oSection->id, $oFAQRow->getData());
        $oSEOData = seo\Api::get( SeoFaq::getGroup(), $oSearchRow->object_id, $oSection->id );

        $oSearchRow->priority = ($oSEOData && !empty($oSEOData->priority))
            ? $oSEOData->priority
            : $oSeoComponent->calculatePriority();

        $oSearchRow->frequency = ($oSEOData && !empty($oSEOData->frequency))
            ? $oSEOData->frequency
            : $oSeoComponent->calculateFrequency();
        
        $oSearchRow->save();
        return true;
    }

    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',id  FROM faq";
        Query::SQL($sql);
    }

} 