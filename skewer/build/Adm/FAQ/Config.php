<?php

use skewer\base\site\Layer;
use skewer\base\section\models\TreeSection;
use skewer\components\search;

/* main */
$aConfig['name']     = 'FAQ';
$aConfig['title']    = 'Модуль вопросов и ответов';
$aConfig['version']  = '2.000a';
$aConfig['description']  = 'Модуль вопросов и ответов';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'faq';

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => skewer\build\Adm\FAQ\ar\FAQ::className(),
    'method' => 'removeSection',
];
$aConfig['dependency'] = [
    ['FAQ', Layer::PAGE],
];
$aConfig['events'][] = [
    'event' => skewer\components\modifications\Api::EVENT_GET_MODIFICATION,
    'class' => skewer\build\Adm\FAQ\ar\FAQ::className(),
    'method' => 'getLastMod',
];
$aConfig['events'][] = [
    'event' => search\Api::EVENT_GET_ENGINE,
    'class' => skewer\build\Adm\FAQ\ar\FAQ::className(),
    'method' => 'getSearchEngine',
];

return $aConfig;
