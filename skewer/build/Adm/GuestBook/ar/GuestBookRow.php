<?php
/**
 * User: Max
 * Date: 25.07.14
 */

namespace skewer\build\Adm\GuestBook\ar;

use skewer\base\orm;
use skewer\components\rating\Rating;
use skewer\build\Tool\Review;
use skewer\build\Page\CatalogViewer;

class GuestBookRow extends orm\ActiveRecord {
    public $id = 0;
    public $parent = 0;
    public $parent_class = '';
    public $date_time = 0;
    public $name = '';
    public $email = '';
    public $content = '';
    public $status = 0;
    public $city = '';
    public $company = '';
    public $photo_gallery = 0;
    public $on_main = 0;
    public $rating = 0;
    public $last_modified_date = '';

    function __construct() {
        $this->setTableName( 'guest_book' );
        $this->setPrimaryKey( 'id' );
    }

    public function preSave() {
        if ( !$this->date_time || ($this->date_time == 'null') )
            $this->date_time = date("Y-m-d H:i:s", time());

        /** @var GuestBookRow $oReviewOrigin */
        // Удалить старый рейтинг
        if ( $this->id and ($oReviewOrigin = GuestBook::find($this->id)) )
            self::removeRating($oReviewOrigin);

        // Добавить новый рейтинг
        if ( ($this->status == GuestBook::statusApproved) and ($this->parent_class == Review\Api::GoodReviews) and $this->rating )
            (new Rating(CatalogViewer\Module::getNameModule()))
                ->setCheck(false)
                ->addRate($this->parent, $this->rating)
            ;

        $this->last_modified_date = date( "Y-m-d H:i:s", time() );

        parent::preSave();
    }

    public function preDelete(){
        \Yii::$app->router->updateModificationDateSite();

        /** @var GuestBookRow $oReviewOrigin */
        // Удалить старый рейтинг
        if ( $this->id and ($oReviewOrigin = GuestBook::find($this->id)) )
            self::removeRating($oReviewOrigin);

        parent::preDelete();
    }

    /**
     * Удалить рейтинг к отзыву
     * @param self $oReview
     */
    private static function removeRating(self $oReview) {

        if ( ($oReview->parent_class == Review\Api::GoodReviews) and ($oReview->status == GuestBook::statusApproved) and $oReview->rating )
            (new Rating(CatalogViewer\Module::getNameModule()))
                ->removeRating($oReview->parent, $oReview->rating)
            ;
    }
}