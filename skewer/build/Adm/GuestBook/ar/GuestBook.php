<?php
/**
 * User: Max
 * Date: 25.07.14
 */

namespace skewer\build\Adm\GuestBook\ar;

use skewer\base\orm;
use skewer\base\ft;
use yii\base\ModelEvent;

class GuestBook extends orm\TablePrototype {

    protected static $sTableName = 'guest_book';
    protected static $sKeyField = 'id';

    /** статус "новый" */
    const statusNew = 0;

    /** статус "одобрен" */
    const statusApproved = 1;

    /** статус "отклонен" */
    const statusRejected = 2;

    /** вывод в карусель "выводить" */
    const carouselApproved = 1;

    protected static function initModel() {

        ft\Entity::get(self::$sTableName)
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField)
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)

            ->addField('rating','int(2)','review.field_rating')
            ->addField( 'parent', 'int(11)', 'review.field_parent' )
            ->addField( 'parent_class', 'varchar(20)', 'review.field_parent_class' )
            ->addField( 'date_time', 'datetime', 'review.field_date_time' )
            ->addField( 'name', 'varchar(128)', 'review.field_name' )
            ->addField( 'email', 'varchar(128)', 'review.field_email' )
            ->addField( 'content', 'text', 'review.field_content' )
            ->addField( 'status', 'int(1)', 'review.field_status' )
            ->addField( 'city', 'varchar(255)', 'review.field_city' )
            ->addField( 'company', 'varchar(255)', 'review.field_company' )
            ->addField( 'photo_gallery', 'int(11)', 'review.field_photo_gallery' )
            ->addField('on_main','int(1)','review.field_on_main')
            ->addField( 'last_modified_date', 'datetime', 'review.field_modifydate' )
            ->save();
    }

    public static function getNewRow($aData = array()) {
        $oRow = new GuestBookRow();
        if ($aData)
            $oRow->setData($aData);
        return $oRow;
    }

    /**
     * Удаляет отзывы вместе с удалением раздела
     * @param ModelEvent $event
     */
    public static function removeSection(ModelEvent $event) {

        /** @var GuestBookRow[] $aReviews */
        $aReviews = self::find()
            ->where('parent', $event->sender->id)
            ->getAll();

        if ($aReviews)
            foreach($aReviews as $oReview)
                $oReview->delete();
    }

    /**
     * Удаляет отзывы вместе с удалением товара
     * @param int $iIdGood id удаляемого товара
     */
    public static function removeGood($iIdGood) {

        /** @var GuestBookRow[] $aReviews */
        $aReviews = self::find()
            ->where('parent_class', \skewer\build\Tool\Review\Api::GoodReviews)
            ->andWhere('parent', $iIdGood)
            ->getAll();

        if ($aReviews)
            foreach($aReviews as $oReview)
                $oReview->delete();
    }

    /**
     * Возвращает максимальную дату модификации сущности
     * @return array|bool
     */
    public static function getMaxLastModifyDate(){
        return (new \yii\db\Query())->select('MAX(`last_modified_date`) as max')->from(self::$sTableName)->one();
    }
    
}