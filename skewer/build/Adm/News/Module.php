<?php

namespace skewer\build\Adm\News;


use skewer\components\auth\CurrentAdmin;
use skewer\build\Adm\News\models\News;
use skewer\base\ui;
use skewer\components\seo;
use skewer\build\Adm;
use Yii;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


/**
 * Class Module
 * @package skewer\build\Adm\News
 */
class Module extends Adm\Tree\ModulePrototype {

    // число элементов на страницу
    protected $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPageNum = 0;

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     */
    protected function preExecute() {

        // номер страницы
        $this->iPageNum = $this->getInt('page');

    }


    /**
     * Первичное состояние - спосок новостей для раздела
     */
    protected function actionInit() {
        // -- сборка интерфейса

        $news = News::find()
            ->where(['parent_section'=>$this->sectionId()])
            ->orderBy(['publication_date'=>SORT_DESC])
            ->limit($this->iOnPage)
            ->offset($this->iPageNum * $this->iOnPage)
            ->asArray()
            ->all();

        $iCount = News::find()
            ->where(['parent_section'=>$this->sectionId()])
            ->count();

        /**
         * @var News[] $news
         */

        $this->render(
            new view\Index([
                'items' => $news,

                'page' => $this->iPageNum,
                'onPage' => $this->iOnPage,
                'total' => $iCount
            ])
        );

    }

    /**
     * Сохраняет записи из спискового интерфейса
     */
    protected function actionSaveFromList() {

        $iId = $this->getInDataValInt( 'id' );

        $sFieldName = $this->get('field_name');

        $oRow = News::findOne(['id'=>$iId]);
        /** @var News $oRow */
        if ( !$oRow )
            throw new UserException( "Запись [$iId] не найдена" );

        $oRow->$sFieldName = $this->getInDataVal( $sFieldName );

        $oRow->save();

        $this->actionInit();
    }

    /**
     * Форма добавления
     */
    protected function actionNew() {

        $this->render(new view\Form([
            'item' => News::getNewRow()
        ]));

    }

    /**
     * Форма редактирования
     */
    protected function actionShow() {

        $aData = $this->get('data');

        if (isset($aData['id']))
            $iItemId = (int)$aData['id'];
        else
            $iItemId = ($this->getInnerDataInt('id'))? $this->getInnerDataInt('id') : 0;

        /**
         * @var \skewer\build\Adm\News\models\News $oNewsRow
         */
        $oNewsRow = News::findOne(['id'=>$iItemId]);

        if ( !$oNewsRow )
            throw new UserException('Item not found');

        $this->render(new view\Form([
            'item' => $oNewsRow
        ]));

    }


    /**
     * Сохранение новости
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );
        $iId = $this->getInDataValInt( 'id' );

        if ( !$aData )
            throw new UserException( 'Empty data' );

        if ( $iId ) {
            $oNewsRow = News::findOne(['id'=>$iId]);
            if ( !$oNewsRow )
                throw new UserException( "Запись [$iId] не найдена" );
            $aOldAttributes = $oNewsRow->getOldAttributes();
        } else {
            $oNewsRow = News::getNewRow();
            $aOldAttributes = $oNewsRow->getAttributes();
        }
        $oNewsRow->setAttributes($aData);
        $oNewsRow->parent_section = $this->sectionId();

        $oNewsRow->save();

        if (seo\Service::$bAliasChanged)
            $this->addMessage(\Yii::t('tree','urlCollisionFlag',['alias'=>$oNewsRow->news_alias]));

        // сохранение SEO данных
        seo\Api::saveJSData(
            new \skewer\build\Adm\News\Seo(ArrayHelper::getValue($aOldAttributes, 'id', 0), $this->sectionId(),$aOldAttributes),
            new \skewer\build\Adm\News\Seo($oNewsRow->id, $this->sectionId(),$oNewsRow->getAttributes()),
            $aData, $this->sectionId()
        );

        if ( $oNewsRow->id )
            $this->addModuleNoticeReport(\Yii::t('news', 'editNews'),$aData);
        else {
            $aData['id'] = $oNewsRow->id;
            $this->addModuleNoticeReport(\Yii::t('news', 'addNews'),$aData);
        }

        // вывод списка
        $this->actionInit();

    }


    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        if (!$iItemId)
            throw new UserException(Yii::t('news', 'error_id_not_found'));

        if (!$oNew = News::findOne($iItemId))
            throw new UserException(Yii::t('news','error_row_not_found',[$iItemId]));

        // удаление SEO данных
        seo\Api::del( 'news', $iItemId );

        $oSearch = new Search();
        $oSearch->deleteByObjectId($iItemId);

        $oNew->delete();

        // todo Перенести в afterDelete
        Yii::$app->router->updateModificationDateSite();

        // удаление SEO данных
        seo\Api::del( 'news', $iItemId );

        $oSearch = new Search();
        $oSearch->deleteByObjectId($iItemId);

        $this->addModuleNoticeReport( \Yii::t('news', 'deleteNews'), $aData );

        // вывод списка
        $this->actionInit();

    }


    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData( ui\state\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'page' => $this->iPageNum
        ) );

    }

}
