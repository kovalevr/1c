<?php

namespace skewer\build\Adm\News\models;

use skewer\base\section\Tree;
use skewer\build\Tool\Rss;
use skewer\components\seo\Api;
use skewer\components\seo\Service;
use skewer\helpers\ImageResize;
use skewer\helpers\Transliterate;
use Yii;
use yii\data\ActiveDataProvider;
use \yii\base\ModelEvent;
use skewer\build\Adm\News\Search;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $news_alias
 * @property integer $parent_section
 * @property string $publication_date
 * @property string $title
 * @property string $announce
 * @property string $full_text
 * @property integer $active
 * @property integer $on_main
 * @property integer $gallery
 * @property string $hyperlink
 * @property string $last_modified_date
 * @property string $url
 *
 * @property string $format_announce
 *
 * @method static News findOne($condition)
 */
class News extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['parent_section', 'title'], 'required'],
            [['parent_section', 'active', 'on_main','gallery'], 'integer'],
            [['publication_date', 'last_modified_date'], 'safe'],
            [['announce', 'full_text'], 'string'],
            [['news_alias', 'title', 'hyperlink'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'                 => Yii::t('news', 'field_id'),
            'news_alias'         => Yii::t('news', 'field_alias'),
            'parent_section'     => Yii::t('news', 'field_parent'),
            'publication_date'   => Yii::t('news', 'field_date'),
            'title'              => Yii::t('news', 'field_title'),
            'announce'           => Yii::t('news', 'field_preview'),
            'full_text'          => Yii::t('news', 'field_fulltext'),
            'active'             => Yii::t('news', 'field_active'),
            'on_main'            => Yii::t('news', 'field_onmain'),
            'gallery'            => Yii::t('news', 'field_gallery'),
            'hyperlink'          => Yii::t('news', 'field_hyperlink'),
            'last_modified_date' => Yii::t('news', 'field_modifydate'),
        ];
    }

    public static function getPublicNewsByAlias($sNewsAlias) {
        return News::findOne(['news_alias' => $sNewsAlias]);
    }

    public static function getPublicNewsById($iNewsId) {
        return News::findOne(['id' => $iNewsId]);
    }

    public function getFormat_Announce() {
        return str_replace('data-fancybox-group="button"', 'data-fancybox-group="news' . $this->id . '"', $this->announce);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public static function getPublicList($params) {
        $query = News::find()->andFilterWhere(['active' => 1])->orderBy([
                                                                            'publication_date' => ($params['order'] == 'DESC') ? SORT_DESC : SORT_ASC,
                                                                        ]);

        if (!$params['on_main'] && Yii::$app->getRequest()->getQueryParam('per-page')) {
            $params['on_page'] = Yii::$app->getRequest()->getQueryParam('per-page');
        }

        $dataProvider = new ActiveDataProvider([
               'query'      => $query,
               'pagination' => [
                   'pageSize' => (isset($params['on_page'])) ? $params['on_page'] : 10,
                   'page'     => $params['page'] - 1,
               ],
           ]);

        if (isset($params['on_page'])) {
            // добавим рулесы в UrlManager для правильного построителя ЧПУ
            // нужно для коректной генерации пагинатора
            News::routerRegister();
        }

        /* Если есть фильтр по дате */
        if (isSet($params['byDate']) && !empty($params['byDate'])) {
            $query->andFilterWhere(['>', 'publication_date', $params['byDate'] . ' 00:00:00']);
            $query->andFilterWhere(['<', 'publication_date', $params['byDate'] . ' 23:59:59']);
        }

        if (!$params['all_news'] && $params['section']) {
            $query->andFilterWhere(['parent_section' => $params['section']]);
        }

        if ($params['on_main']) {
            $query->andFilterWhere(['on_main' => 1]);
        }

        /**
         * @todo наверное можно проще
         */
        $aSections = Tree::getAllSubsection(\Yii::$app->sections->languageRoot());
        $query->andFilterWhere([ 'parent_section' => array_intersect_key($aSections, Tree::getVisibleSections()) ]);

        if ($params['future'])
            $query->andFilterWhere(['>', 'publication_date', date('Y-m-d H:i:s', time())]);

        return $dataProvider;
    }

    public static function routerRegister() {
        $url = Yii::$app->getRequest()->getPathInfo();
        $url = preg_replace('/page(.)*/', '${2}', $url);

        Yii::$app->getUrlManager()->addRules([$url . 'page/<page:[\w\.]+>' => 'site/index']);
    }

    public static function getNewRow($aData = array()) {
        $oRow = new News();

        $oRow->title            = \Yii::t('news', 'new_news');
        $oRow->publication_date = date("Y-m-d H:i:s", time());
        $oRow->active           = 1;

        $oRow->announce  = '';
        $oRow->full_text = '';
        $oRow->gallery = '';
        $oRow->hyperlink = '';
        $oRow->on_main   = 0;

        if ($aData)
            $oRow->setAttributes($aData);
        return $oRow;
    }

    public function beforeSave($insert) {

        if (!$this->news_alias)
            $sValue = Transliterate::change($this->title);
        else
            $sValue = Transliterate::change($this->news_alias);

        // к числам прибавляем префикс
        if (is_numeric($sValue)) {
            $sValue = 'news-' . $sValue;
        }

        // приводим к нужному виду
        $sValue = Transliterate::changeDeprecated($sValue);
        $sValue = Transliterate::mergeDelimiters($sValue);
        $sValue = trim($sValue, '-');

        $this->news_alias = Service::generateAlias($sValue, $this->id,$this->parent_section,'News');

        // format wyswyg fields
        if ($this->full_text && $this->parent_section)
            $this->full_text = ImageResize::wrapTags($this->full_text, $this->parent_section);

        if ($this->announce && $this->parent_section)
            $this->announce = ImageResize::wrapTags($this->announce, $this->parent_section);

        if ($this->hyperlink && (strpos($this->hyperlink, 'http') === false) and ($this->hyperlink[0] !== '/') and ($this->hyperlink[0] !== '['))
            $this->hyperlink = 'http://' . $this->hyperlink; // Ссылка на внешний ресурс

        if (!$this->publication_date || ($this->publication_date == 'null'))
            $this->publication_date = date("Y-m-d H:i:s", time());

        $this->last_modified_date = date('Y-m-d H:i:s', time());

        return parent::beforeSave($insert);
    }

    /**
     * @inheritDoc
     */
    public function afterSave($insert, $changedAttributes) {

        parent::afterSave($insert, $changedAttributes);

        if (($changedAttributes) && !(isset($changedAttributes['on_main']) && (count($changedAttributes) == 1))){
            $oSearch = new Search();
            $oSearch->updateByObjectId($this->id);

            if (in_array($this->parent_section, Rss\Api::getSectionsIncludedInRss()) )
                Yii::$app->trigger(Rss\Api::EVENT_REBUILD_RSS);
        }

    }

    /**
     * @inheritDoc
     */
    public function afterDelete() {

        // удаление SEO данных
        Api::del( 'news', $this->id );

        $oSearch = new Search();
        $oSearch->deleteByObjectId($this->id);

        if (in_array($this->parent_section, Rss\Api::getSectionsIncludedInRss()))
            Yii::$app->trigger(Rss\Api::EVENT_REBUILD_RSS);

    }

    /**
     * Удаление всех новостей для раздела
     * @param ModelEvent $event
     */
    public static function removeSection(ModelEvent $event) {
        self::deleteAll(['parent_section' => $event->sender->id]);
    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param \skewer\components\search\GetEngineEvent $event
     */
    public static function getSearchEngine(\skewer\components\search\GetEngineEvent $event) {
        $event->addSearchEngine(Search::className());
    }

    /**
     * Возвращает максимальную дату модификации сущности
     * @return array|bool
     */
    public static function getMaxLastModifyDate(){
        return (new \yii\db\Query())->select('MAX(`last_modified_date`) as max')->from(self::tableName())->one();
    }


    /**
     * Вернет урл новости
     * @return string
     */
    public function getUrl(){
        $hrefParam = ($this->news_alias)?"news_alias={$this->news_alias}":"news_id={$this->id}";
        return  ($this->hyperlink)?$this->hyperlink:("[{$this->parent_section}][News?".$hrefParam."]");
    }

    /**
     * Набивает внутренний массив события $oEvent последними новостями
     * @param Rss\GetRowsEvent $oEvent
     */
    public static function getRssRows( Rss\GetRowsEvent $oEvent ) {

        $aSections = array_intersect(Tree::getVisibleSections(), Rss\Api::getSectionsIncludedInRss());

        if (!$aSections)
            return ;

        $aRecords = self::find()
            ->where('announce <> :emptyString', ['emptyString' => ''])
            ->andWhere(['active' => 1])
            ->andWhere(['parent_section' => $aSections])
            ->orderBy(['publication_date' => SORT_DESC])
            ->limit(Rss\Api::COUNT_RECORDS_PER_MODULE)
            ->all();

        $oEvent->aRows = ArrayHelper::merge($oEvent->aRows,$aRecords);

    }


}
