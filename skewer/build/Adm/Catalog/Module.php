<?php

namespace skewer\build\Adm\Catalog;

use skewer\build\Adm;
use skewer\build\Catalog\Collections\Search;
use skewer\components\search\GetEngineEvent;
use skewer\components\seo\Service;
use skewer\build\Page\CatalogViewer;
use skewer\build\Catalog\Goods;

use skewer\base\section;
use skewer\base\ui;
use skewer\components\catalog;
use skewer\components\auth\CurrentAdmin;
use skewer\base\site\Layer;
use skewer\base\SysVar;
use yii\base\UserException;
use skewer\base\site_module;


/**
 * Модуль настройки вывода каталога в разделе
 * Class Module
 * @package skewer\build\Adm\Catalog
 */
class Module extends Goods\Module implements site_module\SectionModuleInterface {

    /** @var int Карточка для поисковой станицы */
    public $searchCard = false;

    /** @var int Поле для страницы с коллекциями */
    public $collectionField = false;

    /** @var string Карточка для создания нового товара */
    public $defCard = false;

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt( 'page', $this->getInnerData( 'page', 0 ) );
        $this->setInnerData( 'page', $this->iPage );

    }


    /**
     * Точка входа
     * @return void
     */
    protected function actionInit() {

        $iInitParam = (int)$this->get('init_param');
        if ($iInitParam) {
            $this->actionEdit($iInitParam);
            return;
        }

        // постраничник
        $this->iOnPage = SysVar::get('catalog.countShowGoods');
        if ( !$this->iOnPage ) $this->iOnPage = 30;

        if ( $this->defCard ) {
            // todo $this->getCard4Section( $this->getPageId() )
            // раздел с товарами и заданной дефолтной карточкой - выводим товары
            $this->actionList();
        } elseif ( $this->searchCard && SysVar::get('catalog.parametricSearch') ) {
            // раздел с результатами поиска товаров - выводим настройки
            $this->actionSearchSetting();
        } elseif ( $this->collectionField ) {
            // раздел с товарами из коллекции - выводим настройки
            $this->actionCollectionSetting();
        } else {
            // не удалось определить тип - новый раздел
            $bParamSearch = SysVar::get('catalog.parametricSearch');
            $bCollectionPage = \Yii::$app->register->moduleExists('Collections', Layer::CATALOG);
            if ( !$bParamSearch && !$bCollectionPage ) {
                // отключены расширения - сразу выбираем карточку для простого раздела каталога
                $this->actionSetCard();
            } else {
                // форма выбора типа
                $this->actionPageTypeForm();
            }
        }

    }


    /**
     * Список типовых каталожных страниц
     * @return array
     * todo перенести в апи
     */
    private function getPageTypes() {

        $aSectionTypes = [ 0 => \Yii::t('catalog', 'section_goods' ) ];

        if ( SysVar::get('catalog.parametricSearch') )
            $aSectionTypes[1] = \Yii::t('catalog', 'section_search' );

        if ( \Yii::$app->register->moduleExists('Collections', Layer::CATALOG) )
            $aSectionTypes[2] = \Yii::t('catalog', 'section_collection' );

        return $aSectionTypes;
    }


    /**
     * Форма выбора типа страницы
     */
    protected function actionPageTypeForm() {

        $this->setPanelName( \Yii::t('catalog', 'section_type_select') );

        $oForm = ui\StateBuilder::newEdit();
        $oForm
            ->fieldSelect( 'section_type', \Yii::t('catalog', 'section_type'), $this->getPageTypes(), [], false )
            ->setValue( [] )
            ->buttonSave( 'SetPageType' )
        ;

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Обработка события выбора типа раздела
     */
    protected function actionSetPageType() {

        $type = $this->getInDataVal( 'section_type', false );

        if ( $type === false ) {
            $this->init();
        } elseif( $type == 1 ) {
            $this->actionSetSearchCard();
        } elseif( $type == 2 ) {
            $this->actionSetCollectionField();
        } else {
            $this->actionSetCard();
        }

    }


    /**
     * Форма выбора карточки для поисковой страницы
     */
    protected function actionSetSearchCard() {

        //$this->setPanelName( \Yii::t('catalog', 'goods_card_select') );

        $oForm = ui\StateBuilder::newEdit();
        $oForm
            ->fieldSelect( 'searchCard', \Yii::t('catalog', 'good_card'), catalog\Card::getGoodsCardList( true ), [], false )
            ->setValue( [] )
            ->buttonSave( 'saveConfig' )
        ;

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Форма выборка поля карточки для страницы с коллекцией
     */
    protected function actionSetCollectionField() {

        //$this->setPanelName( \Yii::t('catalog', 'goods_card_select') );

        $oForm = ui\StateBuilder::newEdit();
        $oForm
            ->fieldSelect( 'collectionField', \Yii::t('catalog', 'goods_collection'), catalog\Card::getCollectionFields(), [], false )
            ->setValue( [] )
            ->buttonSave( 'saveConfig' )
        ;

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Сохранение параметров модуля, пришедших из форм
     */
    protected function actionSaveConfig() {

        $data = $this->get('data');

        if ( isSet($data['onPage']) )
            $data['onPage'] = abs((int)$data['onPage']);

        if (isset($data['related_from'])){
            catalog\RelatedSections::deleteAll(['target_section'=>$this->sectionId()]);

            if ($data['related_from']!=='') {
                $aSections = explode(',', $data['related_from']);

                foreach ($aSections as $section) {
                    catalog\RelatedSections::addRelation($this->sectionId(), $section);
                }
            }

        }

        // если пришло поле с разделом формы заказа - то раздел должен существовать
        if ( isSet($data['buyFormSection']) ) {
            $oSection = section\Tree::getSection( $data['buyFormSection'] );
            if ( !$oSection )
                unSet( $data['buyFormSection'] );
        }

        foreach ( $data as $field => $value ) {
            catalog\Section::setParam( $this->sectionId(), $field, $value );
            if ( isSet($this->$field) )
                $this->$field = $value;
        }

        // если создали раздел с коллекцией - перестроить индекс коллекций
        if ( isSet($data['collectionField']) ) {

            // пересобрать все поисковые индексы коллекций
            $oEvent = new GetEngineEvent();
            Search::getSearchEngine( $oEvent );
            foreach ( $oEvent->getList() as $sName => $sClass ) {
                /** @var Search $oSearch */
                $oSearch = new $sClass();
                $oSearch->provideName( $sName );
                $oSearch->deleteAll();
                $oSearch->restore();
            }

            Service::updateSearchIndex();

            /** Выведем панель сортировки для коллекций по умолчанию */
            section\Parameters::setParams($this->sectionId(), 'content', 'showSort', 1);

        }

        $this->actionInit();
    }


    /**
     * Настройки для страницы с товарами
     */
    protected function actionSettings() {

        // interface
        $oForm = ui\StateBuilder::newEdit();

        $oForm
            ->fieldSelect( 'listTemplate', \Yii::t('catalog', 'listTpl'), CatalogViewer\State\ListPage::getTemplates() )
            ->fieldInt( 'onPage', \Yii::t('catalog', 'listCnt'), ['minValue' => 0] )
            ->field('showFilter', \Yii::t('catalog', 'showListFilters'), 'check')
            ->field('showSort', \Yii::t('catalog', 'showListSort'), 'check')
            ->fieldString( 'buyFormSection', \Yii::t('catalog', 'formSection') )
            ->fieldSelect( 'relatedTpl', \Yii::t('catalog', 'relatedTpl'), CatalogViewer\State\ListPage::getTemplates() )
            ->fieldSelect( 'includedTpl', \Yii::t('catalog', 'includedTpl'), CatalogViewer\State\ListPage::getTemplates() );

        if ( SysVar::get('catalog.random_related') )
            $oForm->fieldMultiSelect( 'related_from', \Yii::t('catalog', 'related_from'), CatalogViewer\State\ListPage::getRelatedList() );

        $oForm
            ->setValue([
                'buyFormSection' => $this->getParamValue('buyFormSection'),
                'listTemplate' => $this->getParamValue('listTemplate'),
                'showFilter' => $this->getParamValue('showFilter'),
                'showSort' => $this->getParamValue('showSort'),
                'onPage' => (int)$this->getParamValue('onPage'),
                'relatedTpl' => $this->getParamValue('relatedTpl'),
                'includedTpl' => $this->getParamValue('includedTpl'),
                'related_from' => catalog\RelatedSections::getRelationsByPageId($this->sectionId()),
            ])

            ->buttonSave( 'SaveConfig' )
            ->buttonCancel( 'list' )
        ;

        // вывод данных в интерфейс
        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Настройка для страницы с поиском товаров
     */
    protected function actionSearchSetting() {

        $this->setPanelName( \Yii::t('catalog', 'filter_editor') );

        $oForm = ui\StateBuilder::newEdit();
        $oForm
            ->fieldSelect( 'listTemplate', \Yii::t('catalog', 'listTpl'), CatalogViewer\State\ListPage::getTemplates() )
            ->fieldInt( 'onPage', \Yii::t('catalog', 'listCnt') )

            ->setValue( [
                'listTemplate' => $this->getParamValue('listTemplate'),
                'onPage' => $this->getParamValue('onPage'),
            ] )

            ->buttonSave( 'SaveConfig' )
        ;

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Настройка для страницы с коллекцией товаров
     */
    protected function actionCollectionSetting() {

        $this->setPanelName( \Yii::t('catalog', 'filter_editor') );

        $oForm = ui\StateBuilder::newEdit();
        $oForm
            ->fieldSelect( 'listTemplate', \Yii::t('catalog', 'listTpl'), CatalogViewer\State\ListPage::getTemplates() )
            ->fieldInt( 'onPage', \Yii::t('catalog', 'listCnt') )
            ->fieldCheck( 'showSort', \Yii::t('catalog', 'showListSort') )

            ->setValue( [
                'listTemplate' => $this->getParamValue('listTemplate'),
                'onPage' => $this->getParamValue('onPage'),
                'showSort' => $this->getParamValue('showSort'),
            ] )

            ->buttonSave( 'SaveConfig' )
        ;

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Получение значения параметра для раздела
     * @param string $sFieldName имя параметра
     * @return string
     * todo перевести на внутренние переменные без обращения к базе данных
     */
    private function getParamValue( $sFieldName ) {
        $sVal = section\Parameters::getValByName( $this->sectionId(), 'content', $sFieldName, true );

        // Получение языкового параметра
        if (!$sVal and ($sFieldName == 'buyFormSection'))
            $sVal = section\Parameters::getValByName($this->sectionId(), catalog\Api::LANG_GROUP_NAME, $sFieldName, true, true);

        return $sVal ? $sVal : '';
    }

}