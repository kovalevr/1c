<?php

$aLanguage = array();

$aLanguage['ru']['Auth.Adm.tab_name'] = 'Клиенты';
$aLanguage['ru']['error_pass_fields'] = 'Введенные пароли не совпадают';

/************************/

$aLanguage['en']['Auth.Adm.tab_name'] = 'Auth';
$aLanguage['en']['error_pass_fields'] = 'Entered passwords do not match';

return $aLanguage;
