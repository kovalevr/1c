<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 19.12.2016
 * Time: 14:26
 */

namespace skewer\build\Adm\Auth\view;
use skewer\components\ext\view\FormView;

class EditLicense  extends FormView {
    public $bManyLangNoSectionId;
    public $aLanguages;
    public $sLanguageFilter;
    public $aData;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        if ($this->bManyLangNoSectionId) {
            $this->_form
                ->filterSelect('filter_language', $this->aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true])
                ->setFilterAction('editLicense');
        }
        $this->_form
            ->field('license', \Yii::t('auth', 'license'), 'wyswyg')
            ->setValue( $this->aData )
            ->buttonSave('saveLicense')
            ->buttonBack('list')
        ;
    }
}