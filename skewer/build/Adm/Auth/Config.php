<?php
/**
 * User: kolesnikiv
 * Date: 31.07.13
 */
use skewer\base\site\Layer;

$aConfig['name']     = 'Auth';
$aConfig['title']    = 'Регистрация';
$aConfig['version']  = '1.0';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'auth';

$aConfig['dependency'] = [
    ['Auth', Layer::PAGE],
    ['Auth', Layer::TOOL],
    ['Profile', Layer::PAGE],
];
$aConfig['events'][] = [
    'event' => skewer\components\modifications\Api::EVENT_GET_MODIFICATION,
    'class' => \skewer\components\auth\Auth::className(),
    'method' => 'getLastMod',
];


return $aConfig;
