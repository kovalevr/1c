<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 21.11.2016
 * Time: 17:46
 */

namespace skewer\build\Adm\Slider\view;

use skewer\components\ext\view\FormView;

class toolsForm extends FormView
{
    public $aHeightParams;
    public $aToolData;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->fieldSelect( 'mode', \Yii::t('slider', 'animtype'),
                [
                    'horizontal'=>'horizontal slide',
                    // 'vertical' => 'vertical slide',
                    'fade'=>'fade',
                ], [], false
            )
            ->field('pause', \Yii::t('slider', 'animduration'), 'int', ['minValue' => 0, 'allowDecimals' => false]) //def:450
            ->field('speed', \Yii::t('slider', 'animspeed'), 'int', ['minValue' => 0, 'allowDecimals' => false]) //def:4000
            ->field('autoHover', \Yii::t('slider', 'hoverpause'), 'check') //def:true
            ->field('auto', \Yii::t('slider', 'autostart'), 'check') //def:true
            ->field('infiniteLoop', \Yii::t('slider', 'infiniteLoop'), 'check') //def:true
            ->field('maxHeight', \Yii::t('slider', 'banner_h'), 'int', $this->aHeightParams) //def:330
            ->field('responsive', \Yii::t('slider', 'responsive'), 'check') //def:false
            ->setValue( $this->aToolData )
            ->buttonSave('saveTools')
            ->buttonCancel('bannerList');
        ;
    }
}