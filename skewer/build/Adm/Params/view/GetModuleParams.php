<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 20.02.2017
 * Time: 14:03
 */

namespace skewer\build\Adm\Params\view;

use skewer\components\ext\view\FormView;

class GetModuleParams extends FormView
{
    public $aParams;
    public $sClass;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->fieldString('class', '', ['readOnly' => true])
            ->fieldSelect('name',  '', $this->aParams, [], false)

            ->setValue([
                'class' => $this->sClass,
            ]);
    }
}