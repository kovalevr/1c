<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 22.11.2016
 * Time: 15:14
 */

namespace skewer\build\Adm\Order\view;

use skewer\components\ext\view\FormView;
use skewer\build\Adm\Order\ar;

class AddTypeDelivery extends FormView
{
    /** @var ar\TypeDeliveryRow */
    public $oItems;

    /**
     * @inheritdoc
     */
    function build() {
        $this->_form
            ->field('title', \Yii::t('order', 'payment_field_title'), 'string')
            ->setValue($this->oItems)
            ->buttonSave('saveTypeDelivery')
            ->buttonCancel('TypeDeliveryList')
            ;
    }
}