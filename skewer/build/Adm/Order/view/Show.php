<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 05.12.2016
 * Time: 18:15
 */

namespace skewer\build\Adm\Order\view;

use skewer\components\ext\view\FormView;

class Show extends FormView
{
    public $aStatusList;
    public $aPaymentList;
    public $aDeliveryList;
    public $aOrder;

    /**
     * @inheritdoc
     */
    function build() {
        $this->_form->field('id', 'ID', 'string', ['readOnly' => 1])
            ->field('date', \Yii::t('order', 'field_date'), 'datetime')
            ->field('person', \Yii::t('order', 'field_contact_face'), 'string')
            ->field('postcode', \Yii::t('order', 'field_postcode'), 'string')
            ->field('address', \Yii::t('order', 'field_address'), 'string')
            ->field('phone', \Yii::t('order', 'field_phone'), 'string')
            ->field('mail', \Yii::t('order', 'field_mail'), 'string')

            ->fieldSelect('status', \Yii::t('order', 'field_status'), $this->aStatusList, [], false)
            ->fieldSelect('type_payment', \Yii::t('order', 'field_payment'), $this->aPaymentList, [], false)
            ->fieldSelect('type_delivery', \Yii::t('order', 'field_delivery'), $this->aDeliveryList, [], false)

            ->field('text', \Yii::t('order', 'field_text'), 'text')
            ->field('notes', \Yii::t('order', 'field_notes'), 'text')
            ->field('good', \Yii::t('order', 'goods_info'), 'show', array('labelAlign' => 'top'))

            ->field('history', \Yii::t('order', 'history_list'), 'show', array('labelAlign' => 'top'))

            ->setValue($this->aOrder)
            ->buttonSave()
            ->buttonBack();

        if (isset($this->aOrder['id']) && $this->aOrder['id']){
            $this->_form->buttonEdit('goodsShow', \Yii::t('order', 'field_goods_title_edit'));
        }

        $this->_form
            ->buttonSeparator('->')
            ->buttonDelete();
    }
}