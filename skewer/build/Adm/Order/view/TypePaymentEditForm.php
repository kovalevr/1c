<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 22.11.2016
 * Time: 15:29
 */

namespace skewer\build\Adm\Order\view;

use skewer\components\ext\view\FormView;
use skewer\build\Adm\Order\ar;

class TypePaymentEditForm extends FormView
{
    /** @var ar\TypePayment $oParameters */
    public $oParameters;
    public $aItems;

    /**
     * @inheritdoc
     */
    function build() {
        $this->_form
            ->field('id', 'id', 'hide')
            ->field('title', \Yii::t('order', 'payment_field_title'), 'string');

        if ($this->aItems)
            $this->_form->fieldSelect( 'payment', \Yii::t('order', 'payment_field_payment'), $this->aItems, [], false);

        $this->_form->setValue($this->oParameters)
            ->buttonSave('saveTypePayment')
            ->buttonCancel('TypePaymentList')
            ;
    }
}