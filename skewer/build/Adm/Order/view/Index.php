<?php

namespace skewer\build\Adm\Order\view;

use skewer\components\ext\view\ListView;

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 21.11.2016
 * Time: 18:30
 */
class Index extends ListView {
    public $aStatusList;
    public $iStatusFilter;
    public $aItems;

    /**
     * @inheritdoc
     */
    function build() {
        $this->_list
            ->filterSelect('filter_status', $this->aStatusList, $this->iStatusFilter, \Yii::t('order', 'field_status'))
            ->field('id', 'ID', 'string', array('listColumns' => array('flex' => 1)))
            ->field('date', \Yii::t('order', 'field_date'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('person', \Yii::t('order', 'field_contact_face'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('mail', \Yii::t('order', 'field_mail'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('total_price', \Yii::t('order', 'field_goods_total'), 'string', array('listColumns' => array('flex' => 2)))
            ->field('status', \Yii::t('order', 'field_status'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('is_mobile', 'Mobile', 'check', array( 'listColumns' => array('flex' => 1) ))
            ->widget( 'status', 'skewer\\build\\Adm\\Order\\Service', 'getStatusValue' )
            ->setValue($this->aItems, $this->onPage, $this->page, $this->total)
            ->buttonRowUpdate()
            ->buttonRowDelete()
            ->buttonEdit('statusList',      \Yii::t('order', 'field_orderstatus_title'))
            ->buttonEdit('emailShow',       \Yii::t('order', 'field_mailtext'))
            ->buttonEdit('typePaymentList', \Yii::t('order', 'field_payment'))
            ->buttonEdit('typeDeliveryList',\Yii::t('order', 'field_delivery'))
            ->buttonEdit('editLicense',     \Yii::t('auth', 'license'))
            ->buttonEdit('editSettings',    \Yii::t('order', 'settings'))
        ;

    }
}