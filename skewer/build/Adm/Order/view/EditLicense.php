<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 05.12.2016
 * Time: 17:10
 */

namespace skewer\build\Adm\Order\view;

use skewer\components\ext\view\FormView;

class EditLicense extends FormView
{
    public $sectionId;
    public $aLanguages;
    public $sLanguageFilter;
    public $aData;
    /**
     * @inheritdoc
     */
    function build() {
        if (!$this->sectionId && count($this->aLanguages) > 1) {
            $this->_form->filterSelect('filter_language', $this->aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true])
                ->setFilterAction('editLicense');
        }

        $this->_form->field('license', \Yii::t('order', 'license_edit'), 'wyswyg')
            ->setValue( $this->aData )
            ->buttonSave( 'saveLicense' )
            ->buttonCancel( 'list' )
        ;
    }
}