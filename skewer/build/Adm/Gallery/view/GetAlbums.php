<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 28.02.2017
 * Time: 11:38
 */

namespace skewer\build\Adm\Gallery\view;

use skewer\components\ext\view\FileView;
use skewer\components\ext;

class GetAlbums extends FileView
{
    protected function getLibFileName()
    {
        return 'PhotoAlbumList';
    }

    /**
     * @inheritdoc
     */
    function build() {
        $this->_form
            ->buttonCustomExt( ext\docked\AddBtn::create()
                ->setAction('addUpdAlbum')
            )
            ->buttonSeparator()
            ->buttonCustomExt( ext\docked\DelBtn::create()
                ->setAction('')
                ->setState('del_selected')
            );
    }
}