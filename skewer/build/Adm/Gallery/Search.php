<?php


namespace skewer\build\Adm\Gallery;

use skewer\base\orm\Query;
use skewer\components\search\Prototype;
use skewer\components\search\Row;
use skewer\components\gallery;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\components\search\SearchIndex;
use yii\helpers\ArrayHelper;

class Search extends Prototype {

    /** @var bool Флаг необходимости сброса статуса всем альбомам */
    public $bResetAllAlbums = false;

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return "Gallery";
    }

    /**
     * @inheritdoc
     */
    protected function update(Row $oSearchRow) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        /** @var gallery\models\Albums $oAlbum */
        if (!$oAlbum = gallery\Album::getById($oSearchRow->object_id))
            return false;

        $aAllAlbums = gallery\Album::getBySection($oAlbum->section_id, true, 0, 1, $iCountAlbums);

        if ($this->bResetAllAlbums){
            $this->resetAlbumsByObjectId(array_keys(ArrayHelper::index($aAllAlbums, 'id')));
            $this->updateSiteMap();
        }

        $bHidden = false;

        if ( !gallery\Photo::getCountByAlbum( $oAlbum->id ) )
            $bHidden = true;

        if (!$oAlbum->visible)
            $bHidden = true;

        /** Если стоит галочка выводить только фото, то не добавляем в поиск */
        if ($oAlbum->section_id && ( Parameters::getValByName($oAlbum->section_id, 'content', 'openAlbum') || ($iCountAlbums <= 1) ) )
            $bHidden = true;

        $oSearchRow->search_text = $this->stripTags($oAlbum->description);
        $oSearchRow->search_title = $this->stripTags($oAlbum->title);
        $oSearchRow->section_id = $oAlbum->section_id;
        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = !$bHidden;
        $oSearchRow->use_in_sitemap = !$bHidden;
        $oSearchRow->modify_date = $oAlbum->last_modified_date;

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        $oSearchRow->language = Parameters::getLanguage($oAlbum->section_id);

        $oSearchRow->href  = $sURL = \Yii::$app->router->rewriteURL(sprintf(
            '[%s][Gallery?%s=%s]',
            $oAlbum->section_id,
            'alias',
            $oAlbum->alias
        ));

        $oSeoComponent = new Seo($oAlbum->id, $oSection->id, $oAlbum->getAttributes());
        $oSEOData = \skewer\components\seo\Api::get( Seo::getGroup(), $oSearchRow->object_id, $oSection->id );

        $oSearchRow->priority = ($oSEOData && !empty($oSEOData->priority))
            ? $oSEOData->priority
            : $oSeoComponent->calculatePriority();

        $oSearchRow->frequency = ($oSEOData && !empty($oSEOData->frequency))
            ? $oSEOData->frequency
            : $oSeoComponent->calculateFrequency();

        $oSearchRow->save();
        return true;

    }

    /**
     * Сброс статуса для всех сущностей, привязанных к этому разделу
     * @param $id - id раздела
     */
    public function resetAllEntityBySectionId($id){

        SearchIndex::update()
            ->set('status', 0)
            ->where('section_id', $id)
            ->get();
    }

    /**
     * Сброс статуса альбому/-ам
     * @param $mId
     */
    public function resetAlbumsByObjectId($mId){

        SearchIndex::update()
            ->set('status', 0)
            ->where('object_id', $mId)
            ->get();

    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`) SELECT '0', '{$this->getName()}', id FROM photogallery_albums WHERE profile_id=6";
        Query::SQL($sql);
    }

 }
