<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 22.02.2017
 * Time: 12:25
 */
namespace skewer\build\Adm\Editor\view;

use skewer\components\ext\view\FormView;
use skewer\components\seo\Api;

class LoadItems extends FormView
{
    public $bNeedFieldSectionLink;
    public $sTextLink;
    public $sHrefLink;
    public $aFieldsData;

    public $bHasAddComponents;
    public $oSeo;
    public $aExcludedFields;

    public $iSectionId;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        if ($this->bNeedFieldSectionLink) $this->_form->fieldLink('page_href', \Yii::t('editor', 'hyperlink'), $this->sTextLink, $this->sHrefLink);

        if ($this->aFieldsData) {
            foreach ($this->aFieldsData as $aFieldData) {
                $this->_form->field($aFieldData['name'], $aFieldData['title'], $aFieldData['editorType'],
                    $aFieldData['params']);
            }
        }

        if ($this->bHasAddComponents) {
            Api::appendExtForm($this->_form, $this->oSeo, $this->iSectionId, $this->aExcludedFields);
        }

        $this->_form->useSpecSectionForImages( $this->iSectionId );
        $this->_form
            ->buttonSave()
            ->buttonCancel()
        ;
    }
}