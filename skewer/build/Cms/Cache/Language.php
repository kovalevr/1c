<?php

$aLanguage = array();

$aLanguage['ru']['Cache.Cms.tab_name'] = 'Сброс кэша';
$aLanguage['ru']['drop_cache_act'] = 'Сбросить кэш';
$aLanguage['ru']['drop_cache_text'] = 'Кэш сброшен';

$aLanguage['en']['Cache.Cms.tab_name'] = 'Cache dropper';
$aLanguage['en']['drop_cache_act'] = 'Drop cache';
$aLanguage['en']['drop_cache_text'] = 'Cache was dropped';

return $aLanguage;