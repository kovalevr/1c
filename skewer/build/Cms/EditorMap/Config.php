<?php

$aConfig['name']     = 'EditorMap';
$aConfig['title']    = 'Редактор карт';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Редактор карт';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \skewer\base\site\Layer::CMS;
$aConfig['languageCategory'] = 'editorMap';

return $aConfig;