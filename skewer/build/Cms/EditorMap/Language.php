<?php

$aLanguage = array();

$aLanguage['ru']['EditorMap.Cms.tab_name'] = 'Редактор карт';
$aLanguage['ru']['mapPanelTitle'] = 'Настройка карты';
$aLanguage['ru']['save'] = "Сохранить";
$aLanguage['ru']['map_not_available'] = "Поле карта не доступно в данном интерфейсе";
$aLanguage['ru']['map_is_not_selected'] = 'Не выбран тип карты';
$aLanguage['ru']['necessary_set_marker'] = 'Необходимо установить маркер';
$aLanguage['ru']['set_marker'] = 'Установить маркер';
$aLanguage['ru']['latitude'] = 'Широта';
$aLanguage['ru']['longitude'] = 'Долгота';
$aLanguage['ru']['all_fields_required'] = 'Все поля обязательны для заполнения';
$aLanguage['ru']['invalid_data_format'] = 'Не верный формат данных';
$aLanguage['ru']['error_latitude_range'] = 'Величина широты должна быть в пределах [-90,90]';
$aLanguage['ru']['error_longtitude_range'] = 'Величина долготы должна быть в пределах [-180,180]';

$aLanguage['en']['EditorMap.Cms.tab_name'] = 'Map Editor';
$aLanguage['en']['mapPanelTitle'] = 'Setting map';
$aLanguage['en']['save'] = "Save";
$aLanguage['en']['map_not_available'] = "Field map is not available in this interface";
$aLanguage['en']['map_is_not_selected'] = 'Map type is not selected';
$aLanguage['en']['necessary_set_marker'] = 'It is necessary to set a marker';
$aLanguage['en']['set_marker'] = 'Set marker';
$aLanguage['en']['latitude']  = 'Latitude';
$aLanguage['en']['longitude'] = 'Longitude';
$aLanguage['en']['all_fields_required'] = 'All fields are required';
$aLanguage['en']['invalid_data_format'] = 'Invalid data format';
$aLanguage['en']['error_latitude_range'] = 'The amount of latitude should be in the range [-90.90]';
$aLanguage['en']['error_longtitude_range'] = 'The value of longitude must be within [-180.180]';

return $aLanguage;