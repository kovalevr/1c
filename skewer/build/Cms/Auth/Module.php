<?php

namespace skewer\build\Cms\Auth;

use skewer\build\Cms;
use skewer\components\auth\CurrentAdmin;

/**
 * Класс для авторизации в CMS
 * Class Module
 * @package skewer\build\Cms\Auth
 */
class Module extends Cms\Frame\ModulePrototype {

    protected $viewMode = 'default';

    /**
     * Разрешает выполнение модуля без авторизации
     * @return bool
     */
    public function allowExecute() {
        return true;
    }

    /**
     * Выбор интерфейса для отображения в текущей ситуации
     * @return bool
     */
    protected function actionInit(){

        if ( $this->viewMode === 'form' ) {
            $this->showAuthForm();
        } else {
            $this->showMainPanel();
        }

    }

    /**
     * Отдает на выход форму авторизации в собственном слое
     */
    protected function showAuthForm(){


        $this->setModuleLangValues(
            array(
                'authPanelTitle',
                'authLoginTitle',
                'authPassTitle',
                'authLoginButton',
                'authLoginIncorrect'
            )
        );

        // основная библиотека вывода
        $this->setJSONHeader('externalLib','AuthLayout');

        // добавить библиотеку
        $this->addLibClass('AuthForm');

        // задать состояние
        $this->setCmd('init');

        // сообщение
        $aMes = \Yii::$app->getParam('systemMessage.cms.message');
        if ( $aMes ) {
            $this->addMessage(
                $aMes[0],
                $aMes[1],
                -1
            );
        }

    }

    /**
     * Отдает панель для админского интерфейса с данными авторизации
     */
    protected function showMainPanel(){

        $this->setModuleLangValues(
            array(
                'authLogoutButton',
                'authLastVisit',
            )
        );
        $aUserData = CurrentAdmin::getUserData();

        // дата последнего захода
        $sDate = isSet($aUserData['lastlogin']) ? $aUserData['lastlogin'] : '';
        if ( $sDate and $sDate>1900  ) {
            $sDate = date('d.m.Y H:i', strtotime($sDate));
        } else {
            $sDate = \Yii::t('auth', 'firstVisit');
        }

        $this->addInitParam('renderData' ,array(
            'username' => (isSet($aUserData['name']))? $aUserData['name']: '',
            'lastlogin' => $sDate
        ));

    }

    /**
     * Авторизация пользователя
     * @return int
     */
    protected function actionLogin(){

        // получение данных
        $sLogin = strtolower($this->getStr('login'));
        $sPass = $this->getStr('pass');

        // задать состояние
        $this->setCmd('login');

        // попытка авторизации
        $bLogIn = CurrentAdmin::login($sLogin, $sPass);

        if ($bLogIn) {
            $notice = \Yii::t('auth', 'user_login');
        }
        else {
            $notice = \Yii::t('auth', 'user_invalid_login');
        }
        $this->addModuleNoticeReport($notice, array('User ID'=>CurrentAdmin::getId(), 'Login'=>$sLogin));

        // результат авторизации
        $this->setData('success',$bLogIn);

        // отдать результат работы метода
        return psComplete;

    }

    /**
     * Выход из системы
     * @return int
     */
    protected function actionLogout(){

        // задать состояние
        $this->setCmd('login');

        $this->addModuleNoticeReport(
            \Yii::t('auth', 'user_logout'),
            array(
                'User ID'=>CurrentAdmin::getId(),
                'Login'=>$_SESSION['auth']['admin']['userData']['login']
            )
        );

        // попытка авторизации
        $bLogOut = CurrentAdmin::logout();

        // результат авторизации
        $this->setData('success',$bLogOut);

        // отдать результат работы метода
        return $bLogOut ? psReset : psComplete;

    }

}
