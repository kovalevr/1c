$(function() {

    $('.js-sidebar-show').on('click', function(event) {
        event.preventDefault();
        showSidebar();
    });
    $('.js-sidebar-hide').on('click', function(event) {
        event.preventDefault();
        hideSidebar();
    });

});


function showSidebar() {
    $('.js-sidebar-inner').addClass('b-sidebar_open');

    $('.js-sidebar').fadeIn('1000');
    $('html, body').css('overflow', 'hidden');

}

function hideSidebar() {
    $('.js-sidebar-inner').removeClass('b-sidebar_open');

    $('.js-sidebar').fadeOut('1000');
    $('html, body').css('overflow', 'visible');
}

// Включение адаптивных слайдеров для каталогов на главной
$('.js-catalogbox_gal_onmain').each(function(){

    $(this).slick({
        respondTo: 'slider',

        arrows: true, // Включить стрелки перелистывания
        prevArrow: $(this).siblings('.js-catalog_slider_back'),
        nextArrow: $(this).siblings('.js-catalog_slider_next'),

        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
            {
                breakpoint: 1080,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 920,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 660,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

});