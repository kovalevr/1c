<?php

use skewer\components\design\Design;
use yii\helpers\ArrayHelper;
use skewer\helpers\Adaptive;

/**
 * @var $this \yii\web\View
 * @var string $SEOTitle
 * @var string $SEOKeywords
 * @var string $SEODescription
 * @var string $canonical_url
 * @var string $openGraph
 * @var array $page_class
 * @var int $sectionId
 * @var array $_params_
 * @var string $canonical_pagination
 */

use skewer\components\seo;
use \yii\helpers\Html;
                                 
$pageBundle = \skewer\build\Page\Main\Asset::register($this);

if (Design::modeIsActive()){
    \skewer\build\Design\Frame\AssetDesign::register($this);
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= \Yii::$app->language ?>" >
<head>
    <meta charset="utf-8" />

    <? if (Adaptive::modeIsActive()):?><meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"><? endif; ?>



    <? // todo SEO переписать по Yii-ому #44105 ?>
    <title><?= seo\Api::prepareRawString($SEOTitle) ?></title>
    <? if ($openGraph):?> <?= $openGraph ?> <? endif; ?>
    <meta name="description" content="<?= seo\Api::prepareRawString($SEODescription) ?>" />
    <meta name="keywords" content="<?= seo\Api::prepareRawString($SEOKeywords) ?>" />
    <link rel="shortcut icon" href="<?= Design::getFavicon() ?>" type="<?= Design::getFavicon(true) ?>" />
    <? if (isSet($SEONonIndex) && $SEONonIndex): ?><meta name="robots" content="none"/><? endif ?>
    <? if (isSet($SEOAddMeta) && $SEOAddMeta): ?><?=Html::decode($SEOAddMeta) ?><? endif ?>

    <? if ($canonical_url): ?>
        <link rel="canonical" href="<?= $canonical_url ?>" />
    <? endif ?>
    <? if (isset($canonical_pagination)): ?>
        <link rel="canonical" href="<?=$canonical_pagination;?>">
    <? endif; ?>

    <? if (isset($aLangLinks)): ?>
        <? foreach ($aLangLinks as $links): ?>
            <meta rel="alternate" hreflang="<?=Html::decode($links['hreflang']) ?>" href="<?=Html::decode($links['href']) ?>" />
        <? endforeach ?>
    <? endif ?>

    <?php $this->head() ?>

    
    <?php
        // css файл, редактируемый диз режиме 
        $sFileName = Design::getAddCssFilePath();
        if ( file_exists($sFileName) ) {
            $lastUpdatedTime = Design::getLastUpdatedTime();
            echo Html::tag('link', '', [
                'rel' => 'stylesheet',
                'href' => str_replace(WEBPATH,'/', $sFileName).'?v='.$lastUpdatedTime,
                'type' => 'text/css',
                'media' => 'screen, all'
            ]);
        }
    ?>

    <? # дополнительный блок в заголовке ?>
    <? if (isSet($addHead['text']) && $addHead['text']): ?><?=Html::decode($addHead['text']) ?><? endif ?>

    <? # должен быть непосредственно перед закрывающим тегом </head> ?>
    <? if (isSet($gaCode['text']) && $gaCode['text']): ?><?=Html::decode($gaCode['text']) ?><? endif ?>

</head>

    <?
    /** @var string $sBodyAttr доп атрибуты для тэга body */
    $sBodyAttr = '';

    if ( isset($page_class) and $page_class['value'] )
        $sBodyAttr .= sprintf(' class="%s"', $page_class['value']);

    if ( Design::modeIsActive() ) {
        $sBodyAttr .= ' sktag="page"';
        $sBodyAttr .= sprintf(' sectionid="%d"', $sectionId );
    }
    ?>

    <body<?= $sBodyAttr ?>>
        <?php $this->beginBody() ?>
        <input type="hidden" id="current_language" value="<?= \Yii::$app->language ?>">

        <div class="l-layout">
            <div class="layout__wrap">

                <?
                $headTpl = ArrayHelper::getValue($_params_, ['.layout', 'head_tpl', 0], 'base');
                echo $this->render('head/'.$headTpl.'/tpl', $_params_)
                ?>

                <?
                $contentTpl = ArrayHelper::getValue($_params_, ['.layout', 'content_tpl', 0], 'base');
                echo $this->render('content/'.$contentTpl.'/tpl', $_params_)
                ?>

            </div>
            <div class="layout__bgbox">
                <div class="layout__bgwrap">
                    <div class="layout__bgleft"></div>
                    <div class="layout__bgright"></div>
                </div>
            </div>
        </div>

        <?
        $footerTpl = ArrayHelper::getValue($_params_, ['.layout', 'footer_tpl', 0], 'base');
        echo $this->render('footer/'.$footerTpl.'/tpl', $_params_)
        ?>

        <?= (isset($countersCode['text']))?$countersCode['text']:'' ?>

        <div class="js-callbackForm" style="display: none;"></div>

        <?= $this->render('MicroData'); ?>

        <?php $this->endBody() ?>

    </body>
</html>
<?php $this->endPage() ?>
