<?php

namespace skewer\build\Page\Main\templates\head\example;

use skewer\components\design\Design;

/**
 * @var int $mainId
 * @var array $headtext1
 * @var array $headtext2
 * @var array $headtext3
 * @var array $headtext4
 * @var array $headtext5
 */

ExampleAsset::register($this);

var_dump( 'example' );

?>

<div class="l-header">
    <div class="header__wrapper js_header__wrapper">
        <div class="b-pilot"<?= Design::write(' sktag="page.head" sklayout="head"') ?>>
            <div class="b-logo <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-left<? endif ?>"<?= Design::write(' sktag="page.head.logo"') ?>><a href="<?= '[' . $mainId . ']' ?>"><img alt="logo" src="<?= Design::getLogo() ?>"></a>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>logo</span><ins></ins></div>
                <? endif ?>
            </div>
            <div class="pilot__1 <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot1','h_position')?>" sktag="page.head.pilot1" skeditor="./headtext1<? endif ?>"><?= $headtext1['text'] ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt1</span><ins></ins></div>
                <? endif ?>
            </div>
            <div class="pilot__2 <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot2','h_position')?>" sktag="page.head.pilot2" skeditor="./headtext2<? endif ?>"><?= $headtext2['text'] ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt2</span><ins></ins></div>
                <? endif ?>
            </div>
            <div class="pilot__3 <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot3','h_position')?>" sktag="page.head.pilot3" skeditor="./headtext3<? endif ?>"><?= $headtext3['text'] ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt3</span><ins></ins></div>
                <? endif ?>
            </div>
            <div class="pilot__4 <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot4','h_position')?>" sktag="page.head.pilot4" skeditor="./headtext4<? endif ?>"><?= $headtext4['text'] ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt4</span><ins></ins></div>
                <? endif ?>
            </div>
            <div class="pilot__5 <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot5','h_position')?>" sktag="page.head.pilot5" skeditor="./headtext5<? endif ?>"><?= $headtext5['text'] ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt5</span><ins></ins></div>
                <? endif ?>
            </div>
        </div>

        <?= (isset($layout['head']))?$layout['head']:'' ?>

        <div class="header__left"></div>
        <div class="header__right"></div>
    </div>
</div>