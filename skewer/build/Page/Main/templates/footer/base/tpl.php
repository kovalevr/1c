<?php

use skewer\components\design\Design;

/**
 * @var array $copyright
 * @var array $copyright_dev
 * @var array $contacts
 * @var array $footertext4
 * @var array $footertext5
 * @var string $bottomMenu
 */

?>

<div class="l-footerbox"<?= Design::write(' sktag="page.footer"') ?>>
    <div class="footerbox__wrapper">

        <div class="l-grid">
            <div class="grid__item1<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid1','h_position')?>" sktag="page.footer.grid1" skeditor="./copyright<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>copyright</span><ins></ins></div>
                <? endif ?>
                <?= str_replace('[Year]', date('Y'),$copyright['text']) ?>
                <?= $copyright_dev['text'] ?>
            </div>
            <div class="grid__item2<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid2','h_position')?>" sktag="page.footer.grid2" skeditor="./counters<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>counters</span><ins></ins></div>
                <? endif ?>
                <div class="b-counter">
                    <!--noindex--><?= (isset($counters) && isset($counters['text']))?$counters['text']:""?><!--/noindex-->
                </div>
            </div>
            <div class="grid__item3<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid3','h_position')?>" sktag="page.footer.grid3" skeditor="./contacts<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>contacts</span><ins></ins></div>
                <? endif ?>
                <?= $bottomMenu ?>
                <?= $contacts['text'] ?>
            </div>
            <div class="grid__item4<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid4','h_position')?>" sktag="page.footer.grid4" skeditor="./footertext4<? endif ?>"><?= $footertext4['text'] ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt</span><ins></ins></div>
                <? endif ?>
            </div>
            <div class="grid__item5<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid5','h_position')?>" sktag="page.footer.grid5" skeditor="./footertext5<? endif ?>"><?= $footertext5['text'] ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt5</span><ins></ins></div>
                <? endif ?>
            </div>
            <div class="grid__item6<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid5','h_position')?>" sktag="page.footer.grid5" skeditor="./footertext5<? endif ?>"><?= $footertext5['text'] ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt6</span><ins></ins></div>
                <? endif ?>
            </div>
        </div>
        <div class="footerbox__left"></div>
        <div class="footerbox__right"></div>
    </div>
</div>

