$(function(){
    
 
    
    var bDebug = false;

    if(bDebug){

        console.log('Document rendered: %s',new Date());
    }//is debug


    $('.js_use_resize').fancybox(
        $.extend(commonFancyBoxConfig, {
            openEffect: 'fade',
            closeEffect: 'fade',
            nextEffect: 'none',
            prevEffect: 'none',
            arrows    : true,
            nextClick : true,
            mouseWheel: true
        })
    );

    $(".js-callback").click(function(){
        var section = $(this).data('section');
        var ajaxForm = $(this).data('ajaxform');
        var formName = $(this).data('formname');
        var label = $(this).data('label');
        var link = $(this);

        var module = $(this).data('module');
        var cmd = $(this).data('cmd');
        var idObj = $(this).data('idobj');
        var count = 1;

        if (!module) module = 'Forms';
        if (!cmd) cmd = 'show';
        if (!idObj) idObj = 0;
        else {

            var context = $(this).closest(".js_goods_container");
            if (!context.length)
                context = '';
            var countInput = $(".js_count[data-id='"+idObj+"']",context);

            if (countInput.length)
                count = parseInt( countInput.val() );

        }

        $.post( '/ajax/ajax.php', {
                moduleName: module,
                cmd: cmd,
                section: section,
                formName: formName,
                ajaxForm: ajaxForm,
                ajaxShow: 1,
                label: label,
                idObj: idObj,
                objectId: idObj,
                count:count,
                language: $('#current_language').val()
            },
            function ( mResponse ) {

                if (!mResponse) return false;

                var oResponse = $.parseJSON( mResponse );

                var formDiv = $( '.js-callbackForm' );

                if (oResponse.css && oResponse.css.length){
                    $(oResponse.css).each(function(){
                        if (!$('link [href="' + this.filePath + '"]').size()){
                            var fileRef=document.createElement("link");
                            fileRef.setAttribute("rel", "stylesheet");
                            fileRef.setAttribute("type", "text/css");
                            fileRef.setAttribute("href", this.filePath) ;
                            document.getElementsByTagName("head")[0].appendChild(fileRef);
                        }
                    });
                }

                if (oResponse.js && oResponse.js.length){
                    $(oResponse.js).each(function(){
                        if (!$('script [src="' + this.filePath + '"]').size()){
                            var fileRef=document.createElement("script");
                            fileRef.setAttribute("type", "text/javascript");
                            fileRef.setAttribute("charset", "utf-8");
                            fileRef.setAttribute("src", this.filePath) ;
                            document.getElementsByTagName("head")[0].appendChild(fileRef);
                        }
                    });
                }

                formDiv.html( oResponse.html );
                var formId = formDiv.find('form:first' ).attr( 'id' );

                if ( typeof formId != 'undefined' ) {

                    maskInit(formDiv);
                    toggleShowPlaceholder(formDiv);

                    updateFromValidator(formId);

                    /* calendar - инициализация календарика */
                    if ( typeof $.datepicker !== 'undefined' ) {
                        $.datepicker.setDefaults({
                            dateFormat: 'dd.mm.yy'
                        });

                        $( '.js_init_datepicker', '#' + formId ).datepicker();
                    }
                }

                var max_width = link.data('js_max_width');
                if ( !max_width ){
                    max_width = 9999
                }
                else{
                    formDiv.css( { width: max_width } );
                }

                var max_height= link.data('js_max_height');
                if ( !max_height ){
                    max_height = 9999
                }else{
                    formDiv.css( { height: max_height } );
                }

                if (!idObj || (idObj && (count > 0))){
                    $.fancybox.open(formDiv,{
                        dataType : 'html',
                        autoSize: true,
                        autoCenter: true,
                        width:max_width,
                        height:max_height,
                        openEffect: 'none',
                        closeEffect: 'none',
                        scrolling : 'no',
                        'afterShow': function(){
                            $(".js-callback").blur();
                        },
                        'afterClose': function(){
                            $('.img_captcha').each( function() {
                                reloadImg( this );
                            });
                        },
                        'onStart':function(){
                            formDiv.show();
                        },
                        'onClosed':function(){
                            formDiv.hide();
                        }
                    });

                } else alert('Введено неправильное количество товара');

                return true;
            });

        return false;
    });

    $('.js-gallery_resize').fancybox(
        $.extend(commonFancyBoxConfig,{
            openEffect: 'fade',
    //            closeEffect: 'fade',
            nextEffect: 'elastic',
            prevEffect: 'elastic',
            helpers : {
                title : {
                    type : 'inside'
                }
            },
            arrows    : true,
            nextClick : true,
            mouseWheel: true,
            closeBtn: true
        })
    );

});

// Конфиг общих настроек для fancybox
var commonFancyBoxConfig = {

    beforeShow: function () {
        var imgAlt = $(this.element).find("img").attr("alt");
        var dataAlt = $(this.element).data("alt");
        if (imgAlt) {
            $(".fancybox-image").attr("alt", imgAlt);
        } else if (dataAlt) {
            $(".fancybox-image").attr("alt", dataAlt);
        }
    }
};

function parseGetParams(url) {
    var $_GET = {};
    if ( url == undefined )
        url = window.location.search;
    var __GET = url.substring(url.indexOf('?') + 1).split("&");
    for(var i=0; i<__GET.length; i++) {
        var getVar = __GET[i].split("=");
        if ( getVar[1] != undefined )
            $_GET[getVar[0]] = getVar[1];
    }
    return $_GET;
}

function reloadImg( obj ) {

    if ( !obj ) return false;
    obj = $( obj );

    var src;
    var base_src = obj.attr('src');
    var pos = base_src.indexOf('?');

    if ( pos >= 0 )
        src = base_src.substr(0, pos);
    else
        src = base_src;

    var hash = '';
    var form = $(obj).parents('form');
    if ( form.size() && form.attr('id') )
        hash = form.attr('id').substr(5);
    else {
        var params = parseGetParams(base_src);
        if ( params['h'] != undefined )
            hash = params['h'];
    }

    var date = new Date();
    obj.attr( 'src', src + '?h=' + hash + '&v=' + date.getTime() );

    return false;
}