<?php

namespace skewer\build\Page\Auth;

use skewer\base\site\Type;
use skewer\components\auth\models\Users;

class PageUsers extends Users {

    
    public function validate($attributeNames = null, $clearErrors = true)
    {
        $bRes = parent::validate($attributeNames, $clearErrors);
        
        if (!filter_var($this->login, FILTER_VALIDATE_EMAIL)) {
                parent::addError(Users::ERR_USER,\Yii::t('auth', 'no_email_valid'));
                return false;
            }

        return $bRes;
    }//func


    public function save($runValidation = true, $attributeNames = null)
    {
        $this->email = ($this->login)?strtolower($this->login):'';
        if ( Type::isShop() )
            $this->reg_date = date('Y-m-d H:i:s');
        return parent::save($runValidation, $attributeNames);
    }//func

    public function getValidateErrorMessage()
    {
        return parent::getErrors(Users::ERR_USER);
    }//ready
}