<?php

namespace skewer\build\Page\Auth;


use skewer\base\site\Site;
use skewer\components\auth\models\Users;
use skewer\helpers\Mailer;
use skewer\base\section\Tree;
use skewer\components\i18n\ModulesParams;
use skewer\components\auth\Auth;
use yii\helpers\ArrayHelper;
use skewer\base\SysVar;

class Api {
    /** активное состояние пользователя*/
    const STATUS_NO_AUTH = 0;
    const STATUS_AUTH = 1;
    const STATUS_BANNED = 2;
    /** статусы активации пользователей в админке*/
    const ACTIVATE_AUTO = 1;
    const ACTIVATE_EMAIL = 2;
    const ACTIVATE_ADMIN = 3;

    /**
     * Путь страницы личного кабинета
     * @return mixed|string
     */
    public static function getProfilePath(){

        return ArrayHelper::getValue(Tree::getCachedSection(), \Yii::$app->sections->getValue('profile') . '.alias_path', '');

    }

    /**
     * Путь страницы авторизации
     * @return mixed|string
     */
    public static function getAuthPath(){

        return ArrayHelper::getValue(Tree::getCachedSection(), \Yii::$app->sections->getValue('auth') . '.alias_path', '');

    }

    /**
     * список режимов активации
     * @return array
     */
    public static function getActivateStatusList(){
        return array(
            1 => \Yii::t('auth', 'activate_auto'),
            2 => \Yii::t('auth', 'activate_confirm'),
            3 => \Yii::t('auth', 'activate_admin'),
        );
    }

    /**
     * значения политик доступа пользователя
     * @return array
     */
    public static function getStatusList(){
        return array(
            0 => \Yii::t('auth', 'status_no_auth'),
            1 => \Yii::t('auth', 'status_auth'),
            2 => \Yii::t('auth', 'status_banned'),
        );
    }

    /**
     * Регистрация нового пользователя
     * @param array $aData Массив данных пользователя
     * @param string $sError Текст ошибки решистрации
     * @return bool;
     */
    public static function registerUser($aData, &$sError = '') {

        $oItem = self::setUserData($aData);

        if ( $oItem->validate()) {

            // генерим токен
            $oItem->active = 0;

            $iStatus = SysVar::get( 'auth.activate_status' );

            //если автоматическая регистрация, то выставляем сразу активность
            $oItem->active = ( $iStatus == Api::ACTIVATE_AUTO ) ? Api::STATUS_AUTH : Api::STATUS_NO_AUTH;

            if ( $oItem->save() ) {

                $oTicket = new AuthTicket();
                $oTicket->setModuleName('auth');
                $oTicket->setActionName('activate');
                $oTicket->setObjectId($oItem->id);
                $sToken = $oTicket->insert();

                $aParams = array();

                // активация по e-mail
                if ( $iStatus == Api::ACTIVATE_EMAIL) {
                    $sBody = ModulesParams::getByName('auth', 'mail_user_activate');

                    $aParams['link'] = Site::httpDomain().Api::getAuthPath().'?cmd=AccountActivation&token='.$sToken;

                    Mailer::sendMail( $oItem->email, ModulesParams::getByName('auth', 'mail_title_mail_activate'), $sBody, $aParams);
                }

                $sBody = ModulesParams::getByName('auth', 'mail_admin_activate');

                $aParams['link'] = Site::httpDomainSlash().'admin/#out.left.tools=Auth;out.tabs=tools_Auth';

                Mailer::sendMailAdmin( ModulesParams::getByName('auth', 'mail_title_admin_newuser'), $sBody, $aParams);

                return true;
            }
        } else
            $sError = $oItem->getValidateErrorMessage();

        return false;
    }

    /**
     * Установить данные пользователя
     * @param array $aData Новые данные пользователя
     * @param int $iUserId Id пользователя
     * @return Users|bool
     */
    public static function setUserData($aData, $iUserId = 0) {

        $oUser = $iUserId ? PageUsers::findOne($iUserId) : new PageUsers();
        if (!$oUser) return false;

        foreach($aData as $sField => $sVal) {
            $oUser->$sField = (($sField == 'email')||($sField == 'login'))?strtolower($sVal):$sVal;
        }
        return $oUser;
    }

    /**
     * Получение пользователя по логину
     * @param string $sLogin
     * @return array $oUser
     */
    public static function getUserByLogin( $sLogin ) {
        $sLogin = strtolower($sLogin);
        $oUser = PageUsers::findOne(["login" => $sLogin]);
        return $oUser;
    }


    /**
     * Генерация токена и отправка сообщения о возможности смены пароля
     * @param PageUsers $oUser
     * @return bool
     */
    public static function recoverPass( PageUsers $oUser ) {

        $oTicket = new AuthTicket();
        $oTicket->setModuleName('auth');
        $oTicket->setActionName('recover_pass');
        $oTicket->setObjectId($oUser->id);

        $sToken = $oTicket->insert();

        $bRes = $oUser->email and $oUser->save();

        if ( $bRes ) {

            $sBody = ModulesParams::getByName('auth', 'mail_reset_password');
            $aParams['link'] = Site::httpDomain().self::getAuthPath().'?cmd=newPassForm&token='.$sToken;

            Mailer::sendMail( $oUser->email, ModulesParams::getByName('auth', 'mail_title_reset_password'), $sBody, $aParams);

        }

        return $bRes;
    }


    /**
     * Сохранение нового пароля и сброс токена
     * @param Users $oUser
     * @param $sPassword
     * @return bool
     */
    public static function saveNewPass( Users $oUser, $sPassword ) {

        $oUser->pass = Auth::buildPassword( $oUser->login, $sPassword );

        $bRes = $oUser->save();

        // send mail
        if ( $bRes ){
            $bRes = Mailer::sendMail($oUser->email,
                ModulesParams::getByName('auth', 'mail_title_new_pass'),
                ModulesParams::getByName('auth', 'mail_new_pass')
            );
        }

        return $bRes;
    }


    /**
     * Активация аккаунта
     * @param PageUsers $oUser
     * @return bool
     */
    public static function accountActivate( PageUsers $oUser ) {

        $oUser->active = 1;

        if ( $oUser->save() ){
            $sBody = static::getTextMailActivate();

            Mailer::sendMail( $oUser->email, ModulesParams::getByName('auth', 'mail_title_mail_activate'), $sBody);

            return true;
        }

        return false;
    }

    /**
     * получить текст письма уведамления для активации пользователя
     */
    public static function getTextMailActivate(){
        return ModulesParams::getByName('auth', 'mail_activate');
    }


    /**
     * получить текст письма уведамления для снятия бана
     */
    public static function getTextMailCloseBan(){
        return ModulesParams::getByName('auth', 'mail_close_ban');
    }

    /**
     * получить текст письма уведамления о блокировании
     */
    public static function getTextMailBanned(){
        return ModulesParams::getByName('auth', 'mail_banned');
    }

} 