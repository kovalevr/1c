<?php

/* main */
use skewer\base\site\Layer;

$aConfig['name']     = 'News';
$aConfig['version']  = '2.0';
$aConfig['title']    = 'Новости';
$aConfig['description']  = 'Модуль новостной системы';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::PAGE;
$aConfig['useNamespace'] = true;

$aConfig['dependency'][] = array("PathLine", Layer::PAGE);
$aConfig['dependency'][] = array("Title", Layer::PAGE);
$aConfig['dependency'][] = array("SEOMetatags", Layer::PAGE);

/* Настраиваемые параметры модуля */
$aConfig['param_settings'] = 'skewer\build\Page\News\ParamSettings';

return $aConfig;
