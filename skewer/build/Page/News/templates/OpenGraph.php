<?php

use skewer\base\router\Router;
use skewer\base\section\Parameters;
use skewer\base\site\Site;
use skewer\components\design\DesignManager;
use skewer\components\gallery\Album;
use \skewer\base\SysVar;
use \skewer\components\seo;
use yii\helpers\ArrayHelper;
use \yii\helpers\StringHelper;

/**
 * @var \skewer\build\Adm\News\models\News $oNews
 * @var \skewer\components\seo\SeoPrototype $oSeoComponent
 */

?>
<?
    $oSeoComponent->initSeoData();
    $sOgTitle = (!empty($oSeoComponent->title))? $oSeoComponent->title : $oNews->title;

    if (!empty($oSeoComponent->description))
        $sOgDescription = $oSeoComponent->description;
    else{
        $sOgDescription = $oNews->announce;
        if (!$sOgDescription)
            $sOgDescription = $oSeoComponent->parseField('description', ['sectionId' => $oSeoComponent->getSectionId()]);
    }

    if (!$oSeoComponent || !($sOgPhoto = Album::getFirstActiveImage($oSeoComponent->seo_gallery, 'format_openGraph'))){
        if (SysVar::get('News.hideGallery') || !($sOgPhoto = Album::getFirstActiveImage($oNews->gallery, 'big'))){
            $iGalleryId = (int) Parameters::getValByName(Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'photoOpenGraph');
            $sOgPhoto = Album::getFirstActiveImage($iGalleryId, 'format_openGraph');

            if (!$sOgPhoto){
                // логотип сайта
                if ($aParam = DesignManager::getParam("page.head.logo.logo"))
                    $sOgPhoto = ArrayHelper::getValue($aParam, 'value', '');
            }
        }
    }

    $aImageSize = @getimagesize(WEBPATH .$sOgPhoto);
    $iMaxLength = (int)Parameters::getValByName(Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA,'sum_symbols');
?>


<meta property="og:type" content="article" />
<meta property="og:url" content="<?=  Site::httpDomain() . Router::rewriteURL($oNews->getUrl()) ?>" />
<meta property="og:title" content="<?= seo\Api::prepareRawString($sOgTitle) ?>" />
<meta property="og:description" content="<?= ($iMaxLength)? StringHelper::truncate(seo\Api::prepareRawString($sOgDescription),$iMaxLength) : seo\Api::prepareRawString($sOgDescription) ?>" />
<meta property="og:image" content="<?= Site::httpDomain() . $sOgPhoto ?>" />
<? if ($aImageSize):?>
    <meta property="og:image:width" content="<?= $aImageSize[0] ?>" />
    <meta property="og:image:height" content="<?= $aImageSize[1] ?>" />
<? endif; ?>
<meta prefix="article: <?=WEBPROTOCOL?>ogp.me/ns/article#" property="article:published_time" content="<?= Yii::$app->getFormatter()->asDate($oNews->publication_date,'php:d-m-Y')?>" />
<meta prefix="article: <?=WEBPROTOCOL?>ogp.me/ns/article#" property="article:modified_time" content="<?= Yii::$app->getFormatter()->asDate($oNews->last_modified_date,'php:d-m-Y')?>" />