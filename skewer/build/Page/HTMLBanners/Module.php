<?php

namespace skewer\build\Page\HTMLBanners;

use skewer\build\Adm\HTMLBanners\models\Banners;
use skewer\base\section\Tree;
use skewer\base\site_module;

class Module extends site_module\page\ModulePrototype {

    public $Location = 'left';
    private $sFileTemplate;
    public $SectionId = 0;
    /**
     * @var int ID главной страницы
     */
    private $defaultSection = 0;

    public function init(){

        $this->setParser(parserPHP);

        $this->SectionId = $this->getEnvParam('sectionId');
        $this->defaultSection = \Yii::$app->sections->main();
        $this->sFileTemplate = 'banner_'.$this->Location.'.php';
    }

    public function execute(){

        \Yii::$app->router->setLastModifiedDate(Banners::getMaxLastModifyDate());

        $aParams = array();
        $aParams['location'] = $this->Location;
        $aParams['current_section'] = $this->SectionId;

        if ( $this->SectionId == $this->defaultSection ){

            $aBanners = Banners::getBannersOnMain($this->SectionId,$this->Location);
        }        else{

            $aParentSections = Tree::getSectionParents($this->SectionId);
            if ( $aParentSections )
                $aParams['parent_sections'] = implode(',', $aParentSections);
            else $aParams['parent_sections'] = $this->SectionId;

            $aBanners = Banners::getBanners($this->SectionId,$aParentSections,$this->Location);
        }
        
        if ( $aBanners )
            $this->setData('dataProvider', $aBanners);

        $this->setTemplate($this->sFileTemplate);


        return psComplete;
    }

    /** @inheritdoc */
    public function canHaveContent() {
        return false;
    }

}//class