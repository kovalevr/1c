<?php

namespace skewer\build\Page\CatalogViewer;

use skewer\build\Catalog\Goods\SeoGood;
use skewer\build\Catalog\Goods\SeoGoodModifications;
use skewer\components\auth\models\GroupPolicy;
use skewer\components\catalog;
use skewer\components\config\UpdateException;
use skewer\components\i18n\Languages;
use skewer\base\orm\Query;
use skewer\components\forms;
use skewer\base\site;
use skewer\components\seo;
use skewer\components\gallery\Profile;
use skewer\components\auth\Policy;
use skewer\components\config\InstallPrototype;
use skewer\base\SysVar;
use skewer\base\section;

/**
 * Class Install
 * @package skewer\build\Page\CatalogViewer
 */
class Install extends InstallPrototype {


    public function init() {
        return true;
    }// func


    public function install() {

        /** установка таблиц в бд */
        $this->buildTables();

        /** Профиль галереи. Профили должны устанавливаться раньше создания полей базовой карточки, т. к. там устанавливается профиль по умолчанию */
        $this->addGalleryProfiles();

        /** сборка данных и базовой карточки */
        $iBaseCard = catalog\Generator::genBaseCard();

        /** Расширенная карточка */
        $card = catalog\Generator::createExtCard( $iBaseCard, 'dopolnitelnye_parametry', \Yii::t('data/catalog', 'cart_ext_title', [], \Yii::$app->language ) );
        catalog\Generator::createField(
            $card->id,
            [
                'name' => 'proizvoditel',
                'title' => \Yii::t('data/catalog', 'field_producer_title', [], \Yii::$app->language ),
                'editor' => 'string',
            ]
        );
        $card->updCache();

        $aLanguages = Languages::getAllActiveNames();

        $iOrderFormSection = 0;
        foreach($aLanguages as $sName){

            /** Создаем форму */
            $iFormId = $this->addOrderForm($sName);

            /** Добавление раздела с формой */
            $iOrderFormSection = $this->addSection4OrderForm($iFormId, $sName);

            /** Каталог на главной */
            $this->addCatalogOnMain($iOrderFormSection, $sName);

        }

        /** SEO template */
        $this->addSeoTemplate();

        /** Шаблон каталожного раздела */
        /** @todo запомнится последняя форма! */
        $this->addCatalogSectionTemplate($iOrderFormSection);

        /** соц кнопки */
        $this->addSocialButton();

        /** sysvars */
        $this->updateSysVars();

        /** Добавление доступов в каталог для админов */
        $this->setPolicy();

        return true;
    }// func


    public function uninstall() {

        $this->fail("Нельзя удалить каталог");

        return true;
    }// func


    /**
     * Изменение базы данных
     */
    private function buildTables() {

        /** Атрибуты */
        catalog\model\FieldAttrTable::rebuildTable();

        /** Группы полей */
        catalog\model\FieldGroupTable::rebuildTable();

        /** Поля */
        catalog\model\FieldTable::rebuildTable();

        /** Валидаторы */
        catalog\model\ValidatorTable::rebuildTable();

        /*Таблица связи с разделами "Сопутствующие"*/
        \Yii::$app->db->createCommand("CREATE TABLE IF NOT EXISTS `related_sections` (
                                      `id` int(10) NOT NULL AUTO_INCREMENT,
                                      `target_section` int(100) NOT NULL,
                                      `related_section` int(100) NOT NULL,
    									PRIMARY KEY (`id`)
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")->execute();

        /** Сущности */
        Query::dropTable( 'c_entity' );
        catalog\model\EntityTable::rebuildTable();


        /** Связи товаров */
        catalog\model\SectionTable::rebuildTable();
        catalog\model\GoodsTable::rebuildTable();
        catalog\model\SemanticTable::rebuildTable();

    }


    /**
     * Создаем форму
     * @param $sLanguage
     * @return int
     */
    private function addOrderForm($sLanguage){

        $aFormData = array(
            'form_name' => 'forma-zakaza-tovara',
            'form_title' => \Yii::t('data/order', 'order_form_title', [], $sLanguage),
            'form_handler_type' => 'toBase',
            'form_is_template' => 1,
            'form_answer' => 1,

        );

        $iFormId = 0;
        $oForm = forms\Table::getNewRow($aFormData);
        if ($oForm){
            /** Нужно для создания таблицы, в которую будут сохраняться заказы */
            $oForm->save();
            $oForm->preSave();
            $iFormId = $oForm->getId();
        }

        if (!$iFormId){
            $this->fail('Не удалось создать форму!');
        }

        /**
         * Поля
         */
        $aFields = array(
            array(
                'param_section_id' => 0,
                'param_name' => 'person',
                'param_title' => \Yii::t('data/order', 'order_form_field_person', [], $sLanguage),
                'param_description' => '',
                'param_type' => 1,
                'param_required' => 0,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'text',
                'param_depend' => '',
                'param_man_params' => '',
                'param_priority' => 3,
                'label_position' => 'top',
                'new_line' => 1,
                'width_factor' => 1
            ),
            array(
                'param_section_id' => 0,
                'param_name' => 'phone',
                'param_title' => \Yii::t('data/order', 'order_form_field_phone', [], $sLanguage),
                'param_description' => '',
                'param_type' => 1,
                'param_required' => 1,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'text',
                'param_depend' => '',
                'param_man_params' => 'data-mask="phone"',
                'param_priority' => 4,
                'label_position' => 'top',
                'new_line' => 0,
                'width_factor' => 1
            ),
            array(
                'param_section_id' => 0,
                'param_name' => 'email',
                'param_title' => \Yii::t('data/order', 'order_form_field_email', [], $sLanguage),
                'param_description' => '',
                'param_type' => 1,
                'param_required' => 1,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'email',
                'param_depend' => '',
                'param_man_params' => '',
                'param_priority' => 5,
                'label_position' => 'top',
                'new_line' => 0,
                'width_factor' => 1
            ),
            array(
                'param_section_id' => 0,
                'param_name' => 'text',
                'param_title' => \Yii::t('data/order', 'order_form_field_text', [], $sLanguage),
                'param_description' => '',
                'param_type' => 2,
                'param_required' => 0,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'text',
                'param_depend' => '',
                'param_man_params' => '',
                'param_priority' => 7,
                'label_position' => 'top',
                'new_line' => 1,
                'width_factor' => 1
            ),
            array(
                'param_section_id' => 0,
                'param_name' => 'naimenovanie-tovara',
                'param_title' => \Yii::t('data/order', 'order_form_field_goods_name', [], $sLanguage),
                'param_description' => '',
                'param_type' => 1,
                'param_required' => 0,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'text',
                'param_depend' => '',
                'param_man_params' => '',
                'param_priority' => 1,
                'label_position' => 'top',
                'new_line' => 1,
                'width_factor' => 1
            )
        );

        foreach($aFields as $aField){
            $aField['form_id'] = $iFormId;
            $oFormField = forms\FieldTable::getNewRow($aField);
            $oFormField->save();
        }

        if ($oForm){
            /** Нужно для создания таблицы, в которую будут сохраняться заказы */
            $oForm->preSave();
        }

        /** Письмо-автоответ */
        $this->executeSQLInsert("INSERT INTO `forms_add_data`
        (`form_id`, `answer_title`, `answer_body`, `agreed_title`, `agreed_text`)
        VALUES (:form_id, :answer_title, :answer_body, :agreed_title, :agreed_text)",
            array(
                'form_id' => $iFormId,
                'answer_title' => \Yii::t('data/order', 'order_form_add_answer_title', [], $sLanguage),
                'answer_body' => \Yii::t('data/order', 'order_form_add_answer_body', [], $sLanguage),
                'agreed_title' => \Yii::t('data/order', 'order_form_add_agreed_title', [], $sLanguage),
                'agreed_text' => \Yii::t('data/order', 'order_form_add_agreed_text', [], $sLanguage)
            ));

        /** Связи с полями карточки */

        /**
         * Создаем таблицу руками, по другому падает с ошибкой
         */
        $sQuery = "CREATE TABLE IF NOT EXISTS `forms_links` (
                        `link_id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
                        `form_id` INT( 11 ) DEFAULT NULL ,
                        `form_field` VARCHAR( 255 ) DEFAULT NULL ,
                        `card_field` VARCHAR( 255 ) DEFAULT NULL ,
                        PRIMARY KEY (`link_id`)
                    ) ENGINE = MYISAM DEFAULT CHARSET = utf8;";

        $this->executeSQLQuery($sQuery);

        $this->executeSQLInsert("INSERT INTO `forms_links`
        (`link_id`, `form_id`, `form_field`, `card_field`)
        VALUES (null, :form_id, :form_field, :card_field )",
            array(
                'form_id' => $iFormId,
                'form_field' => 'naimenovanie-tovara',
                'card_field' => 'title'
            ));

        return $iFormId;
    }

    /**
     * Добавление раздела с формой
     * @param $iForm
     * @param $sLanguage
     * @throws \Exception
     * @throws UpdateException
     * @return bool|int
     */
    private function addSection4OrderForm($iForm, $sLanguage){
        $iTplSection = $this->addSectionByTemplate(
            \Yii::$app->sections->getValue('tools', $sLanguage),
            \Yii::$app->sections->tplNew(),
            'zayavka-na-tovar',
            \Yii::t('data/app', 'section_orderForm', [], $sLanguage),
            1
        );

        if (!$iTplSection){
            throw new \Exception('Не удалось создать раздел для формы заказа!');
        }

        /** раздел должен быть неудаляемый! */
        $this->setParameter($iTplSection, '_break_delete', '.', '1', '', 'Системный (неудаляемый) раздел', 0);

        /**
         * добавить связь раздела с формой
         */
        forms\Table::link2Section($iForm, $iTplSection);

        $this->setServiceSections('orderForm', \Yii::t('app', 'orderForm', [], $sLanguage), $iTplSection, $sLanguage);

        return $iTplSection;
    }

    /**
     * Добавление профиля галлереи для каталога
     */
    private function addGalleryProfiles(){
        $profile_id = Profile::setProfile([
            'type' => Profile::TYPE_CATALOG,
            'alias' => 'catalog',
            'title' => \Yii::t('data/gallery', 'profile_catalog_name', [], \Yii::$app->language ),
            'active' => 1
        ]);

        if (!$profile_id)
            $this->fail('cant create gallery profile');
    }

    /**
     * Добавим шаблон SEO для каталожных страниц
     */
    private function addSeoTemplate(){

        /**
         * @todo lang сео на языках?
         */

        /** Добавляем общий для всех карточек seo - шаблон */

        if (!seo\Template::find()->where('alias', SeoGood::getAlias())->getOne()){
            seo\Template::getNewRow(
                array(
                    'alias' => SeoGood::getAlias(),
                    'name' => 'Страница товара в каталоге',
                    'title' => '[Название товара]. [Название страницы]. [Цепочка разделов до главной] [Название сайта]',
                    'description' => '[Название сайта], [Цепочка разделов до страницы], [Название страницы], [Название товара]',
                    'keywords' => '[название товара], [название страницы], [цепочка разделов до главной], [Название сайта]',
                    'info' => '[Название товара] - название товара
                        [название товара] - название товара с маленькой буквы',
                    'altTitle' => "[Название товара]",
                    'nameImage' => "[Название товара]"
                )
            )
                ->save();
        }

        /** Добавляем seo - шаблон для товаров-аналогов */
        if (!seo\Template::find()->where('alias', SeoGoodModifications::getAlias())->getOne()){
            seo\Template::getNewRow(
                array(
                    'alias' => SeoGoodModifications::getAlias(),
                    'name' => 'Страница аналогов',
                    'title' => '[Название товара]. [Название страницы]. [Цепочка разделов до главной] [Название сайта]',
                    'description' => '[Название сайта], [Цепочка разделов до страницы], [Название страницы], [Название товара]',
                    'keywords' => '[название товара], [название страницы], [цепочка разделов до главной], [Название сайта]',
                    'info' => "[Название товара] - название товара \n [название товара] - название товара с маленькой буквы",
                    'altTitle' => "[Название товара]",
                    'nameImage' => "[Название товара]"
                )
            )
                ->save();
        }

    }

    /**
     * Добавление шаблона каталожного раздела
     * @param $iOrderFormSection
     * @throws \Exception
     */
    private function addCatalogSectionTemplate($iOrderFormSection){

        $iTplSection = $this->addSectionByTemplate(
            \Yii::$app->sections->templates(),
            \Yii::$app->sections->tplNew(),
            'katalog',
            \Yii::t('data/catalog', 'template_catalog_section', [], \Yii::$app->language ),
            1
        );

        if (!$iTplSection){
            throw new \Exception('Не удалось создать шаблон каталожного раздела');
        }

        /*
         * Добавим в ServiceSections
         */
        $sLanguage = \Yii::$app->language;
        \Yii::$app->db->createCommand("INSERT INTO `ServiceSections` (`name`, `value`, `language`, `title`)
                                       VALUES ('catalog_tpl', '$iTplSection', '$sLanguage', '');")->execute();

        /**
         * накидаем параметры
         */

        /** для дизайнерского */
        $this->setParameter($iTplSection, '.title', '.layout', 'Каталог', '', 'Название шаблона расположения', 0);
        $this->setParameter($iTplSection, '.order', '.layout', '10', '', 'вес при сортировке', 0);
        $this->setParameter($iTplSection, 'content', '.layout', 'pathLine,bannerContentTop,title,staticContent,CategoryViewer,content,socialButtons,forms,staticContent2', '', 'Контент (центр)', 0);

        $this->setParameter($iTplSection, 'title', 'CatalogFilter', '', '', \Yii::t('catalogFilter', 'BlockTitle', [], \Yii::$app->language ), 1);
        $this->setParameter($iTplSection, 'layout', 'CatalogFilter', 'content,left,right,head', '', '', 0);
        $this->setParameter($iTplSection, '.title', 'CatalogFilter', 'Фильтр для каталога', '', '', 0);

        $this->setParameter($iTplSection, 'relatedTpl', 'content', 'gallery', '', '', 0);
        $this->setParameter($iTplSection, 'includedTpl', 'content', 'gallery', '', '', 0);
        $this->setParameter($iTplSection, 'showFilter', 'content', 0, '', \Yii::t('catalog', 'showFilter', [], \Yii::$app->language ), 0);
        $this->setParameter($iTplSection, 'listTemplate', 'content', 'list', '', \Yii::t('catalog', 'template_catalog_list_template', [], \Yii::$app->language ), 0);
        $this->setParameter($iTplSection, 'object', 'content', 'CatalogViewer', '', '', 0);
        $this->setParameter($iTplSection, 'objectAdm', 'content', 'Catalog', '', '', 0);
        $this->setParameter($iTplSection, 'onPage', 'content', 12, '', \Yii::t('catalog', 'template_catalog_on_page'), 0);
        $this->setParameter($iTplSection, 'showSort', 'content', 0, '', \Yii::t('catalog', 'showSort', [], \Yii::$app->language ), 0);
        $this->setParameter($iTplSection, 'viewCategory', 'content', '', '', \Yii::t('catalog', 'view_category', [], \Yii::$app->language ), 0);

        // Языковой параметр раздела с формой заказа
        $this->setParameter(\Yii::$app->sections->languageRoot(), 'buyFormSection', catalog\Api::LANG_GROUP_NAME, $iOrderFormSection, '', \Yii::t('catalog', 'template_catalog_order_form', [], \Yii::$app->language ), 0);
        $this->setParameter(\Yii::$app->sections->tplNew(), 'buyFormSection', catalog\Api::LANG_GROUP_NAME, '', '', \Yii::t('catalog', 'template_catalog_order_form', [], \Yii::$app->language ), section\params\Type::paramLanguage);
    }

    /**
     * Добавление каталога на главной
     * @param $iFormSection
     * @param $sLanguage
     */
    private function addCatalogOnMain($iFormSection, $sLanguage){

        $iMainSection = \Yii::$app->sections->getValue('main', $sLanguage);

        $this->setParameter($iMainSection, '.title', 'catalog', 'catalog.catalog_title', '', 'Название шаблона расположения', 0);
        $this->setParameter($iMainSection, 'layout', 'catalog', 'content', '', '', 0);
        $this->setParameter($iMainSection, 'object', 'catalog', 'CatalogViewer', '', '', 0);
        $this->setParameter($iMainSection, 'onMain', 'catalog', 'on_main', '', 'Вывод на главной', 0);
        $this->setParameter($iMainSection, 'onPage', 'catalog', '3', '', \Yii::t('catalog', 'param_on_page', [], \Yii::$app->language ), 0);
        $this->setParameter($iMainSection, 'titleOnMain', 'catalog', 'Каталог', '', \Yii::t('catalog', 'param_title_on_main', [], $sLanguage), 0);
        $this->setParameter($iMainSection, 'listTemplate', 'catalog', 'gallery', 'list '.\Yii::t('editor', 'type_catalog_list', [], $sLanguage)."\n"
            .'gallery '.\Yii::t('editor', 'type_catalog_gallery', [], $sLanguage), \Yii::t('editor', 'type_catalog_view', [], $sLanguage), 0);

        $this->setParameter($iMainSection, '.title', 'catalog2', 'catalog.catalog_hits', '', 'Название шаблона расположения', 0);
        $this->setParameter($iMainSection, 'layout', 'catalog2', 'content', '', '', 0);
        $this->setParameter($iMainSection, 'object', 'catalog2', 'CatalogViewer', '', '', 0);
        $this->setParameter($iMainSection, 'onMain', 'catalog2', 'hit', '', 'Вывод на главной', 0);
        $this->setParameter($iMainSection, 'onPage', 'catalog2', '3', '', \Yii::t('catalog', 'param_on_page', [], \Yii::$app->language ), 0);
        $this->setParameter($iMainSection, 'titleOnMain', 'catalog2', 'Hits', '', \Yii::t('catalog', 'param_title_on_main', [], $sLanguage), 0);
        $this->setParameter($iMainSection, 'listTemplate', 'catalog2', 'gallery', 'list '.\Yii::t('editor', 'type_catalog_list', [], $sLanguage)."\n"
            .'gallery '.\Yii::t('editor', 'type_catalog_gallery', [], $sLanguage), \Yii::t('editor', 'type_catalog_view', [], $sLanguage), 0);

        $this->setParameter($iMainSection, '.title', 'catalog3', 'catalog.catalog_news', '', 'Название шаблона расположения', 0);
        $this->setParameter($iMainSection, 'layout', 'catalog3', 'content', '', '', 0);
        $this->setParameter($iMainSection, 'object', 'catalog3', 'CatalogViewer', '', '', 0);
        $this->setParameter($iMainSection, 'onMain', 'catalog3', 'new', '', 'Вывод на главной', 0);
        $this->setParameter($iMainSection, 'onPage', 'catalog3', '3', '', \Yii::t('catalog', 'param_on_page', [], \Yii::$app->language ), 0);
        $this->setParameter($iMainSection, 'titleOnMain', 'catalog3', 'News', '', \Yii::t('catalog', 'param_title_on_main', [], $sLanguage), 0);
        $this->setParameter($iMainSection, 'listTemplate', 'catalog3', 'gallery', 'list '.\Yii::t('editor', 'type_catalog_list', [], $sLanguage)."\n"
            .'gallery '.\Yii::t('editor', 'type_catalog_gallery', [], $sLanguage), \Yii::t('editor', 'type_catalog_view', [], $sLanguage), 0);

    }

    /**
     * Добавляем значения нужных полей в sysvar
     */
    private function updateSysVars(){
        SysVar::set( 'syte_type', site\Type::catalog );
        SysVar::set( 'catalog.random_related_count', 4 );
    }

    /**
     * Добавление параметра о соц. кнопке
     */
    private function addSocialButton(){
        $this->setParameter(\Yii::$app->sections->root(), 'soclinkGoods', 'socialButtons', '', '', 'SocialButtons.social_catalog', -5);
    }

    /**
     * Добавление доступа в панель каталога
     */
    private function setPolicy(){

        $aPolicy = GroupPolicy::find()
            ->where(['!=','access_level',0])
            ->andWhere(['area'=>'admin'])
            ->asArray()
            ->all();

        if ($aPolicy)
            foreach ($aPolicy as $aItem)
                Policy::setGroupActionParam(
                    $aItem['id'],
                    'skewer\build\Catalog\LeftList\Module',
                    'useCatalog',
                    1
                );

    }

}// class