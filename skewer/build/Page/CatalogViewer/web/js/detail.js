$(document).ready(function(){
    // инициализация табов
    $(".js-tabs").tabs();
});

$(window).load(function () {
    $('.catalogbox__galbox ul').carouFredSel({
        auto: false,
        prev: '.catalogbox__back',
        next: '.catalogbox__next',
        width: '100%'
    });

    // Адаптивные слайдеры сопутствующих товаров и товаров в комплекте
    $('.js-catalog_slider').each(function(){

        $(this).slick({
            respondTo: 'slider',

            arrows: true, // Включить стрелки перелистывания
            prevArrow: $(this).siblings('.js-catalog_slider_back'),
            nextArrow: $(this).siblings('.js-catalog_slider_next'),

            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [
                {
                    breakpoint: 1080,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 920,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 660,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    });

});