<?php

namespace skewer\build\Page\CatalogViewer\State;

use skewer\build\Catalog\Collections\SeoElementCollection;
use skewer\build\Design\Zones;
use skewer\components\catalog;
use skewer\base\site;
use skewer\components\seo;
use skewer\components\seo\SeoPrototype;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Объект вывода списка товарных позиций для старницы бренда
 * Class CollectionPage
 * @package skewer\build\Page\CatalogViewer\State
 */
class CollectionPage extends ListPage {

    /** @var int Идентификатор позиции в коллекции */
    protected $collection = 0;

    /** @var bool Имя карточки коллекции */
    private $card = false;

    /** @var bool Имя поля в карточке товара */
    private $field = false;

    /** @var array */
    private $obj = [];

    /** @var string Шаблон вывода */
    protected $sMainTpl = 'CollectionPage.twig';


    /**
     * Инициализация объекта коллекции по идентификатору
     * @param int|string $collection id or alias
     * @throws NotFoundHttpException
     * @return bool
     */
    public function setCollectionId( $collection ) {

        list($this->card, $this->field) = explode(':', $this->getModuleField('collectionField'));

        $this->obj = catalog\ObjectSelector::getElementCollection($collection, $this->card, $this->sectionId());

        if ( !$this->obj )
            throw new NotFoundHttpException('Collection not found');

        \Yii::$app->router->setLastModifiedDate($this->obj['last_modified_date']);

        $this->collection = ArrayHelper::getValue($this->obj, 'id', 0);

        return true;
    }


    /**
     * Получение списка товарных позиций для текущей страницы
     * @return bool
     */
    protected function getGoods() {

        if ( !$this->iCount )
            return false;

        $oSelector = catalog\GoodsSelector::getList4Collection( $this->card, $this->field, $this->collection );

        if ( !$oSelector ) return false;

        $this->list = $oSelector
            ->condition( 'active', 1 )
            ->sort( $this->sSortField, ($this->sSortWay == 'down' ? 'DESC' : 'ASC') )
            ->limit( $this->iCount, $this->iPageId, $this->iAllCount )
            ->withSeo($this->getSection())
            ->parse()
        ;

        return true;
    }


    public function build() {

        $this->oModule->setStatePage( Zones\Api::DETAIL_LAYOUT );

        if ( !$this->obj )
            return false;// todo exception

        $this->getModule()->setData( 'useMainSection', 1 );

        // todo придется перенести код и адаптировать под текущую механику
        parent::build();

        //$this->showNearItems();

        $this->getModule()->setData( 'collection', $this->obj );
        $this->getModule()->setData( 'view', $this->sTpl );

        $this->getModule()->setTemplate( $this->sMainTpl );

        if ( is_numeric($this->card) ){
            $iCardId = $this->card;
            $sCardName = catalog\Card::getName($this->card);
        } else {
            $sCardName = $this->card;
            $oCollection = catalog\Card::getCollection( $sCardName );
            $iCardId = $oCollection->getVal('id');
        }

        $this->setSeo(new SeoElementCollection(0, $iCardId, $this->obj, $sCardName ));

        site\Page::setTitle( $this->obj['title'] );

        site\Page::setAddPathItem( $this->obj['title'] );

        return true;
    }

    public function setSeo(SeoPrototype $oSeo){

        $this->oModule->setEnvParam(seo\Api::SEO_COMPONENT, $oSeo );
        $this->oModule->setEnvParam(seo\Api::OPENGRAPH, '');
        site\Page::reloadSEO();
    }


    /**
     * Вывод PathLine
     */
    protected function setPathLine() {

        // todo и все таки параметры должны браться из запросника после обработки
        $aURLParams = $_GET;
        unSet( $aURLParams['page'] );
        unSet( $aURLParams['url'] );

        $aURLParams['goods-alias'] = ArrayHelper::getValue( $this->obj, 'alias', 0 );// fixme изза этой строчку дублировал метод

        foreach( $aURLParams as $sKey=>$mParam )
            if ( is_array( $mParam ) ) {

                foreach( $mParam as $sWKey=>$mValue )
                    $aURLParams[ $sKey . '[' .$sWKey . ']' ] = $mValue;

                unSet( $aURLParams[$sKey] );
            }

        if ( $this->sSortField )
            $aURLParams['sort'] = $this->sSortField;

        if ( $this->sSortWay )
            $aURLParams['way'] = $this->sSortWay;

        if ( $this->sTpl )
            $aURLParams['view'] = $this->sTpl;

        $this->getModule()->getPageLine(
            $this->iPageId,
            $this->iAllCount,
            $this->sectionId(),
            $aURLParams,
            ['onPage' => $this->iCount]
        );

    }

    protected function showNearItems() {

        $aData = [
            'section' => $this->sectionId(),
            'next' => catalog\ObjectSelector::getNext( $this->collection, $this->card ),
            'prev' => catalog\ObjectSelector::getPrev( $this->collection, $this->card )
        ];

        $this->oModule->setData( 'nearItems', $aData );
    }


}