<?php

namespace skewer\build\Page\GuestBook;

use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    
    public $sourcePath = '@skewer/build/Page/GuestBook/web/';

    public $css = [
        'css/custom.css'
    ];

    public $js = [
        'js/jquery.rating.js',
        'js/guestbook.init.js',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\build\Page\Forms\Asset',
        'skewer\libs\owlcarousel\Asset'
    ];

    public function init()
    {
        parent::init();
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            if (is_dir($from) && basename($from)=='images'){
                FileHelper::copyDirectory($from,WEBPATH.'images/');
            }
            return true;
        };
    }

}