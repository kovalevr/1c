$(function() {
    if ($('.js-reviews-carousel').length) {
        $('.js-reviews-carousel').addClass('owl-carousel').owlCarousel({
            items: 3,
            margin: 20,
            dots: false,
            nav: true,
            navText: false
        });
    };
});//ready