$(document).ready(function() {

    $('a.single_3').fancybox(
        $.extend(commonFancyBoxConfig,{
            openEffect: 'fade',
            nextEffect: 'elastic',
            prevEffect: 'elastic',
            helpers : {
                title : {
                    type : 'inside'
                }
            },
            arrows    : true,
            nextClick : true,
            mouseWheel: true,
            closeBtn: true
        })
    );


    $('.js-gal_slider').each(function(){

        $(this).carouFredSel({
            circular: false,
            auto 	: false,
            width : '100%',
            align : 'center',
            // Раскомментировать для использования пагинатора/булетов
            // pagination: $(this).siblings('.js_pagination'),
            prev	: {
                button	: $(this).siblings('.js-but_prev'),
                key		: "left",
                items: 1
            },
            next	: {
                button	: $(this).siblings('.js-but_next'),
                key		: "right",
                items: 1
            }

        });
    });

});
