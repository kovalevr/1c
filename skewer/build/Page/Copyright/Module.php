<?php

namespace skewer\build\Page\Copyright;

use skewer\base\site_module;
use skewer\components\lp;


/**
 * Модуль антикопипаста
 * Class Module
 * @package skewer\build\Page\Copyright
 */
class Module extends site_module\page\ModulePrototype implements lp\ModuleInterface{

    /**
     * @const Группа параметров модуля
     */
    const GROUP_COPYRIGHT = 'Copyright';

    public $sTemplate = 'copyright.php';

    public function init() {
        $this->setParser(parserPHP);
        return true;
    }

    public function autoInitAsset() {
        return false;
    }


    public function execute(){

        $this->setTemplate($this->sTemplate);
        return psComplete;
    }

    /** @inheritdoc */
    public function canHaveContent() {
        return false;
    }

} 