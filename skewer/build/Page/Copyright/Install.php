<?php

namespace skewer\build\Page\Copyright;

use skewer\base\section\models\ParamsAr;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\build\Design\Zones\Api;
use skewer\components\config\InstallPrototype;
use skewer\components\i18n\ModulesParams;
use yii\db\Expression;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }

    public function install() {
        /** Добавляем модуль в шаблон новой страницы */
        Parameters::setParams(\Yii::$app->sections->tplNew(), Module::GROUP_COPYRIGHT, Parameters::object, Module::getNameModule());
        Parameters::setParams(\Yii::$app->sections->tplNew(), Module::GROUP_COPYRIGHT, Parameters::layout, 'content');


        /** Добавляем модуль во все унаследованные от новой страницы разделы с переопределенной зоной-контент */
        $aSections = Tree::getSubSectionsByTemplate(\Yii::$app->sections->tplNew());
        $aSections[] = \Yii::$app->sections->tplNew();
        $aOverrideContent = [];
        foreach ($aSections as $item){
            $aParam = Parameters::getList( $item )
                ->fields(['id'])
                ->group(Api::layoutGroupName)
                ->name('content')
                ->asArray()->get();

            if ($aParam)
                $aOverrideContent[] = $aParam[0]['id'];
        }

        $sAddPart = ',' . Module::GROUP_COPYRIGHT;
        ParamsAr::updateAll(
            ['show_val' => new Expression("CONCAT(`show_val`, :module)",[':module' => $sAddPart])],
            ['id' => $aOverrideContent]
        );

        return true;
    }

    public function uninstall() {

        Parameters::removeByGroup(Module::GROUP_COPYRIGHT, \Yii::$app->sections->tplNew());

        $sFind = ',' . Module::GROUP_COPYRIGHT;

        $aSections = ParamsAr::find()
            ->where(['group' => Api::layoutGroupName])
            ->andWhere(['name' => 'content'])
            ->andWhere(['LIKE', 'show_val', $sFind])
            ->asArray()->all();

        foreach ($aSections as $aSection)
            Api::deleteLabel(Module::GROUP_COPYRIGHT, $aSection['id']);

        return true;
    }

}// class
