<?php

namespace skewer\build\Page\Cart;

use skewer\components\catalog;
use yii\helpers\ArrayHelper;

/**
 * Класс для работы с заказом корзины
 * Class Order
 * @package skewer\build\Page\Cart
 */
class Order {

    /** Массив позиций заказа */
    private $items = [];

    /** Id последней заказанной/изменённой позиции корзины */
    private $lastItem = 0;

    /**
     * Возвращает существующую позицию или новую
     * @param $iObjectId int ID объекта
     * @return array|bool
     */
    public function getExistingOrNew($iObjectId) {

        if (isset($this->items[$iObjectId]))
            return $this->items[$iObjectId];

        $aGood = catalog\GoodsSelector::get($iObjectId, 1);

        return (!$aGood) ? false : [
            'id_goods' => $iObjectId,
            'url'      => $aGood['url'],
            'title'    => $aGood['title'],
            'article'  => ArrayHelper::getValue($aGood, 'fields.article.value', ''),
            'image'    => ArrayHelper::getValue($aGood, 'fields.gallery.first_img.images_data', ''),
            'count'    => 0,
            'price'    => ArrayHelper::getValue($aGood, 'fields.price.value', ''),
            'total'    => 0,
        ];
    }

    /**
     * Устанавливает позицию заказа
     * @param array $aItem Позиция заказа
     */
    public function setItem(array $aItem) {

        $this->items[$aItem['id_goods']] = $aItem;
        $this->lastItem                  = $aItem['id_goods'];
    }

    /**
     * Удаляет позицию заказа
     * @param $itemId int ID позиции
     */
    public function unsetItem($itemId) {

        if (isset($this->items[$itemId]))
            unset($this->items[$itemId]);
    }

    /**
     * Возвращает массив позиций заказа. Точнее указатель на массив (сделано для оптимизации, иначе создаётся копия массива).
     * @return array Позиции заказа
     */
    public function &getItems() {

        return $this->items;
    }

    /**
     * Возвращает число позиций заказа
     * @return int
     */
    public function getCount() {

        return count($this->items);
    }

    /**
     * Возвращает общую сумму заказа
     * @return int
     */
    public function getTotalPrice() {

        $iTotal = 0;

        foreach ($this->items as &$aItem)
            $iTotal += $aItem['total'];

        return $iTotal;
    }

    /**
     * Возвращает позицию по ID, а именно прямую ссылку
     * @param $id int ID позиции
     * @return array|bool
     */
    public function getItemById($id) {

        return isset($this->items[$id]) ? $this->items[$id] : false;
    }

    /**
     * Удаляет все позиции заказа
     * @return void
     */
    public function unsetAll() {
        $this->items = [];
    }

    /**
     * Возвращает общее количество товаров с учетом количества
     * @return int
     */
    public function getTotalCount() {

        $iCount = 0;

        foreach ($this->items as &$aItem)
            $iCount += $aItem['count'];

        return $iCount;
    }

    /**
     * Возвращает последнюю позицию в виде массива
     * @return array
     */
    public function getItemLast() {

        if (!isset($this->items[$this->lastItem]))
            return [];

        $aItem = $this->items[$this->lastItem];

        return [
            'id_goods' => $aItem['id_goods'],
            'title'    => $aItem['title'],
            'count'    => $aItem['count'],
            'price'    => Api::priceFormatAjax($aItem['price']),
            'total'    => Api::priceFormatAjax($aItem['total']),
        ];
    }
} 