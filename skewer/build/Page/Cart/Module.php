<?php

namespace skewer\build\Page\Cart;

use skewer\build\Adm\Order as AdmOrder;
use skewer\base\section\Tree;
use skewer\build\Tool\Payments as Payments;

use skewer\build\Page;
use \skewer\build\Page\CatalogViewer;
use skewer\components\i18n\ModulesParams;
use skewer\components\auth\Auth;
use skewer\components\auth\CurrentUser;
use skewer\base\site_module;
use skewer\base\site;
use yii\web\NotFoundHttpException;

class Module extends site_module\page\ModulePrototype implements site_module\Ajax {

    /** @var int ID текущего раздела */
    public $sectionId;

    public $bIsFustBuy = false;

    /** @var string имя параметра для команды */
    private $sActionParamName = 'action';

    /**
     * Шаблон вывода списка
     * @var string
     */
    public $templateList = 'list.twig';

    /**
     * Инициализация
     * @return void
     */
    public function init() {
        $this->sectionId = $this->sectionId();
    }

    protected function getActionParamName() {
        return $this->sActionParamName;
    }

    public function getBaseActionName() {
        return 'list';
    }

    /**
     * Запуск модуля
     * @return int
     */
    public function execute() {

        $this->executeRequestCmd();

        $this->bIsFustBuy = $this->getStr('ajaxShow', 0);

        if ($this->getStr('cmd', '') && $this->bIsFustBuy){
            $this->sActionParamName = 'cmd';
        }

        return parent::execute();
    }

    /**
     * Список позиций заказа
     * @return int
     */
    public function actionList() {

        // подключен для js и css для кнопок "+" и "-"
        CatalogViewer\Asset::register(\Yii::$app->getView());

        $this->setTemplate($this->templateList);
        $this->setData('order', Api::getOrder());
        $this->setData('sectionId', $this->sectionId);
        $this->setData('mainSection', \Yii::$app->sections->main());

        return psComplete;
    }

    /**
     * Форма оформления заказа
     * @return int
     * @throws NotFoundHttpException
     */
    public function actionCheckout() {

        $aParams = array(
            'section_path' => Tree::getSectionAliasPath(\Yii::$app->sections->getValue('cart'), true),
            'license_agreement' => ModulesParams::getByName('order', 'license'),
            'fastBuy' => ($this->bIsFustBuy)
        );

        // Заказ в один клик?
        if ($this->bIsFustBuy){

            $this->sectionId = \Yii::$app->sections->getValue('cart');

            $oOrderCart = Api::getOrder(true);
            $oOrderCart->unsetAll();
            Api::setOrder($oOrderCart, true);

            $iObj = $this->getInt('idObj');

            if (!$iObj) return 0;
            $count = $this->getInt('count',1);
            if (!$count) return 0;
            Api::setItem($iObj, $count, true);
            $this->setData('fastBuy',1);
        }

        $bFast = $this->get('fast', $this->bIsFustBuy);
        $oOrderCart = Api::getOrder($bFast);

        // Не выводим форму, если заказ пустой
        if (!$oOrderCart->getCount())
            throw new NotFoundHttpException();

        $this->setTemplate('checkout.twig');
        $this->setData('order', $oOrderCart);

        $oForm = new OrderForm();
        $oForm->setFast($bFast);

        if ( ( $iUserId = CurrentUser::getId() ) && ( CurrentUser::getPolicyId() != Auth::getDefaultGroupId() ) )
            $oForm->setUserData( $iUserId );

        $aData = $this->getPost();
        if ( $oForm->load($aData) && $oForm->isValid() ) {

            $oService = new AdmOrder\Service();

            $id = $oService->saveOrderForm( $oForm );
            $sToken = '';

            if ($id) {
                $aOrder = AdmOrder\ar\Order::find()->where('id',$id)->asArray()->getOne();

                if (isset($aOrder['token']))
                    $sToken = $aOrder['token'];
            }

            $sCanapeuuid = isset($aData['_canapeuuid']) ? $aData['_canapeuuid'] : '';

            \Yii::$app->getResponse()->redirect(\Yii::$app->router->rewriteURL("[$this->sectionId][Cart?action=done&token=$sToken&canapeuuid=$sCanapeuuid]" ),'301')->send();
        }

        $this->setData( 'form', $oForm->getForm( 'OrderForm.twig', __DIR__, $aParams ) );

        $sTitle = \Yii::t('order', 'title_checkout' );
        site\Page::setTitle( $sTitle );
        site\Page::setAddPathItem( $sTitle );

        return psComplete;
    }

    /**
     * Завершение заказа
     * @return int
     */
    public function actionDone() {

        // rename title and pathline
        $sTitle =\Yii::t('order', 'title_checkout' );

        site\Page::setTitle( $sTitle );

        site\Page::setAddPathItem( $sTitle );

        $sToken = $this->get('token','');

        if ($sToken){

            $aOrder =  AdmOrder\ar\Order::find()->where('token', $sToken)->order('id','DESC')->asArray()->getOne();

            if ($aOrder){

                $aGoods = AdmOrder\ar\Goods::find()->where('id_order', $aOrder['id'])->asArray()->getAll();

                $total = 0;

                foreach ($aGoods as $aItem)
                    $total += $aItem['total'];

                if ($total>0){

                    if ($aOrder['status'] == AdmOrder\model\Status::getIdByNew()) {

                        /**
                         * @var $oPaymentType AdmOrder\ar\TypePaymentRow
                         */
                        $oPaymentType = AdmOrder\ar\TypePayment::find( $aOrder['type_payment'] );

                        if ($oPaymentType && $oPaymentType->payment){
                            /**
                             * @var Payments\Payment $oPayment
                             */
                            $oPayment = Payments\Api::make( $oPaymentType->payment );

                            if ($oPayment){
                                $oPayment->setOrderId($aOrder['id']);
                                $oPayment->setSum($total);

                                $this->setData( 'paymentForm', $oPayment->getForm() );
                            }
                        }

                    }
                }
            }
        }

        $this->setTemplate( 'done.twig' );
        $this->setData('mainSection', \Yii::$app->sections->main());

        return psComplete;
    }

    /**
     * Добавление новой позиции в заказ
     * @return void
     */
    public function cmdSetItem() {

        $objectId = $this->getInt('objectId');
        $count = $this->getInt('count');

        $bErrorCount = false;
        if ( $count and ($count > 0) )
            $bErrorCount = !Api::setItem($objectId, $count);

        Api::sendJSON(false,
            ($bErrorCount ? ['errorCount' => $bErrorCount] : [])
        );
    }

    /**
     * Удаление позиции заказа
     * @return void
     */
    public function cmdRemoveItem() {

        $id = $this->getInt('id');
        $oOrderCart = Api::getOrder();
        $oOrderCart->unsetItem($id);

        Api::setOrder($oOrderCart);
        Api::sendJSON();
    }

    /**
     * Пересчет количества позиции
     * @return void
     */
    public function cmdRecountItem() {

        $id = $this->getInt('id');
        $count = $this->getInt('count');

        if (!$count || $count<0) exit;

        $oOrderCart = Api::getOrder();

        if ($aItem = $oOrderCart->getItemById($id)) {

            $aItem['count'] = $count;
            $aItem['total'] = $count * Api::priceFormat($aItem['price']);
            $oOrderCart->setItem($aItem);
            Api::setOrder($oOrderCart);
        }

        Api::sendJSON();
    }

    /**
     * Удаление всех позиций
     * @return void
     */
    public function cmdUnsetAll() {

        $oOrderCart = Api::getOrder();
        $oOrderCart->unsetAll();

        Api::setOrder($oOrderCart);
        Api::sendJSON();
    }

} 