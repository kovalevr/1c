<?php

namespace skewer\build\Page\Articles;

/**
 * Класс, содержащий редактируемые для текущего модуля параметры, используемые в админском модуле "Настройка параметров"
 * (skewer\build\Adm\ParamSettings)
 * Class ParamSettings
 * @package skewer\build\Page\ParamSettings
 */

class ParamSettings extends \skewer\build\Adm\ParamSettings\Prototype {

    public static $aGroups = [
        'articles' => 'articles.groups_articles',
    ];

    public static $iGroupSortIndex = 20;

    /** @inheritdoc */
    public function getList() {

        return [
            [
                'name'   => 'titleOnMain',
                'group'  => 'articles',
                'title'  => 'articles.titleOnMain',
            ],
            [
                'name'   => 'onPage',
                'group'  => 'articles',
                'title'  => 'articles.on_column',
                'editor' => 'int',
                'settings' => [
                    'minValue' => 0,
                    'allowDecimals' => false
                ],
            ],
            [
                'name'  => 'section_all',
                'group' => 'articles',
                'title' => 'articles.section_all',
            ],
        ];
    }

    /** @inheritdoc */
    public function saveData() {
    }

    public function getInstallationParam(){
        return [];
    }

}