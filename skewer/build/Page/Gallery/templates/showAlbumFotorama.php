<?php

use skewer\build\Page\Gallery\AssetFotorama;
use skewer\components\design\Design;
use skewer\components\gallery\Album;
use skewer\components\gallery\Format;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var $_objectId
 * @var $description
 * @var $images
 * @var $openAlbum
 * @var $aAlbums
 */

AssetFotorama::register(\Yii::$app->view);

?>

<div class="b-galbox b-galbox-fotorama"  <? if (Design::modeIsActive()): ?>sktag="modules.gallery" sklabel="<?=$_objectId?>" <?endif;?> >
    <? if (!empty($description)): ?>
        <div class="galbox__contentbox"><?= Html::encode($description)?></div>
    <?endif;?>

    <? if (!empty($images)): ?>

        <?
        list($iMaxWidth, $iMaxHeight) = Album::getDimensions4Fotorama($aAlbums, 'med', $images);
        list($iThumbWidth, $iThumbHeight) = Album::getDimensions4Fotorama($aAlbums, 'mini', $images);

        if (!$iThumbWidth || !$iThumbHeight){

            $iAlbumId = array_shift($aAlbums);
            if ( ($iProfileId = Album::getProfileId($iAlbumId)) !== false ){

                if ($aFormat = Format::getByName('mini', $iProfileId)){
                    $iThumbWidth  = (int)ArrayHelper::getValue($aFormat,'0.width', 0);
                    $iThumbHeight = (int)ArrayHelper::getValue($aFormat,'0.height', 0);
                }
            }

        }

        ?>

        <div class="js-fotorama"  <? if ($iMaxWidth): ?>data-max-width="<?=$iMaxWidth?>px" <? endif; ?> <? if ($iMaxHeight): ?>data-height="<?=$iMaxHeight?>px" <? endif; ?> <? if ($iThumbHeight):?>data-thumbheight="<?=$iThumbHeight?>"<?endif;?> <? if ($iThumbWidth):?>data-thumbwidth="<?=$iThumbWidth?>"<?endif;?>  >
            <? foreach ($images as $aImage):?>

                <? if ( ($sBigPath = ArrayHelper::getValue($aImage->images_data, 'med.file', ''))  ): ?>
                    <a href="<?= $sBigPath ?>" alt="<?=Html::encode($aImage['alt_title'])?>" title="<? if (!empty($aImage['title'])):?><?=Html::encode($aImage['title'])?><?endif;?><? if (!empty($aImage['description'])):?><?=Html::encode($aImage['description'])?><?endif;?>" >
                        <?
                            $sMiniPath = ArrayHelper::getValue($aImage->images_data, 'mini.file', '');
                            $sPath = ($sMiniPath && file_exists(WEBPATH . $sMiniPath)) ? $sMiniPath : $sBigPath;
                        ?>
                        <img src="<?= $sPath ?>" />
                    </a>
                <?endif;?>

            <? endforeach;?>
        </div>

    <? else: ?>
        <p></p>
    <?endif;?>



    <div class="g-clear"></div>

    <? if (!$openAlbum):?>
        <p class="galbox__linkback"><a href="javascript: history.go(-1);" rel="nofollow"><?= \Yii::t('page', 'back') ?></a></p>
    <? endif; ?>
</div>
