<?php

namespace skewer\build\Page\Profile;

use skewer\components\auth\CurrentUserPrototype;
use skewer\helpers\Mailer;
use skewer\components\auth\models\Users;
use skewer\build\Adm\Order\ar;
use skewer\build\Adm\Order\model\ChangeStatus;
use skewer\build\Adm\Order\model\Status;
use skewer\components\catalog\GoodsSelector;
use skewer\components\i18n\ModulesParams;
use skewer\build\Page\Auth\Api;
use skewer\build\Tool\Payments as Payments;
use skewer\components\auth\Auth;
use skewer\components\auth\CurrentUser;
use skewer\base\site_module;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use skewer\build\Page\Cart;
use skewer\build\Adm\Order;

class Module extends site_module\page\ModulePrototype {

    public $prm = 0;

    private $aStatusList = array();
    private $newStatus = null;

    /**
     * Метод - исполнитель функционала
     */
    public function execute() {

        $this->setData('profile_url',  Api::getProfilePath());
        $this->setData('auth_url',  Api::getAuthPath());

        /**
         * отключаем счетчики в личном кабинете
         * если надо будет где еще, вынести в отдельный метод
         */
        $this->oContext->getParentProcess()->setData('counters','');
        $this->oContext->getParentProcess()->setData('countersCode','');

        $this->aStatusList = Status::getListTitle();

        $sCmd = $this->getStr('cmd', 'init');

        $this->setData('page', $this->sectionId());

        $sActionName = 'action' . ucfirst($sCmd);

        if (method_exists($this, $sActionName))
            $this->$sActionName();
        else
            throw new NotFoundHttpException();

        return psComplete;
    }

    /**
     * Список заказов у пользователя
     * @param int $iUserId id User
     * @param int $iOrderId id заказа, если нет взять все товары пользователя
     * @param string $sToken токен
     * @param int $iLimit Число позиций
     * @param int $iSkip Пропуск позиций
     * @param int $iTotalCount Ссылка на переменную общего числа заказов
     * @return array Массив заказаов клиента
     */
    private function getOrders($iUserId, $iOrderId = 0, $sToken = '', $iLimit = 1, $iSkip = 0, &$iTotalCount = null) {

        if ($sToken){
            $oQueryOrders = ar\Order::find()->where('token', $sToken);
        } else
        if ($iOrderId){
            $oQueryOrders = ar\Order::find()->where('auth', $iUserId)->where('id',$iOrderId)->order('id','DESC');
        } else
            $oQueryOrders = ar\Order::find()->where('auth', $iUserId)->order('id','DESC');

        $aOrders = $oQueryOrders->index('id')->limit($iLimit, $iSkip)->setCounterRef($iTotalCount)->asArray()->getAll();

        /** Информация о товарах заказов клиента */
        $aGoodsSatistic = (!$aOrders) ? [] : Order\Api::getGoodsStatistic(array_keys($aOrders));

        /** Закэшированные формы систем оплат текущего пользователя */
        $aPayments = [];

        foreach ($aOrders as $iOrderId => &$aOrderItem) {

            // Записать текстовый статус заказа
            $aOrderItem['text_status'] = $this->getStatusText($aOrderItem['status']);

            // Установить общую информацию о товарах заказа
            isset($aGoodsSatistic[$iOrderId]) and $aOrderItem['goods'] = $aGoodsSatistic[$iOrderId];

            // Установить кнопки оплаты для каждого заказа
            if ( isset($aGoodsSatistic[$iOrderId]) and ($aOrderItem['status'] == $this->getNewStatusId()) ) {

                /** @var $oPaymentType ar\TypePaymentRow */
                $oPaymentType = ar\TypePayment::find( $aOrderItem['type_payment'] );
                if ($oPaymentType && $oPaymentType->payment){

                    /** @var Payments\Payment $oPayment */
                    if (!isset($aPayments[$oPaymentType->payment]) || !$aPayments[$oPaymentType->payment])
                        $aPayments[$oPaymentType->payment] = Payments\Api::make($oPaymentType->payment);

                    $oPayment = $aPayments[$oPaymentType->payment];

                    if ($oPayment) {

                        $oPayment->setOrderId($aOrderItem['id']);
                        $oPayment->setSum($aGoodsSatistic[$iOrderId]['sum']);
                        if ($aOrderItem['status'] == Status::getIdByNew())
                            $aOrderItem['paymentForm'] = $oPayment->getForm();
                    }
                }
            }
        }

        return $aOrders;
    }

    //** Получить товары заказа */
    private function getGoods($iOrderId) {

        $aGoods = ar\Goods::find()->where('id_order', $iOrderId)->asArray()->getAll();

        // Добавить поля из каталожной информации о товаре
        foreach ($aGoods as &$aGood) {
            if ($aGood['id_goods'] and $aGoodData = GoodsSelector::get($aGood['id_goods'], 1))
                $aGood += [
                    'url'   => $aGoodData['url'],
                    'image' => ArrayHelper::getValue($aGoodData, 'fields.gallery.first_img.images_data.mini.file', ''),
                ];
        }

        return $aGoods;
    }

    /**
     * Форма инфы по юзеру
     */
    public function actionInfo() {

        if (CurrentUser::isLoggedIn()) {

            $oForm = new EditProfileForm( CurrentUser::getId() );

            if ( $oForm->load( $this->getPost() ) && $oForm->save() ) {
                $this->setData( 'msg', \Yii::t('auth', 'msg_save') );
                Auth::loadUser(CurrentUserPrototype::$sLayer,CurrentUser::getId());
            } else {
                Auth::loadUser(CurrentUserPrototype::$sLayer,CurrentUser::getId());
            }

            $this->setData( 'form', $oForm->getForm( 'EditProfileForm.twig', __DIR__ ) );
            $this->setData('cmd', 'info');
            $this->setTemplate('info.twig');

        } else
            $this->setTemplate('no_auth.twig');
    }

    /**
     * Форма смены пароля
     */
    public function actionSettings() {

        if ( CurrentUser::isLoggedIn() ) {

            $oNewPassForm = new NewPassForm();

            $oNewPassForm->oldpass = $this->getStr('oldpass');
            $oNewPassForm->pass = $this->getStr('pass');
            $oNewPassForm->wpass = $this->getStr('wpass');

            if ( $oNewPassForm->load( $this->getPost() ) && $oNewPassForm->isValid() ) {
                
                $oItem = Users::findOne( CurrentUser::getId() );
                $oItem->pass = Auth::buildPassword( $oItem->login, $oNewPassForm->pass );

                if ( $oItem->save() ) {

                    $this->setData( 'msg', \Yii::t('auth', 'msg_pass_save') );

                    Mailer::sendMail( $oItem->email, ModulesParams::getByName('auth', 'mail_title_new_pass'),
                        ModulesParams::getByName('auth', 'mail_new_pass')
                    );

                    $this->setTemplate('settings.twig');
                    return;

                } else
                    $this->setData('msg', \Yii::t('auth', 'msg_pass_no_save'));
            }

            $iSectionId = $this->sectionId();

            $this->setData('cmd', 'settings');
            $this->setData('url', \Yii::$app->router->rewriteURL( "[$iSectionId][Auth?cmd=settings]" ) );
            $this->setData( 'forms', $oNewPassForm->getForm('new_pass.twig', __DIR__) );
            $this->setTemplate('settings.twig');

        } else
            $this->setTemplate('no_auth.twig');
    }


    /**
     * Форма по дефолту заказа
     */
    public function actionInit() {
        // проверить авторизацию
        if (CurrentUser::isLoggedIn()) {
            $this->setData( 'current_user', CurrentUser::getUserData() );
            $this->setData( 'cmd', 'order' );

            $iPage       = $this->getInt('page', 1);
            $iOnPage     = Cart\Api::maxOrdersOnPage();
            $iTotalCount = 0;

            $this->setData( 'orders', $this->getOrders(CurrentUser::getId(), 0, '', $iOnPage, $iOnPage * ($iPage-1), $iTotalCount) );

            $this->getPageLine($iPage, $iTotalCount, $this->sectionId(), [], ['onPage' => $iOnPage]);

            $this->setTemplate('view.twig');
        } else {
            $this->setTemplate('no_auth.twig');
        }
    }

    public function actionDetail() {
        $iOrderId = $this->getInt('id');
        $sToken = $this->getStr('token','');

        $aOrders = $this->getOrders(CurrentUser::getId(),$iOrderId,$sToken);
        if (!$aOrders)
            throw new NotFoundHttpException();

        $aOrder = reset($aOrders);
        $aOrder['type_payment_text'] = ar\TypePayment::getValue($aOrder['type_payment']);
        $aOrder['type_delivery_text'] = ar\TypeDelivery::getValue($aOrder['type_delivery']);

        $aOrderGoods = $this->getGoods($aOrder['id']);
        if (!$aOrderGoods)
            throw new NotFoundHttpException();

        if ($sToken) $this->setData('token', $sToken);

        $aHistoryList = ChangeStatus::find()->where(['id_order' => $aOrder['id']])->asArray()->all();
        if ($aHistoryList){
            foreach($aHistoryList as $k=>$item){
                $aHistoryList[$k]['title_old_status'] = $this->getStatusText($item['id_old_status']);
                $aHistoryList[$k]['title_new_status'] = $this->getStatusText($item['id_new_status']);
            }
            $this->setData('historyList',$aHistoryList);
        }

        $this->setData('order', $aOrder);
        $this->setData('items', $aOrderGoods);

        $this->setTemplate('detail.twig');
    }

    /**
     * Отдает название статуса по id
     * @param $mId
     * @return string
     */
    private function getStatusText( $mId ) {
        if ( isset($this->aStatusList[$mId]) )
            return $this->aStatusList[$mId];
        else
            return '---';
    }

    /**
     * Ид статуса нового заказа
     * @return int
     */
    private function getNewStatusId() {
        if (is_null($this->newStatus))
            $this->newStatus = Status::getIdByNew();

        return $this->newStatus;
    }
}