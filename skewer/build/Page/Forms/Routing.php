<?php

namespace skewer\build\Page\Forms;

use skewer\base\router\ExclusionTailsInterface;
use skewer\base\router\RoutingInterface;


/**
 * Class Routing
 * @package skewer\build\Page\Forms
 */
class Routing implements RoutingInterface, ExclusionTailsInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/*response/',
        );
    }

    /**
     * Проверяем используется ли в данный момент какой то из паттернов
     */
    public static function patternUsed(){

        // todo Правильнее будет применять методы Router::getRulesByClassName && Router::decodeRules
        foreach (self::getRoutePatterns() as $sInRule) {
            $sInRule = str_replace('*','',$sInRule);
            $sInRule = str_replace('/','',$sInRule);
            if (strpos($_SERVER['REQUEST_URI'],$sInRule)!==false){
                return true;
            }
        }
        return false;

    }

    /** Возвращает правила исключений, допустимых остатков урл, на главной странице  */
    public static function getRulesExclusionTails4MainPage(){
        return array(
            '/!response/'
        );
    }


}
