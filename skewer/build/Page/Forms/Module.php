<?php

namespace skewer\build\Page\Forms;

use skewer\base\section\Parameters;
use skewer\build\Tool\ReserveOnline\Api;
use skewer\components\forms;
use skewer\base\section\Tree;
use skewer\base\site;
use skewer\base\site_module;
use skewer\components\import\Logger;
use skewer\components\targets\Yandex;
use skewer\helpers\Captcha;
use skewer\components\targets\models\Targets;
use yii\web\NotFoundHttpException;

/**
 * Модуль вывода формы
 * Class Module
 * @package skewer\build\Page\Forms
 */
class Module extends site_module\page\ModulePrototype implements site_module\Ajax {

    /** Шаблон формы по умолчанию */
    const DEF_FORM_TPL = 'form.twig';

    public  $FormTemplate    = '';             // Шаблон отображения формы раздела
    public  $AnswersTemplate = 'answers.twig'; // Шаблон отображения ответов
    private $LetterTemplate  = 'letter.twig';  // Шаблон письма
    private $ErrorTemplate   = 'error.twig';   // Шаблон отображения ошибок
    private $ReachGoalTemplate = 'reachgoals.twig';   // Шаблон reachgoals

    public $FormId    = 0;
    public $AjaxForm  = 0;
    public $tplDir;

    /** @var string Получение формы по имени приоритетнее чем по id. Используется только в ajax-запросе формы */
    private $FormName  = '';

    public function init() {

        if ($this->tplDir) $this->oContext->setModuleDir($this->tplDir);

        $this->setData( 'moduleWebPath', $this->getModuleWebDir() );
    }

    public function execute() {

        $sCmd       = $this->getStr( 'cmd' );
        $iObjectId  = $this->getInt( 'objectId' );
        $iFormId    = $this->getInt( 'form_id' );

        /* ajax обработка */
        $ajaxForm = $this->getInt('ajaxForm', $this->AjaxForm); // Отправлять форму через ajax

        // Получить имя формы из post-параметров для запроса формы через ajax
        $this->FormName = \Yii::$app->request->post('formName', '');

        // Получение раздела с формой по id или alias из post-параметров для запроса формы через ajax
        if ($section = \Yii::$app->request->post('section'))
            $section = ($aSection = Tree::getSection($section, true)) ? $aSection['id'] : $this->sectionId();
        else
            $section = $this->sectionId();

        // Учитываем метку формы, если будет несколько форм из одного раздела на странице
        $sLabel = $ajaxForm ? 'out' : $this->oContext->getLabel();

        //только если приход не со всплывающей формы, адекватно работает только с одной формой на странице
//        if ($this->get('label')!=='out')
//             Обработаем отправку формы только в соответствующей ей метке
//            if ( ($sCmd == 'send') and ($sLabel != $this->get('label')) and ($this->get('label')!=='forms') )
//                $sCmd = 'show';


        /*Проверка на состояние "после отправки показать результирующую"*/
        if ((!is_null(\Yii::$app->session->get('forms_answerTemplate')))
            and (!is_null(\Yii::$app->session->get('forms_data')))
            and (!is_null(\Yii::$app->session->get('forms_formId')))
	        and (!is_null(\Yii::$app->session->get('forms_label')))
	        and ( /** Проверяем в той ли мы метке, иначе просто отобразим форму */
	        	(\Yii::$app->session->get('forms_label') == $this->oContext->getLabel())
	            OR /** для аякс форм с отдельной результирующей */
		        !$ajaxForm and \Yii::$app->session->get('forms_label') == 'out'
	        )
        ){
            $sCmd = 'redirect';
        }

        switch ( $sCmd ) {

            case 'redirect':
                /*Обработчик после стандартной отправки формы*/
                $aFormData = \Yii::$app->session->get('forms_data');

                // Читаем flash сообщение; в след.запросе оно будет удалено
                \Yii::$app->session->getFlash('legal_redirect');

                if (isset($aFormData['success'])) {
                    $sData = 'success';
                    $bRes = 1;
                }else {
                    $bRes = 0;
                    $sData = 'error';
                }

                $iFormId = \Yii::$app->session->get('forms_formId');

                $oForm = forms\Table::build( $iFormId );

                // Альтернативный текст ответа пользователю
                if ( $bRes and $sFormAnswer = trim($oForm->getFormSuccAnswer()) ) {

                    // Замена имён полей в шаблоне на заполенные данные пользователя
                    $aReplaceData = [];
                    foreach($oForm->getFields() as $oFormField)
                        $aReplaceData[ '[' . $oFormField->param_name . ']' ] = $oFormField->param_default;
                    $this->setData( 'SuccAnswer', strtr($sFormAnswer, $aReplaceData) );
                }

                \Yii::$app->session->set('forms_formId',null);
                \Yii::$app->session->set('forms_answerTemplate',null);
                \Yii::$app->session->set('forms_data',null);
	            \Yii::$app->session->set('forms_label', null);

                $sSuccAnswer = \Yii::$app->session->get('forms_SuccAnswer');

                if ($sSuccAnswer){
                    \Yii::$app->session->set('forms_SuccAnswer',null);
                    $this->setData( 'SuccAnswer', $sSuccAnswer );
                }

                if (isset($aFormData['yandexReachGoal'])) {
                    $this->setData( 'yandexReachGoal', $aFormData['yandexReachGoal'] );
                }

                if (isset($aFormData['googleReachGoal'])) {
                    $this->setData( 'googleReachGoal', $aFormData['googleReachGoal'] );
                }

                $this->setData( $sData, 1 );
                $this->setData( 'form_section', $section );
                $this->setTemplate( $this->AnswersTemplate );
                break;
            case 'send':

                try {

                    if ( !$iFormId )
                        throw new \Exception( 'form_not_found_error' );

                    $oForm = forms\Table::build( $iFormId, $_POST );

                    $formHash = $oForm->getHash( (int)\Yii::$app->request->post('section', $section), $this->get('label') );

                    if ( !$oForm->validate( $formHash ) )
                        throw new \Exception( $oForm->getError() );

                    switch ( $oForm->getHandlerType() ) {

                        case 'toMail':

                            $bRes = $oForm->send2Mail( $this->LetterTemplate, $this->getModuleDir() . $this->getTplDirectory() );

                            break;

                        case 'toMethod':
                            $bRes = $oForm->send2Method();
                            break;

                        case 'toBase':

                            $oForm->send2Base( $section ?: $this->sectionId() );
                            $bRes = $oForm->send2Mail( $this->LetterTemplate, $this->getModuleDir() . $this->getTplDirectory() );
                            break;

                        default:
                            throw new \Exception('handler_not_found');# обработчик неизвестен - выходим
                            break;
                    }

                    // добавление обработчика ReachGoal
                    if ( $bRes ) {
                        $sTarget = $oForm->getTarget();
                        if ( $sTarget and Yandex::isActive() ) {
                            $this->setData('yandexReachGoal', array(
                                'cnt' => Yandex::getCounter(),
                                'target' => $sTarget
                            ));
                        }

                        $gTarget = $oForm->getTargetGoogle();

                        /** @var Targets $oTarget */
                        $aTargetAttrs = ($oTarget = Targets::findOne(['name'=>$gTarget])) ? $oTarget->getAttributes() : [];
                        if ( $gTarget  ) {
                            $this->setData('googleReachGoal', array(
                                'target' => $gTarget,
                                'category' => $aTargetAttrs['category']
                            ));
                        }
                    }

                    // вывести сообщение об успешной / неуспешной отправке
                    if ( !$ajaxForm && $oForm->getFormRedirect() ) {
                        $sRedirectLink = $oForm->getFormRedirect();
                        /*Если указана ссылка как [id раздела]*/
                        if ( preg_match('/^\[\d+\]$/', $sRedirectLink) )
                            $sRedirectLink = \Yii::$app->router->rewriteURL( $sRedirectLink);
                        \Yii::$app->session->set('forms_data',$this->getData());
                        \Yii::$app->getResponse()->redirect($sRedirectLink,'301');
                        return psComplete;
                    }

                    $sData = $bRes ? 'success' : 'error';
                    if ( $bRes && $ajaxForm )
                        $sData = 'successAjax';

                    // Альтернативный текст ответа пользователю
                    if ( $bRes and $sFormAnswer = trim($oForm->getFormSuccAnswer()) ) {

                        // Замена имён полей в шаблоне на заполенные данные пользователя
                        $aReplaceData = [];
                        foreach($oForm->getFields() as $oFormField){
                            $aReplaceData[ '[' . $oFormField->param_name . ']' ] = $oFormField->param_default;
                        }

                        \Yii::$app->session->set('forms_SuccAnswer',strtr($sFormAnswer, $aReplaceData));

                    }

                    $this->setData( $sData, 1 );
                    $this->setData( 'form_section', $section );

                } catch ( \Exception $e ) {

                    $this->setData( $e->getMessage(), 1 );
                    $this->setTemplate( $this->ErrorTemplate );
                    return psComplete;
                }

                if (!$ajaxForm){
                    /*Если форма отрабатывает стандартно, подготовим результат к редиректу*/
                    \Yii::$app->session->set('forms_formId',$iFormId);
                    \Yii::$app->session->set('forms_answerTemplate',$this->AnswersTemplate);
                    \Yii::$app->session->set('forms_data',$this->getData());
	                \Yii::$app->session->set('forms_label', $this->get('label'));
                    \Yii::$app->session->setFlash( 'legal_redirect', 1 );

                    $sNewUrl = str_replace('response/', '', \Yii::$app->request->pathInfo);
                    \Yii::$app->response->redirect( site\Site::httpDomainSlash() . $sNewUrl . 'response' )->send();
                    exit;
                }

                $this->setTemplate( $this->AnswersTemplate );
                break;

            case 'captcha_ajax':

                if ( Captcha::check( $this->getStr( 'code' ), $this->getStr( 'hash' ), false ) ) {

                    $this->setData( 'out', 1 );

                } else {

                    $sMessage = \Yii::t('forms', 'wrong_captcha' );
                    $this->setData( 'out', $sMessage );
                }

                return psRendered;
                break;

            case 'show':
            default:
                /*Перехват целей с отдельной результирующей*/
                $aFormData = \Yii::$app->session->get('forms_data');
                \Yii::$app->session->set('forms_data',null);

                if (isset($aFormData['yandexReachGoal'])) {
                    $this->setData( 'yandexReachGoal', $aFormData['yandexReachGoal'] );
                }

                if (isset($aFormData['googleReachGoal'])) {
                    $this->setData( 'googleReachGoal', $aFormData['googleReachGoal'] );
                }

                // Обращение к /response без отправки формы
                if ( !\Yii::$app->session->getFlash('legal_redirect') && Routing::patternUsed() ) {
                    $sNewUrl = str_replace('response/', '', \Yii::$app->request->pathInfo);
                    \Yii::$app->response->redirect( site\Site::httpDomainSlash() . $sNewUrl )->send();
                    exit;
                }

                $sDetailUrl = '';
                $oContent = site\Page::getMainModuleProcess();
                if ( $oContent ) {
                    if( !$oContent->isComplete() )
                        return psWait;
                    $sDetailUrl = $oContent->getUsedURL();
                }

                if ( $this->FormName )
                    $oFormRow = forms\Table::getByName( $this->FormName );
                elseif ( $this->FormId )
                    $oFormRow = forms\Table::getById($this->FormId);
                elseif ( \Yii::$app->request->post('section') ) { // Поиск по секции только для ajax форм
	                $aSectionForms = forms\Table::get4Section( $section );
	                if ($aSectionForms)
		                $oFormRow = array_shift($aSectionForms);
	                 else
		                $oFormRow = null;
                } else
					$oFormRow = null;

                if ( !( $oFormRow instanceof forms\Row ) ) {

                    $this->setTemplate( $this->ReachGoalTemplate );

                    return psComplete;

                } else {

                    /** @var forms\Entity $oForm */
                    $oForm = forms\Table::build( $oFormRow->form_id );

                    \Yii::$app->router->setLastModifiedDate($oForm->getFormParam('last_modified_date'));

                    if ( $iObjectId )
                        $oForm->fillGoodsFields( $iObjectId );

                    //форма бронирования гостинниц
                    if ($section == Api::getReserveOnlineSectionByLang(\Yii::$app->language)) {
                        if(isset($oForm->getFields()['data-zaezda'])&&(isset($oForm->getFields()['data-otezda']))) {
                            $oForm->getFields()['data-zaezda']->param_default = (isset($_GET['from']))?$_GET['from']:'';
                            $oForm->getFields()['data-otezda']->param_default = (isset($_GET['to']))?$_GET['to']:'';
                        }
                    }
                    $this->setData('HiddenCaptchaInput',forms\Entity::getHiddenCaptchaInput());
                    $this->setData( 'oForm', $oForm );
                    $this->setData( 'iRandVal', rand(0, 1000) );
                    $this->setData( 'section', $section );
                    $this->setData( 'form_section_path', '' );
                    $this->setData( 'form_section_path', Tree::getSectionAliasPath($section, true) . $sDetailUrl );

                    // Установка шаблона формы:
                    // сперва устанавливаем шаблон в разделе, если его нет, то ищем шаблон в настройках формы, иначе -- шаблон по умолчанию
                    $this->setTemplate( $this->FormTemplate ?: ($oFormRow->form_template ?: self::DEF_FORM_TPL) );

                    $this->setData( 'ajaxForm', $ajaxForm );
                    $this->setData( 'label', $sLabel );
                    $this->setData( 'formHash', $oForm->getHash( $section, $sLabel ) );
                }

                break;
        }


        return psComplete;
    }
}