<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 30.12.2016
 * Time: 15:40
 */

namespace skewer\build\Catalog\ViewSettings\view;

use skewer\components\ext\view\FormView;

class Index extends FormView
{
    public $aTplList;
    public $bGoodsRelated;
    public $bGoodsInclude;
    public $aCheckList;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->headText( \Yii::t( 'catalog', 'head_title' ) )
            ->fieldSelect( 'listTemplate', \Yii::t( 'catalog', 'listTpl'), $this->aTplList )
        ;
        if ($this->bGoodsRelated) {
            $this->_form->fieldSelect( 'relatedTpl', \Yii::t( 'catalog', 'relatedTpl'), $this->aTplList );
        }
        if ($this->bGoodsInclude) {
            $this->_form->fieldSelect( 'includedTpl', \Yii::t( 'catalog', 'includedTpl'), $this->aTplList );
        }
        $this->_form
            ->fieldInt( 'onPage', \Yii::t('Catalog', 'listCnt'), ['subtext' => \Yii::t('catalog', 'onPage_label'), 'minValue' => 0] )
            ->fieldSelect( 'showFilter', \Yii::t('Catalog', 'showListFilters'), $this->aCheckList, [], false )
            ->fieldSelect( 'showSort', \Yii::t('Catalog', 'showListSort'), $this->aCheckList, [], false )
            ->setValue([
                'listTemplate' => '',
                'relatedTpl' => '',
                'includedTpl' => '',
                'onPage' => '',
                'showFilter' => '0',
                'showSort' => '0',
            ])
            ->buttonConfirm('save', \Yii::t('adm', 'save'), \Yii::t('catalog', 'save_msg'), 'icon-save')
        ;
    }
}