<?php

namespace skewer\build\Catalog\ViewSettings;


use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\base\section\Parameters;
use skewer\base\SysVar;


class Module extends ModulePrototype {


    public function actionInit() {
        $this->render(new view\Index([
            'aTplList' => self::getTplList(),
            'bGoodsRelated' => SysVar::get('catalog.goods_related'),
            'bGoodsInclude' => SysVar::get('catalog.goods_include'),
            'aCheckList' => self::getCheckList()
        ]));
    }


    public function actionSave() {

        $keys = ['listTemplate','relatedTpl','includedTpl','onPage','showFilter','showSort'];
        $data = $this->getInData();

        foreach ( $keys as $key ) {
            $val = isSet( $data[$key] ) ? $data[$key] : '';
            if ( $val ) {
                if ( $val == -1 ) $val = 0;
                $this->saveParam( $key, $val );
            }
        }

        $this->addMessage( '', \Yii::t('catalog', 'good_save_msg' ) );
        \Yii::$app->router->updateModificationDateSite();
        
        $this->actionInit();
    }


    protected function saveParam( $key, $val ) {
        /* @todo обновление может затронуть некаталожные разделы! */
        return Parameters::updateByName( 'content', $key, $val );
    }


    protected function getTplList() {

        $aList = array( 'list', 'gallery', 'table' );

        $aOut = array();
        foreach ($aList as $sName)
            $aOut[$sName] = \Yii::t('catalog', 'tpl_'.$sName);

        return $aOut;
    }


    protected function getCheckList() {

        $aList = array( 'list', 'gallery', 'table' );

        $aOut = array();
        foreach ($aList as $sName)
            $aOut[$sName] = \Yii::t('catalog', 'tpl_'.$sName);

        return [
            0 => \Yii::t('catalog', 'dontchange'),
            1 => \Yii::t('catalog', 'yes'),
            -1 => \Yii::t('catalog', 'no'),
        ];
    }

}