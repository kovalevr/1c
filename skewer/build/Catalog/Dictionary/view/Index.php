<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 30.12.2016
 * Time: 16:17
 */
namespace skewer\build\Catalog\Dictionary\view;

use skewer\components\ext\view\ListView;
use skewer\components\catalog\model\EntityRow;

class Index extends ListView
{
    /** @var EntityRow[] */
    public $aDictionaries;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_list
            ->fieldShow( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('dict', 'dict_name'), ['listColumns.flex' => 1] )
            ->setValue( $this->aDictionaries )
            ->buttonAddNew('New', \Yii::t('dict', 'create_dict'))
            ->buttonRowUpdate( 'View' )
            ->setEditableFields(['title'], 'ChangeDictName')
        ;
    }
}