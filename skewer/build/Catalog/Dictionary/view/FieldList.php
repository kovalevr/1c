<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 09.01.2017
 * Time: 14:32
 */

namespace skewer\build\Catalog\Dictionary\view;

use skewer\components\ext\view\ListView;

class FieldList extends ListView
{
    public $sHeadText;
    public $aFields;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_list
            ->headText( sprintf('<h1>%s</h1>', $this->sHeadText) )
            ->fieldShow( 'id', 'id' )
            ->fieldString( 'name', \Yii::t( 'card', 'field_f_name') )
            ->fieldString( 'title', \Yii::t( 'card', 'field_f_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'editor', \Yii::t( 'card', 'field_f_editor') )
            ->widget( 'editor', 'skewer\\build\\Catalog\\CardEditor\\Api', 'applyEditorWidget' )
            ->setValue($this->aFields)
            ->buttonAddNew('FieldEdit', \Yii::t('card', 'btn_add_field'))
            ->buttonCancel('View', \Yii::t('card', 'btn_back'))
            ->buttonRowUpdate( 'FieldEdit' )
            ->buttonRowDelete( 'FieldRemove' )
        ;
    }
}