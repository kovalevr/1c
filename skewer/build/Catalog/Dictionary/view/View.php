<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 09.01.2017
 * Time: 14:13
 */

namespace skewer\build\Catalog\Dictionary\view;

use skewer\components\ext\view\ListView;

class View extends ListView
{
    public $aItems;

    function build() {
        $this->_list
            ->fieldShow( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('dict', 'title'), ['listColumns.flex' => 1] )
            ->setValue( $this->aItems )

            ->buttonAddNew('ItemEdit', \Yii::t('dict', 'add'))
            ->buttonEdit('FieldList', \Yii::t('dict', 'struct'))
            ->buttonCancel('List', \Yii::t('dict', 'back'))

            ->buttonSeparator( '->' )

            ->buttonDelete('Remove', \Yii::t('dict', 'del'))
            ->buttonRowUpdate( 'ItemEdit' )
            ->buttonRowDelete( 'ItemRemove' )

            ->setEditableFields(['title'], 'ItemSave')
        ;
        $this->_list->enableDragAndDrop('sort');
    }
}