<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 09.01.2017
 * Time: 16:05
 */

namespace skewer\build\Catalog\Dictionary\view;

use skewer\components\ext\view\FormView;

class ItemEdit extends FormView
{
    public $aNotSortFields;
    public $mItem;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form->fieldHide( 'id', 'id' );
        foreach ( $this->aNotSortFields as $oField ) {
            $this->_form->fieldByEntity($oField);
        }
        $this->_form
            ->setValue( $this->mItem ? : [] )
            ->buttonSave('ItemSave')
            ->buttonCancel('View')
        ;
    }
}