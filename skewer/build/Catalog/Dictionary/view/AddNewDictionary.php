<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 09.01.2017
 * Time: 16:33
 */

namespace skewer\build\Catalog\Dictionary\view;

use skewer\components\ext\view\FormView;

class AddNewDictionary extends FormView
{
    public $oCard;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->headText( '<h1>' . \Yii::t('dict', 'new_dict') . '</h1>' )
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('dict', 'dict_name'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('dict', 'system_name') )
            ->setValue( $this->oCard )
            ->buttonSave('Add')
            ->buttonCancel('List')
        ;
    }
}