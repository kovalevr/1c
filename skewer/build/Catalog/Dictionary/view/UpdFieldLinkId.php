<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 09.01.2017
 * Time: 15:27
 */

namespace skewer\build\Catalog\Dictionary\view;

use skewer\components\ext\view\FormView;

class UpdFieldLinkId extends FormView
{
    public $aProfiles;
    public $bIsNotLinked;
    public $iTypeId;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->fieldSelect('link_id',  \Yii::t( 'gallery', 'profiles_select'), $this->aProfiles, ['hidden' => $this->bIsNotLinked], false)
            ->setValue([
                'link_id' => $this->iTypeId
            ]);
    }
}