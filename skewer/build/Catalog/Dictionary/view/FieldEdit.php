<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 09.01.2017
 * Time: 14:55
 */

namespace skewer\build\Catalog\Dictionary\view;

use skewer\components\catalog\model\FieldRow;
use skewer\components\ext\view\FormView;

class FieldEdit extends FormView
{
    public $sCardTitle;
    public $iFieldId;
    public $aSimpleTypeList;
    public $aProfiles;

    /** @var  FieldRow */
    public $oItem;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->headText( '<h1>' . \Yii::t( 'card', 'head_card_name', $this->sCardTitle ) . '</h1>' )
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t( 'card', 'field_f_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t( 'card', 'field_f_name'), ['disabled' => (bool)$this->iFieldId] )
            ->fieldSelect( 'editor', \Yii::t( 'card', 'field_f_editor'), $this->aSimpleTypeList, ['onUpdateAction'=>'UpdFieldLinkId'], false )
            ->fieldSelect( 'link_id', \Yii::t('card', 'field_f_link_id'), $this->aProfiles, ['hidden' => !$this->oItem->isLinked()], false )
            ->fieldString( 'def_value', \Yii::t( 'card', 'field_f_def_value') )
            ->setValue($this->oItem)
            ->buttonSave('FieldSave')
            ->buttonCancel('FieldList')
        ;
    }
}