<?php

namespace skewer\build\Catalog\Goods\model;

use skewer\build\Catalog\Goods\Search;
use skewer\components\catalog\GoodsRow;
use skewer\base\ft;
use skewer\components\gallery;
use skewer\helpers\ImageResize;
use skewer\build\Adm\GuestBook\ar\GuestBook;
use yii\base\UserException;

/**
 * Модель для работы с товаром
 * Class GoodsEditor
 * @package skewer\build\Catalog\Goods\model
 */
class GoodsEditor extends FormPrototype {

    const categoryField = '__category';
    const mainLinkField = '__main_link';

    private $bExistRow = false;

    private $defSection = 0;

    private $defCard = '';

    private $defId = 0;


    public function setSectionData( $iSectionId, $sSectionCard, $iDefGoods = 0 ) {
        $this->defSection = $iSectionId;
        $this->defCard = $sSectionCard;
        $this->defId = $iDefGoods;
        return $this;
    }


    public function isExistRow() {
        return $this->bExistRow;
    }


    public static function get() {
        $obj = new static();
        return $obj;
    }



    /**
     * Обработка входных данных
     * @param bool $bGoNext
     * @return bool
     * @throws \Exception
     */
    public function load( $bGoNext = false ) {

        if ( !$this->defId )
            $this->defId = $this->getDataField( 'id' );

        if ( $this->defId ) {

            $this->oGoodsRow = GoodsRow::get( $this->defId );
        } else {

            if ($sCard = $this->getDataField('__card'))
                $this->defCard = $sCard;

            // Проверить существование карточки и выдать более понятное предупреждение
            try {
                ft\Cache::get($this->defCard);
            } catch (\Exception $e) {
                throw new UserException(\Yii::t('catalog', 'error_card_not_found', $this->defCard));
            }

            $this->oGoodsRow = GoodsRow::create( $this->defCard );
        }


        if ( $this->type == 'form' ) {

            $aData = $this->getData();

            // обработка полей по типу
            $aFields = $this->oGoodsRow->getFields();
            foreach( $aFields as $oField ) {

                if ( isSet( $aData[$oField->getName()] ) ) {

                    if ($oField->getEditorName() == ft\Editor::WYSWYG)
                        $aData[$oField->getName()] = ImageResize::wrapTags($aData[$oField->getName()]);

                    if ($oField->getEditorName() == ft\Editor::MAP_SINGLE_MARKER){
                        $aMatches = [];
                        preg_match('/[^;]*;\s*\[id=([0-9]*)\]/i', $aData[$oField->getName()], $aMatches);

                        if ( isset($aMatches[1]) )
                            $aData[$oField->getName()] = $aMatches[1];
                    }

                }

            }

            $this->oGoodsRow->setData( $aData );

        } elseif( $this->type == 'field' ) {

            if ( !$this->updField )
                return false;

            $aData[ $this->updField ] = $this->getDataField( $this->updField );
            $this->oGoodsRow->setData( $aData );

        } else {

            // todo возможно нужно(поля расширенной карточки) -> $this->data = $this->oGoodsRow->getVars();
            return $bGoNext;
        }

        return true;
    }


    /**
     * Сохранение товара
     * @return bool
     */
    public function save() {

        if ( !$this->load() )
            return false;

        return $this->saveGoods();
    }


    private function saveGoods() {

        if ( !$this->oGoodsRow->save() ) {
            return false; // todo error view: $this->addError( $this->makeErrorText( $oGoodsRow ) );
        }

        if ( $sSectionList = $this->getDataField( self::categoryField ) ) {
            $aSectionList = $sSectionList ? explode( ',', $sSectionList ) : [];
            $this->oGoodsRow->setViewSection( $aSectionList );
        } else {

            if ( !($aSectionList = $this->oGoodsRow->getViewSection()) ) {
                $aSectionList = [ $this->defSection ];
                $this->oGoodsRow->setViewSection( $aSectionList );
            }

        }

        if ( $iMainSection = $this->getDataField( self::mainLinkField ) ) {
            $this->oGoodsRow->setMainSection( $iMainSection );
        }

        $this->oGoodsRow->getMainSection();

        // обновление поискового индекса
        $oSearch = new Search();
        $oSearch->updateByObjectId( $this->oGoodsRow->getRowId() );

         return true;
    }


    /**
     * Сохранить товар как новый
     * @return bool
     */
    public function saveNew() {

        if ( !$this->load( true ) )
            return false;

        $aData = $this->oGoodsRow->getData();
        $aData['__card'] = $this->oGoodsRow->getExtCardName();

        unSet( $aData['id'] );
        unSet( $aData['alias'] );
        unSet( $aData[self::categoryField] );
        unSet( $aData[self::mainLinkField] );

        if ( isset( $aData['title'] ) )
            $aData['title'] .= sprintf( ' (%s)', \Yii::t('catalog', 'clone') );

        // дублирование всех галерей карточки
        $aFields = $this->oGoodsRow->getFields();
        foreach( $aFields as $oField ) {

            if ( $oField->getEditorName() == ft\Editor::GALLERY )
                $aData[$oField->getName()] = gallery\Album::copyAlbum($aData[$oField->getName()]);
        }

        //$aData[self::categoryField] = implode( ',', $this->oGoodsRow->getViewSection() );
        if (!$this->defSection)
            return false;

        // связываем с тем разделом, в котором находимся
        $aData[self::categoryField] = $this->defSection;

        $this->data = $aData;

        $this->oGoodsRow = GoodsRow::create( $aData['__card'] );
        $this->oGoodsRow->setData( $aData );

        return $this->saveGoods();
    }


    /**
     * Удаление товара
     * @return bool
     */
    public function delete() {

        if ( !$this->load( true ) )
            return false;

        return $this->deleteGoods();
    }


    private function deleteGoods() {

        if ( !($id = $this->oGoodsRow->getRowId()) )
            return false;

        $oSearch = new Search();
        $oSearch->deleteByObjectId( $id );

        // Удалить отзывы к товару
        GuestBook::removeGood($id);
        \Yii::$app->router->updateModificationDateSite();

        if ( !($iId = $this->oGoodsRow->delete()) ) {
            return false;
        }

        return true;
    }


    /**
     * Удаление набора товаров
     * @return bool
     * @throws \Exception
     */
    public function multipleDelete() {

        $aItems = array();
        if ( $this->getDataField('multiple') ) {

            if ( $items = $this->getDataField('items') )
                foreach ( $items as $aItem )
                    $aItems[] = isset( $aItem['id'] ) ? $aItem['id'] : 0;

        } else {
            if ( $id = $this->getDataField('id') )
                $aItems[] = $id;
        }

        if ( count( $aItems ) )
            foreach ( $aItems as $id ) {
                $this->oGoodsRow = GoodsRow::get( $id );
                if ( !$this->deleteGoods() )
                    return false;
            }

        return true;
    }

}