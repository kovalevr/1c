<?php
/**
 * User: kolesnikov
 * Date: 31.07.13
 */

use skewer\base\site\Layer;
use skewer\base\section\models\TreeSection;
use skewer\components\catalog\GoodsRow;
use skewer\components\search;

$aConfig['name']     = 'Goods';
$aConfig['title']    = 'Товары';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Товары';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::CATALOG;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory'] = 'catalog';

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => GoodsRow::className(),
    'method' => 'removeSection',
];

$aConfig['events'][] = [
    'event' => search\Api::EVENT_GET_ENGINE,
    'class' => GoodsRow::className(),
    'method' => 'getSearchEngine',
];

$aConfig['events'][] = [
    'event' => search\Api::EVENT_CMS_SEARCH,
    'class' => \skewer\components\catalog\Api::className(),
    'method' => 'search',
];

return $aConfig;
