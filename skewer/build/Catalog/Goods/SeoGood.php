<?php
namespace skewer\build\Catalog\Goods;

use skewer\base\section\Tree;
use skewer\base\site\Site;
use skewer\components\catalog;
use skewer\components\seo\Frequency;
use skewer\components\seo\SeoPrototype;
use skewer\components\seo\Template;
use yii\helpers\ArrayHelper;

class SeoGood extends SeoPrototype{

    public static function getGroup(){
        return 'good';
    }

    /**
     * @inheritdoc
     */
    public static function getAlias(){
        return 'catalogDetail';
    }

    /**
     * @return array
     */
    public function extractReplaceLabels(){

        $aOut =  [];

        $aDataEntity = $this->getDataEntity();

        $sTitle = ArrayHelper::getValue($aDataEntity, 'fields.title.value', '');

        $aOut['label_catalog_title_upper'] =  $sTitle;
        $aOut['label_catalog_title_lower'] = $this->toLower($sTitle);

        // добавление всех полей
        if ( !empty($aDataEntity['fields']) )
            foreach ( $aDataEntity['fields'] as $sFieldName => $aField ){

                if ($aField['type'] == 'money'){

                    if ($aField['value']){
                        $aField['value'] = round($aField['value'], 2, PHP_ROUND_HALF_UP);
                        $aOut[ $aField['title'] ] = $aField['value'];
                        if (isset($aField['measure']))
                            $aOut[ $aField['title'] ] .= ' ' . $aField['measure'];
                    } else
                        $aOut[ $aField['title'] ] = '';

                }elseif ( isSet( $aField['title'] ) && isSet( $aField['html'] ))
                    $aOut[ $aField['title'] ] = strip_tags($aField['html']);
            }

        return $aOut;

    }

    public function getPriority(){
        return 0.9;
    }

    public function calculateFrequency(){
        return Frequency::DAILY;
    }

    public function loadDataEntity(){

        if ($this->iEntityId)
            $GoodData = catalog\GoodsSelector::get($this->iEntityId);

        if ($GoodData){
            $this->aDataEntity = $GoodData ? $GoodData : [];
            if (isset($this->aDataEntity['card']))
                self::setExtraAlias($this->aDataEntity['card']);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function getSearchClassName(){
        return Search::className();
    }


    /**
     * @inheritdoc
     */
    public function getRecordWithinEntityByPosition($iPosition){

        $aResult = catalog\model\SectionTable::find()
            ->fields(['goods_id'])
            ->where('section_id', $this->iSectionId)
            ->order('priority')
            ->limit(1,$iPosition)
            ->asArray()->get();

        if (!isset($aResult[0]))
            return false;

        $aCurrentRecord = $aResult[0];

        $aGood = catalog\GoodsSelector::get($aCurrentRecord['goods_id'], catalog\Card::DEF_BASE_CARD );

        $this->setDataEntity($aGood);
        $this->setExtraAlias($aGood['card']);

        $aRow = array_merge($aGood,[
            'url' => catalog\Parser::buildUrl($this->iSectionId, $aGood['id'], $aGood['alias']),
            'seo' => $this->parseSeoData( ['sectionId' => $this->iSectionId] )
        ]);

        return $aRow;

    }


    /**
     * @inheritdoc
     */
    public function doExistRecord($sPath){

        $sTail = '';
        Tree::getSectionByPath($sPath, $sTail);
        $sTail = trim($sTail, '/');

        return ($aRecord = catalog\GoodsSelector::get($sTail))
            ? $aRecord['id']
            : false
        ;


    }

    /**
     * Получить пустой товар
     * @param int|string $sCard - карточка товара
     * @return array
     */
    public static function getBlankGood( $sCard ){

        $oGoodsRow = catalog\GoodsRow::create($sCard);

        $aGoodData =  catalog\Parser::get( $oGoodsRow->getFields() )
            ->parseGood( [], [], false );

        return $aGoodData;

    }

    /**
     * @inheritdoc
     */
    public function doSupportAltTitle(){
        return true;
    }

    /**
     * @inheritdoc
     */
    public function doSupportNameImage(){
        return true;
    }

    /**
     * @inheritdoc
     */
    public function getIndividualTemplate4Section(){
        return Template::getByAliases(static::getAlias(), $this->iSectionId);
    }


}