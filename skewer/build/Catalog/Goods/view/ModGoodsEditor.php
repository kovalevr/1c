<?php

namespace skewer\build\Catalog\Goods\view;

use skewer\base\ft\Editor;
use \skewer\build\Page\CatalogMaps;
use skewer\build\Catalog\Goods\SeoGood;
use skewer\build\Catalog\Goods\SeoGoodModifications;
use skewer\components\seo\Api;
use skewer\components\catalog;
use yii\helpers\ArrayHelper;


/**
 * Построение интерфейса редактирование модификации товара
 * Class ModGoodsEditor
 * @package skewer\build\Catalog\Goods\view
 */
class ModGoodsEditor extends GoodsEditor {

    protected $aUniqFields = array( 'id', 'alias', 'active' );

    public function build() {

        // задание набора полей для интерфейса
        $this->initForm(['active'], $this->aUniqFields, true);

        $aData = $this->model->getData();

        // кнопки
        $this->_form->buttonSave( 'EditModificationsItem' );
        $this->_form->buttonCancel( 'ModificationsItems' );

        if ( $this->model->getGoodsRow()->getRowId() ) {

            $this->_form->buttonSeparator();
            $this->_form->buttonDelete('DeleteModificationsItem');
        }

        $this->_form->useSpecSectionForImages();

        /** @var bool Это общий список товаров(Фильтр = Раздел:Все)? $bIsCommonList */
        $bIsCommonList = !(bool)$this->_module->getCurrentSection();

        $this->_form->getField('gallery')->setDescVal('iEntityId', $this->model->getGoodsRow()->getRowId());
        $this->_form->getField('gallery')->setDescVal('seoClass', SeoGoodModifications::className());
        $this->_form->getField('gallery')->setDescVal('sectionId', $this->_module->getCurrentSection());

        $aExtFields = $this->_form->getForm()->getFields();

        foreach ($aExtFields as $oExtField) {
            if ( $oExtField->getView() == Editor::MAP_SINGLE_MARKER ){
                // Подмена значения в поле
                $aData[$oExtField->getName()] = CatalogMaps\Api::getAddressGeoObjectFormatted( $aData[$oExtField->getName()] );
            }

        }

        // контент
        $this->_form->setValue( $aData );

        if (!$bIsCommonList){

            $bIsNewGood = !(bool)$aData['id'];
            $iBaseGoodId = $this->_module->getInnerData( 'currentGoods' );

            $aGood = $bIsNewGood
                ? SeoGood::getBlankGood( $this->_module->getCardName() )
                : catalog\GoodsSelector::get( $aData['id'] );

            Api::appendExtForm($this->_form, new SeoGoodModifications(ArrayHelper::getValue($aGood, 'id', 0), $this->_module->getCurrentSection(), $aGood), $this->_module->getCurrentSection());
        }
        else
            $this->_form->fieldWithValue('warning_text',\Yii::t('SEO','warning'), 'show', \Yii::t('SEO','warning_text'),['groupTitle' => \Yii::t('SEO', 'group_title'), 'groupType'  => 1]);

    }
}