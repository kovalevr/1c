<?php

namespace skewer\build\Catalog\LeftList;

use skewer\build\Adm;

/**
 * Прототип класса для вкладок
 * Class ModulePrototype
 * @package skewer\build\Catalog\LeftList
 */
class ModulePrototype extends Adm\Tree\ModulePrototype implements ModuleInterface {

    /**
     * @inheritDoc
     */
    public function init()
    {
        \skewer\build\Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    protected function checkAccess() {
    }
}