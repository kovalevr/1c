```css
.[block_class] .section__content {
    padding: 20px 0;
}
.[block_class] .section__inner {
    height: 250px;
}
```

```html
<div class="b-section b-section_fullscreen">
    <div class="section__inner">
        <div class="section__content">
            <h2 class="section__header">{string|Название}</h2>
            <div>{text|Код карты}</div>
        </div>
    </div>
</div>
```