<?php

namespace skewer\build\LandingPage\News;

use skewer\base\section\Parameters;
use skewer\components\lp;
use skewer\build\Adm\News;
use skewer\base\Twig;

/**
 * Class Module
 * @package skewer\build\Page\LandingPage\Editor
 */
class Module extends lp\PagePrototype {

    /**
     * Отдает собранный html код для сборки блока
     * @return string
     */
    protected function getRenderedText() {

        $sContent = lp\Api::getViewForSection( $this->sectionId() );

        /**
         * @todo group object??
         */
        $sGetFromSection = Parameters::getValByName( $this->sectionId(), 'object', 'getFromSection', true );

        $iOnPage= abs(Parameters::getValByName( $this->sectionId(), 'object', 'onPage', true ));
        if (!$iOnPage) return '';

        $aNewsList = News\models\News::getPublicList(array(
            'page' => $this->getInt( 'page', 1 ),
            'order'    => 'DESC',
            'future'   => '',
            'byDate'   => '',
            'on_page'  => $iOnPage,
            'on_main'  => '',
            'section'  => explode(',', $this->sectionId() . ",$sGetFromSection"),
            'all_news' => '',
            'show_list'=> ''
        ));

        $aData = array();
        foreach ( $aNewsList->getModels() as $oNews ) {
            /**
             * @var \skewer\build\Adm\News\models\News $oNews
             */
            $aNews = $oNews->getAttributes();
            if ( $oNews->parent_section == $this->sectionId() )
                $aNews['without_href'] = true;
            $aData['aNewsList'][] = $aNews;
        }
        
        \Yii::$app->router->setLastModifiedDate(News\models\News::getMaxLastModifyDate());

        return Twig::renderSource( $sContent, $aData );

    }

}