<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 06.02.2017
 * Time: 15:35
 */

namespace skewer\build\Design\ParamEditor;
use skewer\base\SysVar;
use yii\base\UserException;

/**
 * Class Custom
 * @package skewer\build\Design\ParamEditor
 * Позволяет создавать методы которые будут запускаться при сохранении параметра
 * TODO за ненадобностью временно убираем.
 */

//class Custom{
//
//    /**
//     * Подготовка Page.favicon
//     * @param $sValue
//     * @return string
//     * @throws UserException
//     */
//    public static function beforeSavePageFavicon($sValue){
//
//        if (!file_exists(ROOTPATH.'web'.$sValue)) return $sValue;
//
//        list($width, $height, $type, $attr) = getimagesize(ROOTPATH.'web'.$sValue);
//
//        if (SysVar::get('favicon_validate') && ($width!==16 || $height!==16))
//            throw new UserException(\Yii::t('design','invalid_favicon'));
//
//        copy(ROOTPATH.'web'.$sValue,ROOTPATH.'web/favicon'.strtolower(substr($sValue, -4)));
//
//        return '/favicon'.strtolower(substr($sValue, -4));
//    }
//
//
//    /**
//     * Выполнить после сохранения Page.favicon
//     * @param $oModule
//     */
//    public static function afterSavePageFavicon($oModule){
//
//        $oModule->fireJSEvent( 'reload' );
//    }
//}