<?php

namespace skewer\build\Design\Settings;

use skewer\base\SysVar;
use skewer\base\ui;
use skewer\build\Cms;
use skewer\components\design\Design;
use skewer\components\ext;
use yii\base\Exception;


class Module extends Cms\Tabs\ModulePrototype {

    /**
     * Состояние начальное
     */
    protected function actionInit() {

        $this->actionGetSettingsForm();

    }

    /**
     * Сохранение настроек
     */
    protected function actionSaveSettingsForm(){

        \Yii::$app->session->set('Settings.DeleteParams',$this->getInDataVal('DeleteParams'));

        $this->addMessage('Сохранено');
        $this->actionInit();

        //$this->fireJSEvent('reload_show_frame');
        $this->fireJSEvent('reload_all');
       // $this->fireJSEvent('reload_inheritance');
    }

    /**
     * Форма настроек
     */
    protected function actionGetSettingsForm()
    {
        $oForm = new ext\FormView();

        $aItems = array();

        $aItems['DeleteParams'] = array(
            'name' => 'DeleteParams',
            'title' => 'Разрешить удаление параметров',
            'view' => 'check',
            'value' => (bool)\Yii::$app->session->get('Settings.DeleteParams'),
        );

        $oForm->addExtButton(ext\docked\Api::create('Сохранить' )
            ->setAction( 'saveSettingsForm' )->unsetDirtyChecker());

        $oForm->setFields($aItems);
        $oForm->setValues(array());

        $this->setInterface($oForm);
    }

}
