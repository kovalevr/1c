<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.02.2017
 * Time: 10:31
 */

namespace skewer\build\Tool\YandexExport\view;

use skewer\components\ext\view\FormView;

class ShowTask extends FormView
{
    public $aData;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->fieldHide('id', 'ID')
            ->fieldCheck('active', \Yii::t('yandexExport', 'active_task'))

            ->fieldInt('c_min', \Yii::t('schedule', 'c_min'), ['minValue' => 0, 'maxValue' => 59])
            ->fieldInt('c_hour', \Yii::t('schedule', 'c_hour'), ['minValue' => 0, 'maxValue' => 23])
            ->fieldInt('c_day', \Yii::t('schedule', 'c_day'), ['minValue' => 1, 'maxValue' => 31])
            ->fieldInt('c_month', \Yii::t('schedule', 'c_month'), ['minValue' => 1, 'maxValue' => 12])
            ->fieldInt('c_dow', \Yii::t('schedule', 'c_dow'),  ['minValue' => 1, 'maxValue' => 7])

            ->setValue( $this->aData )

            ->buttonSave('saveTask')
            ->buttonCancel()
        ;
    }
}