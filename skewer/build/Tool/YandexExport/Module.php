<?php

namespace skewer\build\Tool\YandexExport;

use skewer\base\queue\Task;
use skewer\build\Adm;
use skewer\components\catalog;
use skewer\build\Tool;
use skewer\base\ui;
use skewer\base\SysVar;
use skewer\base\queue\ar\Schedule;
use skewer\build\Tool\YandexExport;

class Module extends Adm\Order\Module implements Tool\LeftList\ModuleInterface {

    private $iExportLimit = 50;

    /**
     * @inheritDoc
     */
    public function init()
    {
        Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    function getName() {
        return $this->getModuleName();
    }

    /**
     * Действие: Обновление файла выгрузки
     */
    protected function actionRunExport(){

        $aData = $this->getInData();
        if (empty($aData) && $this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $taskId = (isset($aData['taskId'])) ? $aData['taskId'] : 0;

        /** Запуск импорта */
        $aRes = Api::runExport($taskId);

        if ( in_array($aRes['status'], [Task::stFrozen, Task::stWait]) ){
            /** Ставим на повторный запуск */
            $this->addJSListener( 'runExport', 'runExport' );
            $this->fireJSEvent( 'runExport', ['taskId' => $aRes['id']] );
        }

        $this->actionInit();

    }

    public function actionSettings() {

        $this->render(new YandexExport\view\Settings([
            "aValue" => [
                'shopName' => SysVar::getSafe('YandexExport.shopName', ''),
                'companyName' => SysVar::getSafe('YandexExport.companyName', ''),
                'localDeliveryCost'=> SysVar::getSafe('YandexExport.localDeliveryCost', '')
            ]
        ]));
    }

    public function actionSaveSettings() {

        $data = $this->getInData();
        $sShopName = $data['shopName'];
        $sCompanyName = $data['companyName'];
        $sLocalDeliveryCost = $data['localDeliveryCost'];

        SysVar::set('YandexExport.shopName', $sShopName);
        SysVar::set('YandexExport.companyName', $sCompanyName);
        SysVar::set('YandexExport.localDeliveryCost', $sLocalDeliveryCost);

        $this->actionSettings();
    }

    public function actionUtils(){

        $aTree = Api::getSections();

        $group = catalog\Card::getGroupByName('yandex');
        $fields = $group->getFields();

        $aEditableFields = array();
        // собираем чекбоксы

        $aFieldNameTitle = array();
        foreach($fields as $field){
            if ($field->group == $group->id && $field->editor=='check'){
                $aEditableFields[] = $field->name;

                $aFieldNameTitle[$field->name] = $field->title;
                foreach ($aTree as $k=>$item) {
                    $aTree[$k][$field->name] = $field->def_value;

                    // Убрать не каталожные разделы из настроек
                    if (!$item['isCatalogSection'])
                        unset($aTree[$k]);
                }
            }
        }

        $this->render(new YandexExport\view\Utils([
            "aFieldNameTitle" => $aFieldNameTitle,
            "aTree" => $aTree,
            "aEditableFields" => $aEditableFields
        ]));
        return psComplete;
    }

    /**
     * Метод сохраняет набор настроек для товаров выбранного раздела
     * Сам список галочек не сохраняется, только изменяются данные товаров
     * @throws \Exception
     * @throws catalog\Exception
     */
    public function actionSaveParam(){

        $aData = $this->getInData();

        if (!$aData){
            $aParams = $this->get('params');
            if (isset($aParams[0]['data'])){
                $aData = $aParams[0]['data'];
            }
        }

        if (!isset($aData['id'])) return;

        $sectionId = $aData['id'];

        // оставляем только метки от яндекс каталога, удаляем все лишнее
        unset($aData['id']);
        unset($aData['title']);
        unset($aData['parent']);

        $iPage = (isset($aData['page']))?$aData['page']:1;

        $query = catalog\GoodsSelector::getList4Section( $sectionId )
            ->limit( $this->iExportLimit, $iPage )
        ;

        $iCount = 0;
        while ( $aGoods = $query->parseEach() ) {

            $oGoodsRow = catalog\GoodsRow::get( $aGoods['id'] );

            $oGoodsRow->setData( $aData );

            $iCount++;
            $oGoodsRow->save();
        }

        $aData['id'] = $sectionId;

        if ($iCount >= $this->iExportLimit){
            $iPage++;
            $aData['page'] = $iPage;
            $this->addJSListener( 'reload_save_param', 'saveParam' );
            $this->fireJSEvent( 'reload_save_param', array('data' => $aData) );
        }

    }


    public function actionSaveCheck(){
        return psComplete;
    }

    public function actionInit() {
        $sRunExportTitle = (!is_file(WEBPATH . YandexExport\Task::sFilePath)) ?
            \Yii::t('yandexExport', 'create') :
            \Yii::t('yandexExport', 'refresh');

        $sReportText = SysVar::get('Yandex.report');
        $sLogs = SysVar::get('Yandex.log');

        $sHeadText = '';
        if ($sReportText && is_file(WEBPATH.YandexExport\Task::sFilePath)) $sHeadText = $sReportText . $sLogs;
        else $sHeadText = "Выгрузка не создана" . $sLogs;

        $this->render(new YandexExport\view\Init([
            "sRunExportTitle" => $sRunExportTitle,
            "sHeadText" => $sHeadText
        ]));
        return psComplete;
    }

    /** Состояние: "Настройка задания" */
    protected function actionShowTask() {
        $this->setPanelName(\Yii::t('yandexExport', 'task_form'));

        $aTaskConfig = \skewer\build\Tool\YandexExport\Task::getConfig();

        $aCommand = json_encode([
            'class' => $aTaskConfig['class'],
            'parameters' => $aTaskConfig['parameters']
        ]);

        $aData = [];

        if ($oTask = Schedule::findOne(['command'=>$aCommand])){
            $aData = $oTask->attributes;
            $aData['active'] = 1;
        }

        $this->render(new YandexExport\view\ShowTask([
            "aData" => $aData
        ]));
    }

    /**
     * Действие: Сохранение задачи в планировщике
     */
    protected function actionSaveTask(){

        $aData = $this->getInData();

        $aSettingsSchedule = \skewer\build\Tool\YandexExport\Task::getConfig();

        $aSettingsSchedule['command'] = json_encode(['class' => $aSettingsSchedule['class'], 'parameters' => $aSettingsSchedule['parameters']]);

        $aSettingsSchedule['status'] = Task::stNew;


        if (!$aData['active'])
            Schedule::deleteAll(['id'=>$aData['id']]);
        else{

            if (!$oSchedule = Schedule::findOne($aData['id']))
                $oSchedule = new Schedule();

            $oSchedule->setAttributes($aData + $aSettingsSchedule);

            if ( !$oSchedule->save() )
                throw new ui\ARSaveException( $oSchedule );

        }

        $this->actionInit();
    }


}