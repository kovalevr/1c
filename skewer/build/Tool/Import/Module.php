<?php

namespace skewer\build\Tool\Import;


use skewer\base\SysVar;
use skewer\components\import\Api;
use skewer\components\import\ar\ImportTemplate;
use skewer\components\import\Config;
use skewer\components\import\Task;
use skewer\base\ui;
use skewer\build\Tool;
use skewer\components\auth\CurrentAdmin;
use skewer\base\queue\ar\Schedule;
use yii\base\UserException;

/**
 * Модуль настройки шаблонов для импорта данных в каталога, запуска импорта и чтения статистики
 * Class Module
 */
class Module extends Tool\LeftList\ModulePrototype {

    /** @var bool Флаг того, что админ sys */
    protected $isSys = false;

    protected function actionInit(){

        $this->isSys = $this->isSys();
        $this->actionList();
    }

    /**
     * id текущего шаблона
     * @return int
     */
    private function getTplId(){

        $iTpl = $this->getInDataValInt( 'id' );
        if (!$iTpl){
            $iTpl = $this->getInnerDataInt( 'tpl_id' );
        }

        $this->setInnerData( 'tpl_id', $iTpl );

        return $iTpl;
    }

    /**
     * Список шаблонов импорта
     */
    protected function actionList(){

        $this->setPanelName(\Yii::t('import', 'tpl_list'));

        $aList = Api::getTemplateList();

        $this->render(new Tool\Import\view\Index([
            "isSys" => $this->isSys,
            "aList" => $aList,
            "bIsNotDirImport" => (!is_dir(ROOTPATH.'import/'))
        ]));
    }

    /**
     * Добавление нового шаблона
     */
    protected function actionAdd(){

        $this->showHeadSettingsForm();

    }

    /**
     * Основные настройки
     * @param null|int $iTpl
     */
    protected function actionHeadSettings( $iTpl = null ){

        if ( !$iTpl )
            $iTpl = $this->getTplId();

        if ($this->isSys)
            $this->showHeadSettingsForm( $iTpl );
        else
            $this->showClientForm( $iTpl );

    }


    /**
     * Форма основных настроек
     * @param null|int $id id шаблона
     */
    private function showHeadSettingsForm( $id = null ){

        $this->setPanelName(\Yii::t('import', 'head_settings_form'));

        $oTemplate = Api::getTemplate( $id );

        $aData = $oTemplate->getData();

        if ( $oTemplate->type == Api::Type_File ){
            $aData['source_file'] = $aData['source'];
        }else{
            $aData['source_str'] = $aData['source'];
        }

        $sGroup = \Yii::t('import', 'head_settings_form');

        $this->render(new Tool\Import\view\HeadSettingsForm([
            "sGroup" => $sGroup,
            "aCardList" => Api::getCardList(),
            "aProviderTypeList" => Api::getProviderTypeList(),
            "aTypeList" => Api::getTypeList(),
            "aCodingList" => Api::getCodingList(),
            "aData" => $aData,
            "id" => $id
        ]));

    }

    /**
     * Форма основных настроек для клиента
     * @param null|int $id id шаблона
     */
    private function showClientForm( $id = null ){
        $this->setPanelName(\Yii::t('import', 'head_settings_form'));

        $oTemplate = Api::getTemplate( $id );

        $aData = $oTemplate->getData();

        if ( $oTemplate->type == Api::Type_File ){
            $aData['source_file'] = $aData['source'];
        }else{
            $aData['source_str'] = $aData['source'];
        }

        $sGroup = \Yii::t('import', 'head_settings_form');

        $this->render(new Tool\Import\view\ClientForm([
            "sGroup" => $sGroup,
            "bEqualTypes" => ($oTemplate->type == Api::Type_File),
            "aData" => $aData
        ]));
    }

    /**
     * Настройки провайдера
     */
    protected function actionProviderSettings(){
        $iTpl = $this->getTplId();
        $this->showProviderSettingsForm( $iTpl );
    }


    private function showProviderSettingsForm( $iTpl ){

        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTemplate = Api::getTemplate( $iTpl );

        if (!$oTemplate){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'provider_settings_form'));

        $this->render(new Tool\Import\view\ProviderSettingsForm([
            "oTemplate" => $oTemplate
        ]));

    }

    /**
     * Настройки соответствия полей
     */
    protected function actionFields(){
        $iTpl = $this->getTplId();
        $this->showFieldsForm( $iTpl );
    }

    /**
     * Форма настроек соответствия полей
     * @param $iTpl
     * @throws UserException
     * @throws UserException
     */
    private function showFieldsForm( $iTpl ){
        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTemplate = Api::getTemplate( $iTpl );

        if (!$oTemplate){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'fields_form'));

        // #39953 Поиск отсутствующих полей импорта в карточке и выдача сообщения
        if ($sMessages = Api::checkImportFields($iTpl))
            $this->addMessage(\Yii::t('Import', 'warning'), $sMessages, 5000);

        $this->render(new Tool\Import\view\FieldsForm([
            "oTemplate" => $oTemplate
        ]));
    }


    /**
     * Форма настройки полей
     */
    protected function actionFieldsSettings(){

        $iTpl = $this->getTplId();
        $this->showFieldSettingsForm( $iTpl );

    }


    private function showFieldSettingsForm( $iTpl = null ){
        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTemplate = Api::getTemplate( $iTpl );

        if (!$oTemplate){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'fields_settings_form'));

        $this->render(new Tool\Import\view\FieldSettingsForm([
            "oTemplate" => $oTemplate
        ]));
    }


    /**
     * Сохранение
     */
    protected function actionSave(){

        $aData = $this->getInData();

        $oTpl = Api::getTemplate( (isset($aData['id'])) ? $aData['id'] : null );

        if ( $this->isSys ) {
            $aRequiredList = ImportTemplate::getModel()->getColumnSet('required');
            foreach ($aRequiredList as $sFieldName) {
                if (!isset($aData[$sFieldName]) || !$aData[$sFieldName])
                    throw new UserException(\Yii::t('import', 'not_defined_field', \Yii::t('import', 'field_' . $sFieldName)));
            }
        }

        $sOldType = $oTpl->provider_type;
        $sOldCard = $oTpl->card;
        $oTpl->setData($aData);

        if ($oTpl->type == Api::Type_File){
            $oTpl->source = (isset($aData['source_file'])) ? $aData['source_file'] : '';
        }else{
            $oTpl->source = (isset($aData['source_str'])) ? $aData['source_str'] : '';
        }

        if (!$oTpl->source){
            throw new UserException( \Yii::t('import', 'not_defined_field', \Yii::t( 'import', 'field_source' )) );
        }

        $id = $oTpl->save();
        if ($id){
            /** Сменился провайдер или карточка - почистим конфиг */
            if ($sOldType != $oTpl->provider_type || $sOldCard != $oTpl->card){
                $oConfig = new Config( $oTpl );
                $oConfig->clearFields();
                $oTpl->settings = json_encode( $oConfig->getData() );
                $oTpl->save();
            }
            $this->actionHeadSettings( $id );
        }else{
            throw new UserException( \Yii::t('import', 'error_no_save') );
        }

    }

    /**
     * Сохранение настроек провайдера
     */
    protected function actionSaveProviderSettings(){

        $aData = $this->getInData();

        if (!isset($aData['id'])){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTpl = Api::getTemplate( $aData['id'] );

        if (!$oTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oConfig = new Config( $oTpl );
        $oProvider = Api::getProvider( $oConfig );

        $aVars = $oConfig->getData();

        /** Параметры провайдера */
        foreach ($oProvider->getParameters() as $key => $value){
            $aVars[$key] = (isset($aData[$key])) ? $aData[$key] : ((isset($value['default'])) ? $value['default'] : '');
        }

        $oTpl->settings = json_encode( $aVars );
        $oTpl->save();
    }


    /**
     * Сохранение настроек соответсвия полей
     */
    protected function actionSaveFields(){

        $aData = $this->getInData();

        if (!isset($aData['id'])){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTpl = Api::getTemplate( $aData['id'] );

        if (!$oTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oConfig = new Config( $oTpl );
        $oConfig->setFields( $aData, true );

        $oTpl->settings = $oConfig->getJsonData();

        $oTpl->save();

        /** Проверка на наличие уникального поля */
        $bUnique = false;
        foreach($aData as $sKey => $value){
            if (preg_match( '/type_(\w+)/', $sKey )){
                if ($value === 'Unique'){
                    $bUnique = true;
                    break;
                }
            }
        }

        if (!$bUnique)
            throw new UserException(\Yii::t('import', 'error_unique_field_not_found'));

    }


    /**
     * Сохранение настроек полей
     */
    protected function actionSaveSettingsFields(){

        $aData = $this->getInData();

        if (!isset($aData['id'])){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTpl = Api::getTemplate( $aData['id'] );

        if (!$oTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oConfig = new Config( $oTpl );
        $oConfig->setFieldsParam( $aData );

        $oTpl->settings = $oConfig->getJsonData();
        $oTpl->save();
    }


    /**
     * Удаление шаблона
     */
    protected function actionDelete(){

        $id = $this->getInDataValInt( 'id' );

        if ($id){
            ImportTemplate::delete( $id );
            Api::deleteLog4Template( $id );
        }

        $this->actionList();

    }

    /**
     * Задача
     */
    protected function actionShowTask(){

        $this->setPanelName(\Yii::t('import', 'task_form'));

        $command = json_encode([
            'class' => '\skewer\components\import\Task',
            'parameters' => ['tpl' => $this->getTplId()]
        ]);

        $aData = [];
        /** @var Schedule $task */
        $task = Schedule::findOne(['command'=>$command]);

        if ($task){

            foreach($task as $key=>$val) $aData[$key]=$val;

            $aData['active'] = 1;
            $aData['schedule_id'] = $task->id;
        }

        $this->render(new Tool\Import\view\ShowTask([
            "aData" => $aData
        ]));
    }

    /**
     * Сохранение задачи
     */
    protected function actionSaveTask(){

        $aData = $this->getInData();

        $command = json_encode([
            'class' => '\skewer\components\import\Task',
            'parameters' => ['tpl' => (int)$this->getTplId()]
        ]);

        if ($aData['active']){

            /** @var \skewer\components\import\ar\ImportTemplateRow $oTpl */
            $oTpl = ImportTemplate::find($this->getTplId());
            if (!$oTpl)
                throw new \Exception(\Yii::t('import', 'error_tpl_not_fount'));

            //save
            $aData['id'] = $aData['schedule_id'];
            $aData['command'] = $command;
            $aData['name'] ='import_' . $oTpl->id;
            $aData['title'] = \Yii::t( 'import', 'task_title', $oTpl->title);
            $aData['priority'] = Task::priorityHigh;
            $aData['resource_use'] = Task::weightHigh;
            $aData['target_area'] = 3;
            $aData['status'] = Task::stNew;

            if( !$scheduleItem = Schedule::findOne($aData['id']) ){
                $scheduleItem = new Schedule;
                unset($aData['id']);
            }

            $scheduleItem->setAttributes($aData);

            if ( !$scheduleItem->save() )
                throw new ui\ARSaveException( $scheduleItem );

        } else {

            //remove
            Schedule::deleteAll(['id'=>$aData['schedule_id']]);

        }

        $this->actionHeadSettings();
    }

    /**
     * Запуск импорта
     */
    protected function actionRunImport(){

        $aData = $this->getInData();
        if (empty($aData) && $this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $taskId = (isset($aData['taskId'])) ? $aData['taskId'] : 0;

        $iTpl = (isset($aData['id'])) ? $aData['id'] : 0;
        if (!$iTpl){
            $iTpl = $this->getInnerDataInt( 'tpl_id' );
        }

        if (!$iTpl && !$taskId){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        if ($iTpl){
            $this->setInnerData( 'tpl_id', $iTpl );
            $this->setInnerData( 'importRun', $iTpl );
        }

        /** Запуск импорта */
        $aRes = Api::runImport( $taskId, $iTpl );

        if ( in_array($aRes['status'], [Task::stFrozen, Task::stWait]) ){
            /** Крутим, ели еще нужно */
            $this->addJSListener( 'runImport', 'runImport' );
            $this->fireJSEvent( 'runImport', ['taskId' => $aRes['id']] );

        } elseif ($oTemplate = Api::getTemplate($iTpl)) {

            // #39953 Поиск отсутствующих полей импорта в карточке и выдача сообщения в конце импорта
            if ($sMessages = Api::checkImportFields($iTpl))
                $this->addMessage(\Yii::t('Import', 'warning'), $sMessages, 5000);
        }

        $this->showLog( $aRes['id'] );
    }

    /**
     * Список логов
     */
    protected function actionLogList(){

        $this->setPanelName(\Yii::t('import', 'logs_list'));

        $iTpl = $this->getInnerDataInt( 'tpl_id' );
        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setInnerData('importRun', false);

        $this->render(new view\LogList([
            "sWidgetClsName" => (__NAMESPACE__.'\View'),
            "aLogs" => Api::getLogs( $iTpl )
        ]));

    }

    /**
     * Детальная лога
     */
    protected function actionDetailLog(){

        $iTpl = $this->getInDataValInt( 'id_log' );

        $this->showLog( $iTpl );

    }


    private function showLog( $id ){
        if (!$id){
            throw new UserException( \Yii::t('import', 'error_task_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'log_show'));

        $log = Api::getLog( $id );

        if (isset($log['status'])){
            $log['status'] = View::getStatus($log);
        }

        $baskAction = $this->getInnerData('importRun')?'headSettings':'logList';

        $sText = $this->renderTemplate('log_template.twig', ['log' => $log]);

        $this->render(new Tool\Import\view\Log([
            "baskAction" => $baskAction,
            "sText" => $sText
        ]));
    }


    /**
     * Удаление записи лога
     */
    protected function actionDeleteLog(){

        $id = $this->getInDataValInt('id_log');
        if (!$id){
            throw new UserException( \Yii::t('import', 'error_task_not_fount') );
        }

        Api::deleteLog( $id );

        $this->actionLogList();
    }


    /**
     * Добавление папки импорта
     */
    protected function actionAddFolder(){

        if (mkdir(ROOTPATH.'import')){
            /**
             * @todo вот тут должно быть 755, но на наших sk* это не покатит, потом надо исправить
             */
            chmod(ROOTPATH.'import', 0777);
            $this->addMessage(\Yii::t('import', 'folderCreateHeader'), \Yii::t('import', 'folderCreate'));
        }else{
            $this->addMessage(\Yii::t('import', 'folderCreateHeader'), \Yii::t('import', 'folderNonCreate'));
        }

        $this->actionInit();

    }


    /**
     * Определяет, является ли текущий админ sys-ом
     * @return bool
     */
    protected function isSys(){

        $aUserData = CurrentAdmin::getUserData();
        if (!isset($aUserData['login']))
            return false;

        return $aUserData['login'] == 'sys';

    }

    protected function actionSettingTrade(){

        $this->render(new Tool\Import\view\SettingsTrade());

    }

    protected function actionSaveSettingsTrade(){

        $aFormData = $this->get('data');

        foreach ($aFormData as $sKey => $mParam) {
            SysVar::set("1c.{$sKey}", $mParam);
        }

        $this->actionList();


    }


}