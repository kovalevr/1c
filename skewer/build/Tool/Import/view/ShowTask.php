<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 03.03.2017
 * Time: 10:37
 */

namespace skewer\build\Tool\Import\view;

use skewer\components\ext\view\FormView;

class ShowTask extends FormView
{
    public $aData;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('schedule_id', 'schedule_id', 'hide')
            ->field('active', \Yii::t('import', 'active_task'), 'check')

            ->field('c_min', \Yii::t('schedule', 'c_min'), 'int', ['minValue' => 0, 'maxValue' => 59, 'allowDecimals' => false])
            ->field('c_hour', \Yii::t('schedule', 'c_hour'), 'int', ['minValue' => 0, 'maxValue' => 23, 'allowDecimals' => false])
            ->field('c_day', \Yii::t('schedule', 'c_day'), 'int', ['minValue' => 1, 'maxValue' => 31, 'allowDecimals' => false])
            ->field('c_month', \Yii::t('schedule', 'c_month'), 'int', ['minValue' => 1, 'maxValue' => 12, 'allowDecimals' => false])
            ->field('c_dow', \Yii::t('schedule', 'c_dow'), 'int', ['minValue' => 1, 'maxValue' => 7, 'allowDecimals' => false])

            ->setValue( $this->aData )

            ->buttonSave('saveTask')
            ->buttonCancel('headSettings');
    }
}