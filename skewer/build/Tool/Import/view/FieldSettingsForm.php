<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 02.03.2017
 * Time: 18:13
 */

namespace skewer\build\Tool\Import\view;

use skewer\build\Tool\Import\View;
use skewer\components\import\Exception;
use yii\base\UserException;

class FieldSettingsForm extends ImportForm
{

    public $oTemplate;
    /**
     * Выполняет сборку интерфейса
     * @return void
     * @throws UserException
     */
    function build() {
        try{
            View::getFieldsSettingsForm( $this->_form, $this->oTemplate );
        } catch (Exception $e){
            throw new UserException( $e->getMessage(), $e->getCode(), $e );
        }
        $this->_form->buttonSave( 'saveSettingsFields' );
        $this->addStateButton('fieldsSettings');
        $this->_form->setTrackChanges( false );
    }
}