<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 03.03.2017
 * Time: 11:08
 */

namespace skewer\build\Tool\Import\view;

use skewer\components\ext\view\FormView;

class Log extends FormView
{
    public $baskAction;
    public $sText;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('result', \Yii::t('import', 'log_result'), 'show', ['labelAlign' => 'top', 'height' => '100%'])
            ->buttonBack($this->baskAction, \Yii::t('import', 'back'))
            ->setValue(['result' => $this->sText]);
    }
}