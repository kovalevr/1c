<?php

namespace skewer\build\Tool\SectionSettings;

use skewer\base\ft\Editor;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\base\SysVar;
use skewer\components\gallery\Profile;

/**
 * Модуль Настроек
 * Class Module
 * @package skewer\build\Tool\SectionSettings
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit() {
        $this->render(new Tool\SectionSettings\view\Index([
            "aSettings" =>  [
                'menuIcons' => SysVar::get('Menu.ShowIcons'),
                'newsDetailLink' => SysVar::get('News.showDetailLink'),
                'showGalleryInNews' => SysVar::get('News.hideGallery'),
                'onCheckSizeOpenGraphImage' => SysVar::get('OpenGraph.onCheckSizeImage'),
                'lock_section_flag' => SysVar::get('lock_section_flag'),
                'album_fotorama_flag' => SysVar::get('Gallery.ShowFotorama'),
                'favicon_validate' => SysVar::get('favicon_validate'),
                'type_list_of_sections' => SysVar::get('Page.type_list_of_sections'),
                'default_img' => SysVar::get('Gallery.DefaultImg'),
                'hideFields' => SysVar::get('Reviews.hideFields'),
            ]
        ]));
    }

    /**
     * Сохранение
     */
    protected function actionSave(){

        SysVar::set('Menu.ShowIcons', $this->getInDataVal('menuIcons'));
        SysVar::set('News.showDetailLink', $this->getInDataVal('newsDetailLink'));
        SysVar::set('News.hideGallery', $this->getInDataVal('showGalleryInNews'));
        SysVar::set('OpenGraph.onCheckSizeImage', $this->getInDataVal('onCheckSizeOpenGraphImage'));
        SysVar::set('lock_section_flag',$this->getInDataVal('lock_section_flag'));
        SysVar::set('Gallery.ShowFotorama',$this->getInDataVal('album_fotorama_flag'));
        SysVar::set('favicon_validate',$this->getInDataVal('favicon_validate'));
        SysVar::set('Gallery.DefaultImg',$this->getInDataVal('default_img'));
        SysVar::set('Page.type_list_of_sections',$this->getInDataVal('type_list_of_sections'));
        SysVar::set('Reviews.hideFields',$this->getInDataVal('hideFields'));

        $this->actionInit();
    }

}