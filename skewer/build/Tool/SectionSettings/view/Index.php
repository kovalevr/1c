<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 27.01.2017
 * Time: 10:14
 */

namespace skewer\build\Tool\SectionSettings\view;

use skewer\base\ft\Editor;
use skewer\components\ext\view\FormView;
use skewer\components\gallery\Profile;

class Index extends FormView
{
    public $aSettings;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('menuIcons', \Yii::t('page', 'menuIcons'), 'check')
            ->field('newsDetailLink', \Yii::t('page', 'newsDetailLink'), 'check')
            ->field('showGalleryInNews', \Yii::t('page', 'showGalleryInNews'), 'check')
            ->field('onCheckSizeOpenGraphImage', \Yii::t('page', 'onCheckSizeOpenGraphImage'), 'check')
            ->field('showGalleryInNews', \Yii::t('page', 'showGalleryInNews'), 'check')
            ->field('onCheckSizeOpenGraphImage', \Yii::t('page', 'onCheckSizeOpenGraphImage'), 'check')
            ->field('onCheckSizeOpenGraphImage', \Yii::t('page', 'onCheckSizeOpenGraphImage'), 'check')
            ->field('lock_section_flag', \Yii::t('page', 'lock_section_flag'), 'check')
            ->field('album_fotorama_flag', \Yii::t('page', 'album_fotorama_flag'), 'check')
            ->field('favicon_validate', \Yii::t('page', 'favicon_validate'), 'check')
            ->fieldSelect('type_list_of_sections', \Yii::t('page', 'type_list_of_sections'), [ 'under_image' => \Yii::t('page', 'header_under_image'), 'on_image' => \Yii::t('page', 'header_on_image')], [], false)
            ->field('default_img', \Yii::t('page', 'default_img'), Editor::GALLERY,
                ['show_val' => Profile::getDefaultId(Profile::TYPE_NEWS)])
            ->fieldCheck('hideFields',\Yii::t('review','section_hide_fields'))

            ->setValue($this->aSettings)
            ->buttonSave('save')
            ->buttonCancel()
        ;
    }
}