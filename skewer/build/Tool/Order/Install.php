<?php

namespace skewer\build\Tool\Order;


use skewer\base\site;
use skewer\components\config\InstallPrototype;
use skewer\base\SysVar;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }

    public function install() {

        SysVar::set( 'syte_type', site\Type::shop );
        return true;
    }// func

    public function uninstall() {

        if (site\Type::isShop()){
            $this->fail("Нельзя удалить магазин");
        };

        return true;
    }// func

}//class
