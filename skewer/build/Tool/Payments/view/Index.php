<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 19.01.2017
 * Time: 15:00
 */

namespace skewer\build\Tool\Payments\view;

use skewer\components\ext\view\ListView;

class Index extends ListView
{
    public $aItems;
    /**
     * @inheritdoc
     */
    function build() {
        $this->_list
            ->field('title', \Yii::t('payments', 'title'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('value', \Yii::t('payments', 'active'), 'check', array('listColumns' => array('flex' => 1)))
            ->setValue( $this->aItems )
            ->setEditableFields( array('value'), 'saveActive' )
            ->buttonRowUpdate( 'edit' );
    }
}