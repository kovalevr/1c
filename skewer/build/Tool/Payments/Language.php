<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Системы оплат';
$aLanguage['ru']['title'] = 'Название';
$aLanguage['ru']['active'] = 'Активность';
$aLanguage['ru']['test_life'] = 'Тестовый режим';
$aLanguage['ru']['robokassa'] = 'Робокасса';
$aLanguage['ru']['payanyway'] = 'Payanyway';

$aLanguage['ru']['Robokassa_login_field'] = 'Идентификатор магазина';
$aLanguage['ru']['Robokassa_password1_field'] = 'Пароль 1';
$aLanguage['ru']['Robokassa_password2_field'] = 'Пароль 2';
$aLanguage['ru']['Payanyway_login_field'] = 'Логин';
$aLanguage['ru']['Payanyway_password_field'] = 'Пароль';
$aLanguage['ru']['Payanyway_invoice_field'] = 'Номер счета';
$aLanguage['ru']['Payanyway_code_field'] = 'Код проверки целостности данных';
$aLanguage['ru']['PayPal_clientId_field'] = 'Id клиента';
$aLanguage['ru']['PayPal_clientSecret_field'] = 'Секретный ключ';
$aLanguage['ru']['PayPal_currency_field'] = 'Трехзначный код валюты';
$aLanguage['ru']['paypal'] = 'PayPal';

$aLanguage['ru']['yandexkassa'] = 'ЯндексКасса';
$aLanguage['ru']['YandexKassa_shopId_field'] = 'Идентификатор Контрагента';
$aLanguage['ru']['YandexKassa_scid_field'] = 'Номер витрины Контрагента';
$aLanguage['ru']['YandexKassa_shopPassword_field'] = 'Пароль магазина';

$aLanguage['ru']['button_label'] = 'Оплатить';

$aLanguage['en']['tab_name'] = 'Payment systems';
$aLanguage['en']['title'] = 'Name';
$aLanguage['en']['active'] = 'Active';
$aLanguage['en']['test_life'] = 'Test mode';
$aLanguage['en']['robokassa'] = 'Robokassa';
$aLanguage['en']['payanyway'] = 'Payanyway';

$aLanguage['en']['Robokassa_login_field'] = 'Scout shop';
$aLanguage['en']['Robokassa_password1_field'] = 'Password 1';
$aLanguage['en']['Robokassa_password2_field'] = 'Password 2';
$aLanguage['en']['Payanyway_login_field'] = 'Login';
$aLanguage['en']['Payanyway_password_field'] = 'Password';
$aLanguage['en']['Payanyway_invoice_field'] = 'Account number';
$aLanguage['en']['Payanyway_code_field'] = 'Code of data integrity verification';
$aLanguage['en']['PayPal_clientId_field'] = 'Client Id';
$aLanguage['en']['PayPal_clientSecret_field'] = 'Client Secret';
$aLanguage['en']['PayPal_currency_field'] = '3-letter Currency Code';
$aLanguage['en']['paypal'] = 'PayPal';

$aLanguage['en']['yandexkassa'] = 'YandexKassa';
$aLanguage['en']['YandexKassa_shopId_field'] = 'Counterparty ID';
$aLanguage['en']['YandexKassa_scid_field'] = 'Room showcases Counterparty';
$aLanguage['en']['YandexKassa_shopPassword_field'] = 'Password';

$aLanguage['en']['button_label'] = 'Proceed to checkout';

return $aLanguage;