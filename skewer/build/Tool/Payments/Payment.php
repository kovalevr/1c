<?php

namespace skewer\build\Tool\Payments;
use skewer\base\site\Site;
use skewer\base\site_module\Parser;


/**
 * Class Payments
 * @package skewer\build\Tool\Payments
 */
abstract class Payment{

    protected static $instances = [];

    /** @var bool Активность */
    protected $active = false;
    
    /** @var bool Тестовый режим */
    protected $test_life = true;

    /** @var int Идентификатор заказа */
    protected $orderId = 0;

    /** @var float Сумма заказа */
    protected $sum = 0;

    /** @var string Описание заказа */
    protected $description;

    /** @var bool Флаг инициализации параметров */
    protected $bInitParams = false;

    /**
     * @var array Список полей для редактирования
     * Формат [ 'name', 'title', 'type', 'field_type' ]
     */
    protected static $aFields = array();

    private function __construct(){}

    private function __clone(){}

    public static function getInstance() {

        $class = get_called_class();
        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new $class;
            if (method_exists(self::$instances[$class], 'init'))
                self::$instances[$class]->init();
        }
        return self::$instances[$class];

    }

    /**
     * Пользовательская функция инициализации
     */
    public function init(){

    }

    /**
     * Отдает флаг инициализации параметров
     * @return bool
     */
    public function isInitParams(){
        return $this->bInitParams;
    }

    /**
     * Тип агрегатора систем оплат
     * @return mixed
     */
    abstract public function getType();

    /**
     * Метод проверки результата об оплате, полученного от системы платежей
     * * Возвращаемые значения: Если true то заказ оплачен. false -- заказ отменён. Иначе не менять статус заказа
     * @return bool|mixed
     */
    abstract public function checkResult();

    /**
     * Вывод формы для оплаты
     * @return string
     */
    abstract public function getForm();

    /**
     * Сообщение об успешной оплате
     * @return string
     */
    abstract public function getSuccess();

    /**
     * Сообщение о неуспешной оплате
     * @return string
     */
    abstract public function getFail();

    /**
     * Инициализация параметров
     * @param [] $aParams
     */
    abstract public function initParams( $aParams = [] );

    /**
     * Возвращает список полей для редактирования
     * @return array
     */
    public static function getFields(){
        $aFields = array();
        if (count(static::$aFields)){
            foreach (static::$aFields as $aField){
                $aField[1] = \Yii::t( 'payments', $aField[1] );
                $aFields[] = $aField;
            }
        }
        return $aFields;
    }

    /**
     * Возвращает номер заказа
     * @return int Номер заказа
     */
    public function getOrderId() {
        return (int)$this->orderId;
    }

    /**
     * Устанавливает номер заказа
     * @param int $orderId Номер заказа
     */
    public function setOrderId($orderId) {
        $this->orderId = (int)$orderId;
    }

    /**
     * Возвращает сумму заказа
     * @return float Сумма заказа
     */
    public function getSum() {
        return $this->sum;
    }

    /**
     * Устанавливает сумму заказа
     * @param float $sum Сумма заказа
     */
    public function setSum($sum) {
        $this->sum = $sum;
    }

    /**
     * Возвращает описание заказа
     * @return string Описание заказа
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Устанавливает описание заказа
     * @param string $description Описание заказа
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Активность
     * @return bool
     */
    public function getActive(){
        return $this->active;
    }

    /**
     * Установка активности
     * @param $active
     */
    public function setActive( $active ){
        $this->active = (bool)$active;
    }


    /**
     * Парсинг формы
     * @param Form $oForm
     * @return string
     */
    final protected function parseForm( Form $oForm ){
        return Parser::parseTwig('form.twig', array('Form' => $oForm), BUILDPATH.'/Tool/Payments/templates/');
    }

    /**
     * Урл успешной отправки
     * @return string
     */
    protected function getSuccessUrl(){
        return Site::httpDomain().\Yii::$app->router->rewriteURL('[' . \Yii::$app->sections->getValue('payment_success') . ']') . '?order_id=' . $this->orderId;
    }

    /**
     * Урл отмены оплаты
     * @return string
     */
    protected function getCancelUrl(){
        return Site::httpDomain().\Yii::$app->router->rewriteURL('[' . \Yii::$app->sections->getValue('payment_fail') . ']') . '?order_id=' . $this->orderId;
    }

}