<?php

namespace skewer\build\Tool\Review;

use skewer\base\ft\Editor;
use skewer\build\Adm;
use skewer\components\gallery\Profile;
use skewer\components\i18n\Languages;
use skewer\base\orm\state\StateSelect;
use skewer\components\catalog\GoodsSelector;
use skewer\base\section\Tree;
use skewer\base\section\Parameters;
use skewer\build\Tool;
use skewer\base\ui;
use skewer\base\site;
use skewer\build\Adm\GuestBook\ar;
use skewer\base\SysVar;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\components\i18n\ModulesParams;

/**
 * Проекция редактора баннеров для слайдера в панель управления
 * Class Module
 * @package skewer\build\Adm\Order
 */
class Module extends Adm\Tree\ModulePrototype implements Tool\LeftList\ModuleInterface {

    protected $sLanguageFilter = '';

    protected $iStatusFilter;
    /**
     * @var int id показываемого раздела
     */
    protected $iShowSection = 0;

    // число элементов на страницу
    protected $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    /** @var array Поля настроек */
    protected $aSettingsKeys =
        [
            'mail.title',
            'mail.content',
            'mail.onNotif',
            'mail.notifTitleNew',
            'mail.notifContentNew',
            'mail.notifTitleApprove',
            'mail.notifContentApprove',
            'mail.notifTitleReject',
            'mail.notifContentReject'
        ];

    /** @var array Поля настроек для каталога */
    protected $aSettingsCatalogKeys =
        [
            'mail.catalog.title',
            'mail.catalog.content',
            'mail.catalog.onNotif',
            'mail.catalog.notifTitleNew',
            'mail.catalog.notifContentNew',
            'mail.catalog.notifTitleApprove',
            'mail.catalog.notifContentApprove',
            'mail.catalog.notifTitleReject',
            'mail.catalog.notifContentReject'
        ];

    /**
     * @inheritDoc
     */
    public function init()
    {
        Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    /**
     * Проверяем установку параметра. Если нет, то не выводим
     * @return bool
     */
    function checkCatalogAccess(){
        return SysVar::get('catalog.guest_book_show');
    }


    function getName() {
        return $this->getModuleName();
    }

    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        $this->iStatusFilter = $this->get('filter_status', false );

        $sLanguage = \Yii::$app->language;
        if ($this->sectionId()){
            $sLanguage = Parameters::getLanguage($this->sectionId()) ?: $sLanguage;
        }

        $this->sLanguageFilter = $this->get('filter_language', $sLanguage);
    }

    /** Действие: Одобрание/отклонение отзыва */
    function actionChangeStatus(){
        $id =  $this->getInDataValInt('id');
        $iStatus = $this->getInDataValInt('status');

        /**
         * @var ar\GuestBookRow $row
         */
        $row =  ar\GuestBook::find($id);

        if ($row){
            Api::sendMailToClient( $row, $iStatus );

            $row->status = $iStatus;
            $row->save();

            $aStatulList = static::getStatusList();

            $aData = $row->getData();
            if (isset($aData['on_main'])) $aData['on_main'] = (int) $aData['on_main'];

            ui\StateBuilder::updRow($this, array_merge($aData, array('status' => $row->status), array('status_text' => $aStatulList[$row->status])));

        }
    }

    /** Состояние: Редактирование отзыва */
    function actionShow(){
        $iId = $this->getInDataValInt('id');
        /**
         * @var ar\GuestBookRow $row
         */
        $row = ar\GuestBook::find($iId);

        if (!$row){
            $row = ar\GuestBook::getNewRow();
            $row->parent_class = '';
            $row->date_time = date('Y-m-d H:i:s');
            $row->parent = $this->get('show_section');
        }

        $aItem = $row->getData();
        self::parseData($aItem);

        $bShowButtonApprove = false;
        $bShowButtonReject = false;

        switch($row->status){
            case ar\GuestBook::statusNew:
                $bShowButtonApprove = true;
                $bShowButtonReject  = true;
                break;
            case ar\GuestBook::statusApproved:
                $bShowButtonReject  = true;
                break;
            case ar\GuestBook::statusRejected:
                $bShowButtonApprove = true;
                break;
        }

        /**
         * @TODO так как виджет отключен для ExtForm и через addWidget это не сделать, добавил костыль
         */
        $aItem['status_text'] = $this::getStatusValue($aItem);

        $this->render(new Tool\Review\view\Show([
            "bShowRatingConfiguration" => ( ($row->rating) or (!$iParentSectionId = (int)$row->parent) or
                ($row->parent_class and Parameters::getValByName(\Yii::$app->sections->languageRoot(), 'Review', 'rating')) or
                (!$row->parent_class and Parameters::getValByName($iParentSectionId, 'Review', 'rating', true, true)) ),
            "bCheckCatalogAccess" => $this->checkCatalogAccess(),
            "bShowButtonApprove" => $bShowButtonApprove,
            "bShowButtonReject" => $bShowButtonReject,
            "iStatusApproved" => ar\GuestBook::statusApproved,
            "iStatusRejected" => ar\GuestBook::statusRejected,
            "aItem" => $aItem
        ]));

        return psComplete;
    }

    function actionSave(){

        // запросить данные
        $aData = $this->get('data');
        $iId = $this->getInDataValInt('id');
        $iStatus = $this->getInDataValInt('status_new', -1);

        if ($iStatus >= 0){
            $aData['status'] = $iStatus;
        }

        if (!$aData)
            throw new UserException('Empty data');

        if ($iId) {
            /**
             * @var ar\GuestBookRow $oRow
             */
            $oRow = ar\GuestBook::find($iId);

            if (!$oRow)
                throw new UserException("Запись [$iId] не найдена");

            Api::sendMailToClient( $oRow, $aData['status'] );

            $oRow->setData($aData);
        } else {
            $oRow = ar\GuestBook::getNewRow($aData);
            /**
             * @todo почему незаполненная дата прихотит как string 'null' ?
             */
            if ( !$oRow->date_time || $oRow->date_time === 'null')
                $oRow->date_time = date('Y-m-d H:i:s');
        }

        $oRow->save();
        $this->actionInit();
    }

    public static function getStatusList(){

        return [
            ar\GuestBook::statusNew      => \Yii::t('review', 'field_status_new'),
            ar\GuestBook::statusApproved => \Yii::t('review', 'field_status_approve'),
            ar\GuestBook::statusRejected => \Yii::t('review', 'field_status_reject'),
        ];
    }


    static function getStatusValue($oItem){
        $aStatulList = static::getStatusList();

        if (isset($aStatulList[$oItem['status']])){
            return $aStatulList[$oItem['status']];
        } else return '';
    }

    /** Состояние: список отзывов */
    function actionInit(){
        $iCount = 0;
        $aItems = ar\GuestBook::find();

        if ($this->iShowSection) {
            $aItems = $aItems
                ->where('parent',$this->iShowSection)
                ->where('parent_class','')
            ;
        }

        if ($this->iStatusFilter!==false){
            $aItems = $aItems->where('status',$this->iStatusFilter);
        }

        /**
         * @var StateSelect $aItems
         */
        $aItems = $aItems
            ->setCounterRef( $iCount )
            ->limit( $this->iOnPage, $this->iPage * $this->iOnPage)
            ->asArray()
            ->order('date_time',"DESC")
            ->getAll();

        foreach($aItems as &$aItem){
            static::parseData($aItem);
        }

        $this->render(new Tool\Review\view\Index([
            "bShowFieldType" => !$this->iShowSection && $this->checkCatalogAccess(),
            "bHasCatalogAndAccess" => site\Type::hasCatalogModule() && $this->checkCatalogAccess(),
            "aStatusList" => self::getStatusList(),
            "iStatusFilter" => $this->iStatusFilter,
            "bIsGuestBookModule" => (get_class($this)=='skewer\build\Adm\GuestBook\Module'),
            "iShowSection" => $this->iShowSection,
            "aItems" => $aItems,
            "iOnPage" => $this->iOnPage,
            "iPage" => $this->iPage,
            "iCount" => $iCount
        ]));

        return psComplete;
    }

    protected function actionSaveOnMain(){

        $iId = $this->getInDataValInt( 'id' );
        $oRow = ar\GuestBook::find($iId);
        /**
         * @var ar\GuestBookRow $oRow
         */
        if ( !$oRow )
            throw new UserException( "Запись [$iId] не найдена" );

        $oRow->on_main = $this->getInDataVal( 'on_main');
        $oRow->save();

        $this->actionInit();
    }

    private static function parseData(&$aItem){

        $aItem['on_main'] = (int)$aItem['on_main'];

        if ($aItem['parent_class'] == '' || $aItem['parent_class']=='0'){
            $path = Tree::getSectionAliasPath( $aItem['parent'] );
            $aItem['link'] = "<a target='_blank' href='".$path."'>".$path."</a>";
            $aItem['type'] = \Yii::t('review', 'field_type_section');
        }

        # todo проверить результат слияния
        if ($aItem['parent_class'] == Api::GoodReviews){
            $goods = GoodsSelector::get($aItem['parent'],1);//fixme #GET_CATALOG_GOODS
            if (isset($goods['url']) && $goods['url'])
                $aItem['link'] = "<a target='_blank' href='".$goods['url']."'>".$goods['url']."</a>";
            $aItem['type'] = \Yii::t('review', 'field_type_order');
        }
    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление. Внимание! Должен обязательно отработать метод \skewer\build\Adm\GuestBook\ar\GuestBookRow::preDelete
        if ( $iItemId and ($oReview = ar\GuestBook::find($iItemId)) )
            $oReview->delete();

        // вывод списка
        $this->actionInit();

    }

    /**
     * Форма настроек модуля
     */
    protected function actionSettings(){
        $aLanguages = array();
        if (!$this->sectionId()) {
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');
        }

        $aModulesData = ModulesParams::getByModule('review', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('review', 'head_mail_text',  [\Yii::t('app', 'site_label', [], $this->sLanguageFilter),
                \Yii::t('app', 'url_label', [], $this->sLanguageFilter),
                \Yii::t('review', 'label_user', [], $this->sLanguageFilter)
                , $this->sLanguageFilter]
        );

        foreach( $this->aSettingsKeys as  $key ){
            $aItems[$key] = (isset($aModulesData[$key]))?$aModulesData[$key]:'';
        }

        $this->render(new Tool\Review\view\Settings([
            "bNotSectionId" => !$this->sectionId(),
            "aLanguages" => $aLanguages,
            "sLanguageFilter" => $this->sLanguageFilter,
            "aItems" => $aItems
        ]));
    }

    /**
     * Форма настроек модуля для каталога
     */
    protected function actionSettingsCatalog(){
        $aLanguages = array();
        if (!$this->sectionId()){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');
        }

        $aModulesData = ModulesParams::getByModule('review', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('review', 'head_mail_text_catalog',  [\Yii::t('app', 'site_label', [], $this->sLanguageFilter),
                \Yii::t('app', 'url_label', [], $this->sLanguageFilter),
                \Yii::t('review', 'label_order', [], $this->sLanguageFilter),
                \Yii::t('review', 'label_user', [], $this->sLanguageFilter),
                $this->sLanguageFilter]
        );

        foreach( $this->aSettingsCatalogKeys as  $key ){
            $aItems[$key] = (isset($aModulesData[$key]))?$aModulesData[$key]:'';
        }

        $this->render(new Tool\Review\view\SettingsCatalog([
            "bNotSectionId" => !$this->sectionId(),
            "aLanguages" => $aLanguages,
            "sLanguageFilter" => $this->sLanguageFilter,
            "aItems" => $aItems
        ]));
    }

    /**
     * Сохраняем настройки формы
     */
    protected function actionSaveSettings(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->aSettingsKeys))
                    continue;

                ModulesParams::setParams( 'review', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

    protected function actionSaveSettingsCatalog(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->aSettingsCatalogKeys))
                    continue;

                ModulesParams::setParams( 'review', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData(ui\state\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'page' => $this->iPage
        ) );

    }

}