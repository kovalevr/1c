<?php

namespace skewer\build\Tool\FormOrders;


use skewer\components\forms;
use skewer\base\orm\Query;
use skewer\base\ui;
use skewer\build\Adm;
use skewer\build\Tool;
use skewer\components\ext;
use skewer\base\site_module;


class Module extends Tool\LeftList\ModulePrototype implements site_module\SectionModuleInterface {

    public $iCurrentForm = 0;

    /** @var int id раздела */
    protected $pageId = 0;

    /** @var int id формы */
    protected $formId = 0;

    function sectionId() {
        return $this->pageId;
    }

    /**
     * Сообщает используется ли только одна форма для отображения
     * @return bool
     */
    private function useOneForm() {
        return (bool)$this->formId;
    }

    protected function preExecute() {

        $this->iCurrentForm = $this->getInt('form_id');
        if ( !$this->iCurrentForm )
            $this->iCurrentForm = $this->getEnvParam('form_id');
    }


    protected function setServiceData(ui\state\BaseInterface $oIface ) {

        $oIface->setServiceData( array(
            'form_id' => $this->iCurrentForm,
        ) );
    }


    protected function actionInit() {

        if ( $this->useOneForm() ) {
            $this->iCurrentForm = $this->formId;
            $this->actionList();
        } else {
            $this->actionShowForms();
        }

    }

    /**
     * Список форм с заказами
     */
    protected function actionShowForms() {

        // список форм с сохранением в базу
        $aItems = forms\Table::find()->where( 'form_handler_type', 'toBase' )->getAll();

        $this->iCurrentForm = 0;

        $this->setPanelName( \Yii::t( 'forms', 'form_list') );

        $this->render(new Tool\FormOrders\view\ShowForms([
            "aItems" => $aItems
        ]));
    }


    /**
     * Список заказов
     */
    protected function actionList() {

        // -- input data
        $aData = $this->getInData();
        $this->iCurrentForm = isSet( $aData['form_id'] ) ? $aData['form_id'] : $this->iCurrentForm;
        if ( !$this->iCurrentForm )
           throw new \Exception( 'form not found' );

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find($this->iCurrentForm);

        $sErrorMsg = "";
        if (!$oFormRow){
            $sErrorMsg = \Yii::t('forms', "not_found");
        }

        if ($oFormRow->form_handler_type != "toBase"){
            $sErrorMsg = \Yii::t('forms', "bad_status");
        }

        if ($sErrorMsg){
            $this->render(new Tool\FormOrders\view\Error([
                "sErrorMsg" => $sErrorMsg
            ]));
            return;
        }

        $aItems = Query::SelectFrom( 'frm_' . $oFormRow->form_name )->getAll();
        $aFieldList = $oFormRow->getFields();

        $this->setPanelName(  \Yii::t( 'forms', 'form_list') );

        $iFieldCounter = 0;
        $aThreeFieldList = array();
        foreach ( $aFieldList as $oFieldRow ) {
            /*Если поле "Файл" Пропустим его*/
            if ($oFieldRow->param_type=='6') continue;

            $iFieldCounter++;
            if ( $iFieldCounter > 3 ) break;

            $aThreeFieldList[] = $oFieldRow;
        }

        $this->render(new Tool\FormOrders\view\ListForm([
            "aFieldList" => $aThreeFieldList,
            "aItems" => $aItems,
            "bNotUseOneForm" => !$this->useOneForm()
        ]));
    }



    protected function actionEdit() {

        // -- обработка данных
        $aData = $this->getInData();
        $iItemId = isSet( $aData['id'] ) ? $aData['id'] : 0;
        if ( !$this->iCurrentForm )
            throw new \Exception( 'item not found' );

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );
        $aFieldList = $oFormRow->getFields();

        if ( $iItemId )
            $aItems = Query::SelectFrom( 'frm_' . $oFormRow->form_name )->where( 'id', $iItemId )->getOne();
        else
            $aItems = array();


        // -- сборка интерфейса

        // создаем форму
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->field('id', 'id', 'hide');


        foreach ( $aFieldList as $oFieldRow ) {

            $sCurType = $oFieldRow->getType4ExtJS();
            switch ( $sCurType ) {
                case 'select':
                    $oFormBuilder->fieldSelect( $oFieldRow->param_name, $oFieldRow->param_title, $oFieldRow->parseDefaultAsList() );
                    break;
                case 'radio':
                    $oFormBuilder->fieldSelect( $oFieldRow->param_name, $oFieldRow->param_title, $oFieldRow->parseDefaultAsList() );
                    break;
                default:
                    /*Если поле "Файл" Пропустим его*/
                    if ($oFieldRow->param_type=='6') continue;
                    $oFormBuilder->field($oFieldRow->param_name, $oFieldRow->param_title, $sCurType);

            }
        }

        $oFormBuilder
            ->field('__add_date', \Yii::t('forms', 'add_date'), 'string', array('disabled' => true))
            ->field('__section', \Yii::t('forms', 'section'), 'string', array('disabled' => true))
            ->fieldSelect( '__status', \Yii::t('forms', 'status'), Api::getStatusList() );

        // устанавливаем значения
        $oFormBuilder->setValue( $aItems );

        // добавляем элементы управления
        $oFormBuilder
            ->buttonSave('save')
            ->buttonCancel('list');

        if ( $this->iCurrentForm ) {
            $oFormBuilder
                ->buttonSeparator( '->' )
                ->buttonDelete();
        }

        $this->render(new Tool\FormOrders\view\Edit([
            "aFieldList" => $aFieldList,
            "aStatusList" => Api::getStatusList(),
            "aItems" => $aItems,
            "bCanDelete" => $iItemId
        ]));
        return psComplete;
    }


    protected function actionDelete() {

        // -- обработка данных
        $aData = $this->getInData();
        $iItemId = isSet( $aData['id'] ) ? $aData['id'] : 0;
        if ( !$iItemId || !$this->iCurrentForm )
            throw new \Exception( 'item not found' );

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );

        Query::DeleteFrom( 'frm_' . $oFormRow->form_name )->where( 'id', $iItemId )->get();

        // вывод списка
        $this->actionList();
    }


    /**
     * Сохранение заказы
     */
    protected function actionSave() {

        // -- обработка данных
        $aData = $this->getInData();
        $iItemId = isSet( $aData['id'] ) ? $aData['id'] : 0;
        if ( !$this->iCurrentForm )
            throw new \Exception( 'item not found' );

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );
        $aFields = $oFormRow->getFields();

        if ( !$iItemId )
            $oQuery = Query::InsertInto( 'frm_' . $oFormRow->form_name )
                ->set( '__status', 'new' )
                ->set( '__add_date', date('Y-m-d') );
        else
            $oQuery = Query::UpdateFrom( 'frm_' . $oFormRow->form_name )
                ->where( 'id', $iItemId )
                ->set( '__status', isSet( $aData['__status'] ) ? $aData['__status'] : 'new' );

        foreach ( $aFields as $oFieldRow ) {
            $sName = $oFieldRow->param_name;
            $oQuery->set( $sName, isSet($aData[$sName]) ? $aData[$sName] : $oFieldRow->param_default );
        }

        $oQuery->get();


        // вывод списка
        $this->actionList();

    }

}