<?php

namespace skewer\build\Tool\Subscribe;

use skewer\base\section\models\ParamsAr;
use skewer\base\section\models\TreeSection;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\base\section\Visible;
use skewer\components\config\InstallPrototype;
use skewer\components\i18n\models\ServiceSections;
use skewer\components\i18n\ModulesParams;
use skewer\components\seo\Data;

class Install extends InstallPrototype {

    public function init() {
        /**
         * @todo section!
         */

        return true;
    }// func

    public function install() {

        /*Создание разделов рассылки*/
        $aSections = \Yii::$app->sections->getValues('tools');
        $aTpls = \Yii::$app->sections->getValues('tplNew');
        foreach ($aSections as $lang=>$section){

            /*Создадим раздел*/
            $oSubscribeSection = Tree::addSection($section,\Yii::t('app','subscribe'),$aTpls[$lang],'',Visible::HIDDEN_FROM_MENU);

            /*Допишем в object Subscribe*/
            $oParam = new ParamsAr();
            $oParam->group = 'content';
            $oParam->name = 'object';
            $oParam->value = 'Subscribe';
            $oParam->parent = $oSubscribeSection->id;
            $oParam->save(false);

            /*Добавим в ServiceSections*/
            $oService = new ServiceSections();
            $oService->name = 'subscribe';
            $oService->value = $oSubscribeSection->id;
            $oService->language = $lang;
            $oService->title = \Yii::t('app','subscribe','ru');
            $oService->save();

            /*Закроем от индексации*/
            \Yii::$app->db
                ->createCommand("INSERT INTO `seo_data`(`group`,`row_id`,`none_index`,`none_search`) VALUES('section','$oSubscribeSection->id','1','1')")
                ->execute();
        };

        return true;
    }// func

    public function uninstall() {

        /*Удалим разделы с рассылкой*/
        $aSections = \Yii::$app->sections->getValues('subscribe');

        foreach ($aSections as $item){
            /*удалим раздел*/
            Tree::removeSection($item);
            /*удалим его из ServiceSections*/
            ServiceSections::deleteAll(['value'=>$item]);
        }

        return true;
    }// func

}//class