<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 27.01.2017
 * Time: 12:30
 */

namespace skewer\build\Tool\Subscribe\view;

use skewer\components\ext\view\FormView;

class SettingsForm extends FormView
{
    public $aModes;
    public $aSubscribeMode;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->fieldSelect('mode',\Yii::t('subscribe', 'subscribe_mode'),$this->aModes,[],false)
            ->buttonSave('saveSettings')
            ->buttonCancel('users')
            ->setValue($this->aSubscribeMode);
    }
}