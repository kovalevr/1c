<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 16.01.2017
 * Time: 11:32
 */

namespace skewer\build\Tool\Crm\view;

use skewer\components\ext\view\FormView;

class Index extends FormView
{
    public $sToken;
    public $sEmail;
    public $aValues;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('token', $this->sToken, 'str')
            ->field('email', $this->sEmail, 'str')
            ->setValue($this->aValues)
            ->buttonSave('save')
        ;
    }
}