<?php

namespace skewer\build\Tool\Crm;


use skewer\base\ui;
use skewer\build\Tool;
use skewer\base\SysVar;
use yii\helpers\ArrayHelper;


class Module extends Tool\LeftList\ModulePrototype  {

    protected function actionInit() {
        $values['token'] = SysVar::get('crm_token');
        $values['email'] = SysVar::get('crm_email');

        $this->render(new Tool\Crm\view\Index([
            "sToken" => \Yii::t('crm', 'token'),
            "sEmail" => \Yii::t('crm', 'email'),
            "aValues" => $values
        ]));

        return psComplete;
    }

    protected function actionSave(){
        try {
            $aData = $this->getInData();
            SysVar::set('crm_token', ArrayHelper::getValue($aData,'token',''));
            SysVar::set('crm_email', ArrayHelper::getValue($aData,'email',''));
            $this->actionInit();
        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }
    }
}