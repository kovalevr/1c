/**
 * Специфическое поле типа набор галочек
 *
 */

Ext.define('Ext.Tool.CheckSet4Module',{
    extend: 'Ext.form.FieldSet',
    defaultType: 'checkbox',
    layout: 'anchor',
    items: [],
    fieldNames: [],

    initComponent: function(){

        var me = this,
            item,itemId,
            newItems,
            newItem,
            fieldName = me.getFieldName()
            ;

        newItems = [];

        // перебор элементов в группе
        for ( itemId in me['items'] ) {

            // инициализация элемента
            item = me['items'][itemId];

            // составление описания для элемента
            newItem = item;
            newItem.xtype = 'checkbox';
            newItem.name = fieldName+'.'+item.name;
            newItem.boxLabel = item.title || item.name;
            newItem.checked = Boolean(parseInt(item.value));
            newItem.uncheckedValue = 0;
            newItem.inputValue = 1;

            // добавление имени поля в список
            me.fieldNames.push( newItem.name );

            newItems.push( newItem );

        }

        me.items = newItems;

        me.callParent();

    },

    getFieldName: function() {
        return this.name;
    }

});
