<?php

/* main */
use skewer\base\site\Layer;

$aConfig['name']     = 'Maps';
$aConfig['version']  = '1.0';
$aConfig['title']    = 'Карты';
$aConfig['description']  = 'Модуль карты';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::TOOL;
$aConfig['group']     = \skewer\build\Tool\LeftList\Group::CONTENT;
$aConfig['languageCategory'] = 'Maps';
$aConfig['useNamespace'] = true;


return $aConfig;
