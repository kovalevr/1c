<?php

namespace skewer\build\Tool\Redirect301;

use skewer\base\ui\Api;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\components\redirect\models\Redirect;
use yii\base\Exception;
use yii\base\UserException;

class Module extends Tool\LeftList\ModulePrototype {

    protected function preExecute() {

    }

    protected function actionInit() {

        $this->actionList();

    }

    /**
     * Список редиректов
     */
    protected function actionList() {
        $this->setPanelName( \Yii::t('redirect301', 'urlList') );

        $this->render(new Tool\Redirect301\view\Index([
            "aRedirects" => Redirect::find()->orderBy('priority')->asArray()->all()
        ]));
    }

    /**
     * Сортировка редиректов
     */
    protected function actionSortRedirects() {

        $aItemDrop   = $this->get('data');
        $aItemTarget = $this->get('dropData');
        $sOrderType  = $this->get('position');

        if ($aItemDrop and $aItemTarget and $sOrderType)
            Api::sortObjects($aItemDrop['id'],$aItemTarget['id'],new Redirect(),$sOrderType);

        $this->actionList();

    }

    /**
     * Показ формы добавления
     * @param array $aData
     */
    public function actionAddForm($aData = []){
        $this->setPanelName(\Yii::t('redirect301', 'newRedirect'));
        $this->render(new Tool\Redirect301\view\AddForm([
            "bNotNewItem" => (isset($aData['id']) and $aData['id']),
            "aData" => (empty($aData) ? new Redirect() : $aData)
        ]));
    }

    /**
     * Добавление редиректа
     */
    public function actionAdd(){

        $aData = $this->get('data');

        unset($aData['input_url']);

        if ((substr($aData['old_url'],-1)!=='/') and (substr($aData['old_url'],-1)!=='$'))
            $aData['old_url'] = $aData['old_url'].'/';

        $aData['old_url'] = str_replace('http://','',$aData['old_url']);
        $aData['old_url'] = str_replace('https://','',$aData['old_url']);
        $aData['old_url'] = str_replace($_SERVER['HTTP_HOST'],'',$aData['old_url']);

        try {
            \skewer\components\redirect\Api::checkRule($aData['old_url'],$aData['new_url'],'/test');
            $redirect301 = new Redirect();

            $redirect301->setAttributes($aData);
            if ( !$redirect301->save(false) )
                throw new \Exception('Ошибка: правило не добавлено!');

            // переход к списку
            $this->actionList();
        } catch (Exception $e){
            throw new UserException(
                \Yii::t('redirect301','validationError', ['message'=>$e->getMessage()]),
                0,
                $e
            );

        }

    }

    /**
     * Запуск тестирования по всем редиректам.
     * @param array $aData
     */
    public function actionTestAll($aData = []){
        $this->setPanelName(\Yii::t('redirect301', 'newRedirect'));
        $this->render(new Tool\Redirect301\view\TestAll([
            "aData" => (empty($aData) ? ['input_url'=>\skewer\components\redirect\Api::getTestUrls()] : $aData)
        ]));
    }

    /**
     * Запуск тестирования по 1 правилу
     */
    public function actionTest(){

        $aData = $this->get('data');

        if ((isset($aData['old_url'])) and (isset($aData['new_url']))) {

            try {
                \skewer\components\redirect\Api::checkRule($aData['old_url'],$aData['new_url'],'/test');
                $aOut = [];
                $aInputUrls = explode("\n", $aData['input_url']);
                foreach ($aInputUrls as $key=>$item){
                    $aOut['items'][$key]['old'] = $item;
                    $aOut['items'][$key]['new'] = \skewer\components\redirect\Api::checkRule($aData['old_url'],$aData['new_url'],$item);
                    if (!$aOut['items'][$key]['new']) $aOut['items'][$key]['new'] = \Yii::t('redirect301','no_redirect');
                }

                $aData['test_results'] = \Yii::$app->getView()->renderFile(\skewer\components\redirect\Api::getDir().'/template/test_redirect.php',$aOut);
            } catch (Exception $e){
                throw new UserException(
                    \Yii::t('redirect301','validationError', ['message'=>$e->getMessage()]),
                    0,
                    $e
                );

            }

            $this->actionAddForm($aData);

        }else{
            $aData['test_results'] = \skewer\components\redirect\Api::testUrls(explode("\n",$aData['input_url']));
            $this->actionTestAll($aData);
        }

    }

    /**
     * Удаление записи
     */
    public function actionDelete(){

        try {

            $aData = $this->get('data');

            if ( !Redirect::deleteAll(['id'=>$aData['id']]) )
                throw new \Exception('Ошибка: не удалось удалить правило!');


        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

    }

    /**
     * Отрисовка формы редактирования
     * @param array $aData
     */
    public function actionEditForm($aData = []){
        if (empty($aData))
            $aData = $this->get('data');

        $this->setPanelName(\Yii::t('redirect301', 'editUrl'));

        $this->render(new Tool\Redirect301\view\EditForm([
            "aData" => ((count($aData) > 2) ?$aData : Redirect::findOne(['id' => $aData['id']]))
        ]));
    }

    /**
     * Обновление записи
     */
    public function actionUpdate(){

        $aData = $this->get('data');

        if ((substr($aData['old_url'],-1)!=='/') and (substr($aData['old_url'],-1)!=='$'))
            $aData['old_url'] = $aData['old_url'].'/';

        $aData['old_url'] = str_replace('http://','',$aData['old_url']);
        $aData['old_url'] = str_replace('https://','',$aData['old_url']);
        $aData['old_url'] = str_replace($_SERVER['HTTP_HOST'],'',$aData['old_url']);

        try {
            \skewer\components\redirect\Api::checkRule($aData['old_url'],$aData['new_url'],'/test');
            /** @var Redirect $redirect301 */
            if($redirect301 = Redirect::findOne(['id'=>$aData['id']])) {
                $redirect301->setAttributes($aData);
                if (!$redirect301->save())
                    throw new \Exception('Ошибка: правило не было изменено!');
            }
            $this->actionList();

        } catch (Exception $e){
            throw new UserException(
                \Yii::t('redirect301','validationError', ['message'=>$e->getMessage()]),
                0,
                $e
            );

        }

    }

}
