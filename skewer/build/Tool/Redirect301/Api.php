<?php

namespace skewer\build\Tool\Redirect301;

use skewer\components\config\UpdateException;
use skewer\components\config\UpdateHelper;

class Api {

    /**
     * Функция пересоздает файл .htaccess в корне
     * todo должна находиться не здесь и должна быть полностью пересмотрена
     * раньше использовалась для добавления 301 редиректов напрямую в .htaccess,
     *   теперь это делается через отдельный файл
     * @return bool
     * @throws UpdateException
     */
    public static function makeHtaccessFile(){

        /* Массив меток, подставляемых в шаблон htaccess */
        $aDomainItems = \skewer\build\Tool\Domains\Api::getRedirectItems();

        $oUpHalper = new UpdateHelper();
        $aData = array('redirectItems'=>array(),
            'buildName'	  => BUILDNAME,
            'buildNumber' => BUILDNUMBER,
            'inCluster'=>INCLUSTER,
            'USECLUSTERBUILD'=>USECLUSTERBUILD,
        );

        foreach($aDomainItems as $aItem)
            $aData['redirectDomainItems'][] = $aItem;

        /* rewrite htaccess */

        $oUpHalper->updateHtaccess(BUILDPATH.'common/templates/',$aData);

        return true;
    }
}
