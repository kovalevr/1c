<?php

namespace skewer\build\Tool\SeoGen;

use skewer\base\queue;
use skewer\base\section\models\TreeSection;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\base\section\Visible;
use skewer\base\site\Site;
use skewer\build\Adm\FAQ;
use skewer\build\Adm\News;
use skewer\build\Adm\Tree\Search;
use skewer\build\Catalog\Goods\SeoGood;
use skewer\build\Catalog\Goods\SeoGoodModifications;
use skewer\build\Adm\Gallery;
use skewer\build\Page\Main;
use skewer\components\import;
use skewer\components\import\ar\ImportTemplate;
use skewer\components\import\ar\ImportTemplateRow;
use skewer\components\import\Config;
use skewer\components\import\Task;
use skewer\components\seo;
use skewer\helpers\ImageResize;
use skewer\helpers\Transliterate;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * Задача импортирования разделов и их seo данных
 * Class Task
 */
class ImportTask extends queue\Task{

    /** @var string Режим работы импорта - создание разделов */
    public static $sCreateMode = 'create';

    /** @var string Режим работы импорта - обновление разделов */
    public static $sUpdateMode = 'update';

    /** @var \skewer\components\import\Logger */
    public $oLogger = null;

    /** @var Config */
    public $oConfig = null;

    /** @var XlsWriter */
    public $oProvider = null;


    public function init(){
        $aArgs = func_get_args();

        $sFilePath   = ArrayHelper::getValue($aArgs[0], 'file', '');

        $oTemplate = self::getImportTemplate($sFilePath);

        $this->oConfig = new Config($oTemplate);
        $this->oConfig->setParam('skip_row', 1);
        $this->oProvider = import\Api::getProvider($this->oConfig);

        $this->oLogger = new import\Logger($this->getId(), $oTemplate->id);

        $this->oLogger->setParam( 'start', date('Y-m-d H:i:s'));
        $this->oLogger->setParam( 'newRecords', 0 );
        $this->oLogger->setParam( 'updateRecords', 0 );
        $this->oProvider->setConfigVal('mode', ArrayHelper::getValue($aArgs, '0.mode', ''));
        $this->oProvider->setConfigVal('file', ArrayHelper::getValue($aArgs, '0.file', ''));

        $sDataType = ArrayHelper::getValue($aArgs, '0.data_type', '');
        $this->oProvider->setConfigVal('sDataType', $sDataType);

        switch ($sDataType){
            case Api::DATATYPE_SECTIONS:
                $this->oProvider->setConfigVal('sectionId', ArrayHelper::getValue($aArgs, '0.sectionId', ''));
                $this->oProvider->setConfigVal('sEnableStaticContents', ArrayHelper::getValue($aArgs, '0.enable_staticContents', ''));
                break;
            case Api::DATATYPE_GOODS:
                $this->oProvider->setConfigVal('sCatalogSections', ArrayHelper::getValue($aArgs, '0.catalog_sections', ''));
                break;
        }

        return true;
    }


    public function recovery(){

        $aArgs = func_get_args();

        if (!isset($aArgs[0]['data']))
            throw new \Exception('no valid data');

        $aData = json_decode($aArgs[0]['data'], true);

        $oTemplate = self::getImportTemplate($aData['file']);

        $this->oConfig = new Config($oTemplate);
        $this->oConfig->setParam('skip_row', 1);
        $this->oConfig->setData($aData);

        $this->oProvider = import\Api::getProvider($this->oConfig);
        $this->oLogger = new import\Logger($this->getId(), $oTemplate->id);

    }


    /** Заморозка задачи */
    public function reservation(){
        $this->setParams(['data' => $this->oConfig->getJsonData()]);
        $this->oLogger->setParam( 'status', static::stFrozen );
        $this->oLogger->save();
    }



    public function beforeExecute(){
        $this->oProvider->beforeExecute();
    }


    public function execute(){
        /** Если провайдер не разрешает читать - прерываемся */
        if (!$this->oProvider->canRead()){
            $this->setStatus(static::stInterapt);
            return false;
        }


        /** Получение данных */
        $aBuffer = $this->oProvider->getRow();

        /** Данных нет - завершаем импорт */
        if ( $aBuffer === false ){
            $this->setStatus( static::stComplete );
            $this->oConfig->setParam( 'importStatus', Task::importFinish );
            return true;
        }

        if ($aBuffer = $this->validateData($aBuffer))
            $this->saveRow($aBuffer);

        return true;

    }


    /** Набор полей, определяющий шаблон файла импорта
     *  Одинаковый для режима обновления и добавления
     * @param $sMode - режим импорта
     * @return array
     */
    public static function getFields($sMode){
        switch ($sMode){
            case self::$sCreateMode:
                return ['parent', 'template', 'name', 'h1', 'title', 'description', 'keywords', 'staticContent', 'staticContent2'];
            case self::$sUpdateMode:
                return ['type', 'url', 'h1', 'title', 'description', 'keywords', 'staticContent', 'staticContent2'];
            default:
                return [];
        }
    }


    public function saveRow($aBuffer){

        $sMode = $this->oProvider->getConfigVal('mode');
        $sDataType = $this->oProvider->getConfigVal('sDataType');

        if ($sDataType == Api::DATATYPE_GOODS){
            $iSectionId = Tree::getSectionByPath( self::getRequestUriFromAbsoluteUrl( $aBuffer['url'] ) );
            $aCatalogSections = StringHelper::explode($this->oProvider->getConfigVal('sCatalogSections', ''), ',', true, true);

            if ( array_search('all', $aCatalogSections) === false )
                if ( !in_array($iSectionId, $aCatalogSections) )
                    return ;
        }

        $aErrors = [];

        switch ($sMode){
            case self::$sUpdateMode:

                $this->updateRecord($aBuffer, $aErrors);

                if (!$aErrors)
                    $this->oLogger->incParam( 'updateRecords');
                else
                    $this->oLogger->setListParam('notUpdated', "Строка №". ($this->oProvider->getConfigVal('row') - 1) . '  ' . array_shift($aErrors) );

                break;

            case self::$sCreateMode:

                $this->createSection($aBuffer, $aErrors);

                if (!$aErrors)
                    $this->oLogger->incParam( 'newRecords');
                else
                    $this->oLogger->setListParam('notAdded', "Строка №". ($this->oProvider->getConfigVal('row') - 1) . '  ' . array_shift($aErrors) );

        }

    }


    public function complete(){
        $this->oLogger->setParam( 'finish', date('Y-m-d H:i:s'));
        $this->oLogger->save();

        // Ставим задачу на обновление Sitemap
        seo\Service::makeSiteMap();
    }


    /** Обновление записи
     * @param $aBuffer - массив с данными
     * @param $aError  - массив ошибок
     * @return bool
     */
    private function updateRecord($aBuffer, &$aError){

        $sAliasRecord = self::getRequestUriFromAbsoluteUrl( $aBuffer['url'] );

        try{

            if ( ($iRecordId = self::doExistRecord($aBuffer['type'],$sAliasRecord)) === false )
                throw new \Exception('Запись не существует');

            $iSectionId = Tree::getSectionByPath($sAliasRecord);

            self::updateEntity($aBuffer['type'], $iRecordId, $iSectionId, $aBuffer);

        }catch ( \Exception $e ){
            $aError[] = $e->getMessage();
        }
    }


    /** Сохранение одной записи(в режиме создания разделов)
     * @param $aBuffer - массив с данными
     * @return bool
     */
    private function createSection($aBuffer, &$aErrors){

        $iRow = $this->oProvider->getConfigVal('row') - 1;

        // Транслитерируем название и получаем alias
        //todo Забрал из TreeSection::save(). Нужен метод
        $sTitleSection = mb_substr( $aBuffer['name'], 0, 100 );
        $sAlias = Transliterate::generateAlias( $sTitleSection );
        $sAlias = mb_substr( $sAlias, 0, 60 );

        try{

            /** Проверка если ли уже раздел с таким alias(!!!Не урлом)*/
            if ($aSection = TreeSection::findOne(['alias' => $sAlias]))
                throw new \Exception( sprintf( "%s - Раздел с alias [ %s ] уже существует", $aBuffer['name'], $sAlias) );

            if ($aBuffer['parent']){
                if ( ($iParentSection = $this->getSystemSectionId($aBuffer['parent'])) == false )
                    throw new \Exception( sprintf("%s - Не найден родительский раздел", $aBuffer['name']) );
            }
            else
                $iParentSection = $this->oProvider->getConfigVal('sectionId');


            // Добавляем раздел
            $oNewSection = new TreeSection([
                'title' => $sTitleSection,
                'alias' => $sAlias,
                'parent' => $iParentSection,
                'visible' => Visible::VISIBLE
            ]);

            if (!$oNewSection->save())
                throw new \Exception( sprintf("%s - Ошибки валидации при сохранении раздела",$aBuffer['name']) );

            $iTemplateId = (!empty($aBuffer['template'])) ? $aBuffer['template'] : \Yii::$app->sections->tplNew();
            $bSuccess = $oNewSection->setTemplate($iTemplateId);

            if (!$bSuccess)
                throw new \Exception( sprintf("%s - шаблон [%d] несуществует", $aBuffer['name'], $iTemplateId) );

            // сохраняем связку [id из файла => id в cms]
            $this->addLinkSections($iRow, $oNewSection->id);

            // Пишем данные в раздел
            self::updateEntity(Main\Seo::className(), $oNewSection->id, $oNewSection->id, $aBuffer);

        }catch( \Exception $e){
            $aErrors[] = $e->getMessage();
        }

    }


    /**
     * Отдаст id раздела в CMS по id разделу указанному в файле
     * @param $iSectionIdFromFile
     * @return bool Вернет false если соответствующий id не найден
     */
    private function getSystemSectionId($iSectionIdFromFile){

        $aArrayConnectionIds = $this->oProvider->getConfigVal('arrayConnectionIds',[]);

        return isset($aArrayConnectionIds[$iSectionIdFromFile])
            ? $aArrayConnectionIds[$iSectionIdFromFile]
            : false;
    }


    /** Добавить связку id раздела в файле => id раздела в CMS
     * @param int $iIdFromFile - id раздела в файле
     * @param int $iSystemId - id раздела в CMS
     */
    private function addLinkSections($iIdFromFile, $iSystemId){
        $aArrayConnectionIds = $this->oProvider->getConfigVal('arrayConnectionIds',[]);
        $aArrayConnectionIds[$iIdFromFile] = $iSystemId;
        $this->oProvider->setConfigVal('arrayConnectionIds',$aArrayConnectionIds);
    }



    /** Имя класса */
    public static function className(){
        return get_called_class();
    }

    /**
     * Шаблон экспорта. Нужен для использования Logger и Config
     * @param $sFile - Имя файла
     * @return \skewer\base\orm\ActiveRecord|ImportTemplateRow
     */
    public static function getImportTemplate($sFile){
        return ImportTemplate::getNewRow([
            'title'         => 'Шаблон импорта seo данных',
            'type'          => import\Api::Type_File,
            'provider_type' => import\Api::ptXLS,
            'source'        => $sFile
        ]);
    }

    /**
     * Возвращает конфиг текущей задачи
     * @return array
     */
    public static function getConfig($aParams = []){

        return array(
            'title'         => 'Импорт разделов(seo)',
            'name'          => 'importSeo',
            'class'         => self::className(),
            'parameters'    => $aParams,
            'priority'      => ImportTask::priorityLow,
            'resource_use'  => ImportTask::weightLow,
            'target_area'   => 1, // область применения - площадка
        );
    }

    /**
     * Режимы выполнения импорта
     * @return array
     */
    public static function getModesExecute(){
        return array(
            self::$sCreateMode => 'Создание',
            self::$sUpdateMode => 'Обновление'
        );
    }


    /**
     * Метод проверки существования записи.
     * Вернёт id записи или false если запись не найдена
     * @param $sEntityType   - тип сущности
     * @param $sAliasRecord  - alias записи
     * @return bool|int
     */
    public static function doExistRecord($sEntityType, $sAliasRecord){

        /** @var seo\SeoPrototype $oSeo */
        if ( !class_exists($sEntityType) || !(($oSeo = new $sEntityType()) instanceof seo\SeoPrototype) )
            return false;

        return $oSeo->doExistRecord($sAliasRecord);

    }


    /**
     * Метод обновления данных сущности
     * @param $iEntityType - тип сущности
     * @param $iEntityId   - id сущности
     * @param $iSectionId  - id раздела
     * @param $aData       - данные
     * @return bool        - true - успешное обновление, false - запись не обновлена
     */
    public static function updateEntity($iEntityType, $iEntityId, $iSectionId, $aData){

        switch ($iEntityType){

            case Main\Seo::className():

                if (isset($aData['h1'])){
                    if ($oH1Param = Parameters::getByName($iSectionId, 'title', 'altTitle')){
                        $oH1Param->value = $aData['h1'];
                        $oH1Param->save();
                    }
                }

                if (isset($aData['staticContent'])){
                    if ($oStContentParam = Parameters::getByName($iSectionId, 'staticContent', 'source')){
                        $sText = html_entity_decode($aData['staticContent']);
                        $oStContentParam->show_val = ImageResize::wrapTags($sText, $iSectionId);
                        $oStContentParam->save();
                    }
                }

                if (isset($aData['staticContent2'])){
                    if ($oStContentParam = Parameters::getByName($iSectionId, 'staticContent2', 'source')){
                        $sText = html_entity_decode($aData['staticContent2']);
                        $oStContentParam->show_val = ImageResize::wrapTags($sText, $iSectionId);
                        $oStContentParam->save();
                    }
                }

                $oSearchComponent = new Search();
                $oSearchComponent->updateByObjectId($iSectionId, false);

                break;

        }

        if (!class_exists($iEntityType))
            return false;

        /** @var seo\SeoPrototype $oSeo */
        $oSeo = new $iEntityType();
        if ( !($oSeo instanceof seo\SeoPrototype) )
            return false;

        $oSeo->setEntityId($iEntityId);
        $oSeo->setSectionId($iSectionId);
        $oSeo->loadDataEntity();
        $oSeo->initSeoData();


        $aSeoData = [];
        foreach (seo\SeoPrototype::getField4Parsing() as $item) {
            if ( isset($aData[$item]) && (seo\Api::prepareRawString($oSeo->parseField($item, ['sectionId' => $iSectionId])) !== seo\Api::prepareRawString($aData[$item])) )
                $aSeoData[$item] = seo\Api::prepareRawString($aData[$item]);
        }

        seo\Api::set($oSeo::getGroup(), $iEntityId, $iSectionId, $aSeoData);

        return true;

    }


    /**
     * Проверяет корректность считанных данных.
     * @param $aData
     * @return array|bool Вернет преобразованный к нужному ввиду массив или false если данные некорректны
     */
    public function validateData($aData){

        $sDataType = $this->oProvider->getConfigVal('sDataType', '');
        $sMode     = $this->oProvider->getConfigVal('mode', '');
        $aAvailableEntities = [];

        $aData = array_slice($aData,0,count(self::getFields($sMode)));
        $aData = array_combine(self::getFields($sMode), $aData);

        // Удаляем те поля из буффера, которые не будут обновляться
        if ($sDataType == Api::DATATYPE_SECTIONS){
            if (!$this->oProvider->getConfigVal('sEnableStaticContents', false)){
                unset($aData['staticContent']);
                unset($aData['staticContent2']);
            }

        }

        if ( ($sDataType == Api::DATATYPE_SECTIONS) && ($sMode == self::$sCreateMode) )
            $sTypeEntity = Main\Seo::className();
        else{
            $sAliasEntity = trim($aData['type']);
            if ( ($sTypeEntity = array_search($sAliasEntity, Api::getEntities())) === false )
                return false;
        }

        $aData['type'] = $sTypeEntity;

        if ( ($sMode == self::$sUpdateMode) && !$aData['url'] )
            return false;

        switch ($sDataType){
            case Api::DATATYPE_SECTIONS:

                $aAvailableEntities = ($sMode == self::$sUpdateMode)
                    ? [Main\Seo::className(), \skewer\build\Adm\Articles\Seo::className(), News\Seo::className(), Gallery\Seo::className(), FAQ\Seo::className() ]
                    : $aAvailableEntities = [Main\Seo::className()];

                break;
            case Api::DATATYPE_GOODS:

                $aAvailableEntities = ($sMode == self::$sUpdateMode)? [SeoGood::className(), SeoGoodModifications::className()] : [];

                break;
        }


        // Если нельзя работать с данным типом сущности
        if ( !in_array($aData['type'], $aAvailableEntities) )
            return false;


        return $aData;
    }


    /**
     * Возвращает обработанный относительный url
     * @param string $sUrl - урл
     * @return mixed
     */
    private static function getRequestUriFromAbsoluteUrl( $sUrl ){
        $sUrl = preg_replace('/^(http|https):\/\/[^\/]+/i', '', $sUrl );
        $sUrl = trim($sUrl, '/');
        return '/' . $sUrl . '/';
    }


}