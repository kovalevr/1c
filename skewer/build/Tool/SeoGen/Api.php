<?php

namespace skewer\build\Tool\SeoGen;

use skewer\base\queue\Manager;
use skewer\base\site\Type;
use skewer\build\Catalog\Goods\SeoGood;
use skewer\build\Catalog\Goods\SeoGoodModifications;
use skewer\build\Page\Main\Seo;
use skewer\build\Adm\Articles;
use skewer\build\Adm\News;
use skewer\build\Adm\Gallery;
use skewer\build\Adm\FAQ;


class Api{

    /** @const Тип данных - товары  */
    const DATATYPE_GOODS = 'goods';

    /** @const Тип данных - разделы */
    const DATATYPE_SECTIONS = 'sections';

    /** @const Директория хранения файла экспорта */
    const SEO_DIRECTORY = 'seo';

    /** @const Имя файла экспорта   */
    const EXPORT_FILENAME = 'export.xlsx';

    /** @const Имя файла-образца для импорта */
    const IMPORT_BLANK_FILE = 'importBlank.xlsx';

    /**
     * Создаёт или запускает существующую задачу
     * @param int $iTask - id задачи. Если =0 - создается новая
     * @param array $aConfig - конфиг задачи
     * @return array - массив со статусом и id задачи
     * @throws \Exception
     */
    public static function runTask($iTask = 0, $aConfig = []){

        $oManager = Manager::getInstance();

        if (!$iTask)
            $iTask = \skewer\base\queue\Api::addTask($aConfig);


        $oTask = \skewer\base\queue\Api::getTaskById( $iTask );

        if (!$oTask)
            throw new \Exception(\Yii::t( 'import', 'error_run'));

        $oManager->executeTask( $oTask );

        return ['status' => $oTask->getStatus(), 'id' => $iTask];

    }


    /**
     * Вернёт массив сущностей. Формат [ тех.имя => название сущности ]
     * @return array
     */
    public static function getEntities(){
        return array(
          Seo::className()           => 'Раздел',
          Articles\Seo::className()  => 'Статья' ,
          News\Seo::className()      => 'Новость',
          Gallery\Seo::className()   => 'Альбом',
          FAQ\Seo::className()       => 'Вопрос-ответ' ,
          SeoGood::className()       => 'Товар',
          SeoGoodModifications::className() => 'Модификация'
        );
    }


    public static function getDataTypes(){
        $aDataTypes = [Api::DATATYPE_SECTIONS => 'Разделы'];
        $aDataTypes += Type::hasCatalogModule()? [Api::DATATYPE_GOODS => 'Каталог'] : [];
        return $aDataTypes;
    }

    /** Путь к файлу экспорта */
    public static function getSystemPathExportFile(){
        return '../private_files'. DIRECTORY_SEPARATOR . self::SEO_DIRECTORY . DIRECTORY_SEPARATOR . self::EXPORT_FILENAME;
    }

    /**
     * Путь к файлу экспорта для web интерфейса
     * @return string
     */
    public static function getWebPathExportFile(){
        return '/local/?ctrl=SeoGen&&mode=export&&fileName=' . self::EXPORT_FILENAME;
    }

    /**
     * Путь к файлу импорта для web интерфейса
     * @return string
     */
    public static function getWebPathImportBlankFile(){
        return '/local/?ctrl=SeoGen&&mode=import&&fileName=' . self::IMPORT_BLANK_FILE;
    }


}