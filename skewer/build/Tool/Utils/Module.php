<?php

namespace skewer\build\Tool\Utils;


use skewer\base\queue\Task;
use skewer\components\content_generator\Asset;
use skewer\components\search\SearchIndex;
use skewer\components\seo\Service;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use \skewer\build\Tool\Domains;
use skewer\components\ext;
use skewer\base\site\Site;
use skewer\base\SysVar;
use yii\base\Exception;

/**
 * Модуль c системными утилитами
 * Class Module
 * @package skewer\build\Tool\Utils
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit( $sText = '' ) {
        $this->render(new Tool\Utils\view\Init([
            'sText' => $sText
        ]));
    }

    protected function actionDropCache() {
        
        \Yii::$app->router->updateModificationDateSite();
        \Yii::$app->rebuildRegistry();
        \Yii::$app->rebuildCss();
        \Yii::$app->rebuildLang();
        \Yii::$app->clearParser();

        $this->actionInit( \Yii::t('cache', 'drop_cache_text') );

    }

    protected function actionLogs( $sText = '' ) {
        $this->render(new Tool\Utils\view\Logs([
            "sText" => $sText
        ]));
    }


    protected function actionViewAccess() {

        $sText = '';

        $sFile = ROOTPATH . 'log/access.log';
        if ( file_exists($sFile) ) {

            $arr = file( $sFile );

            $iStrCount = count($arr);
            if ( $iStrCount ) {
                $iFirstStr = ($iStrCount > 30) ? $iStrCount - 30 : 0;
                for ( $i = $iFirstStr; $i<$iStrCount; $i++ )
                    $sText .= $arr[ $i ] . '<br>';
            }
        }

        $this->actionLogs( $sText );
    }


    protected function actionViewError() {

        $sText = '';

        $sFile = ROOTPATH . 'log/error.log';
        if ( file_exists($sFile) ) {

            $arr = file( $sFile );

            $iStrCount = count($arr);
            if ( $iStrCount ) {
                $iFirstStr = ($iStrCount > 30) ? $iStrCount - 30 : 0;
                for ( $i = $iFirstStr; $i<$iStrCount; $i++ )
                    $sText .= $arr[ $i ] . '<br>';
            }
        }

        $this->actionLogs( $sText );
    }


    protected function actionClearLogs() {

        $sLogFile = ROOTPATH . '/log/access.log';
        $fh = fopen( $sLogFile, 'w' );
        fclose($fh);

        $sLogFile = ROOTPATH . '/log/error.log';
        $fh = fopen( $sLogFile, 'w' );
        fclose($fh);

        $this->actionLogs();
    }

    protected function actionSearch($sHeadText = '', $sText = ''){
        $this->render(new Tool\Utils\view\Search([
            "sHeadText" => $sHeadText,
            "sText" => $sText
        ]));
    }

    /**
     * "Пересобрать заново" - dropAll(), затем restoreAll()
     */
    protected function actionSearchDropAll(){

        Service::rebuildSearchIndex();
        $sText = \Yii::t('utils', 'search_drop_index');

        $this->actionSearch($sText);
    }

    /**
     * "Сбросить автивность записей" - сбросить флаг активности для всех записей
     */
    protected function actionResetActive(){

        $iAffectedRow =  SearchIndex::update()->set('status',0)->where('status',1)->get();
        $sText = \Yii::t('utils', 'record_update') . ": $iAffectedRow";

        $this->actionSearch($sText);
    }

    /**
     * "Переиндексировать неактивные" - запуск переиндексации для записей со статусом 0
     */
    protected function actionReindex(){

        $aData = $this->getInData();
        if ($this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $iTask = (isset($aData['idTask']))?$aData['idTask']:0;

        $iTask = Service::makeSearchIndex( $iTask );

        $aCollisions = Service::findUrlCollisions();
        if (!empty($aCollisions)){

            $aCollisionsUrls = [];

            foreach ($aCollisions as $item){
                $aCollisionsUrls[] = $item['href'];
            }

            throw new Exception(\Yii::t('utils', 'collision', ['collisions'=>implode("\r\n",$aCollisionsUrls)]));
        }

        $iRefreshCount = SysVar::get('Search.updated');
        $sText = \Yii::t('utils', 'record_update') . ": $iRefreshCount";

        if ( $iTask ){
            $this->addJSListener( 'search_reindex', 'reindex' );
            $this->fireJSEvent( 'search_reindex', ['idTask' => $iTask] );

        }

        $this->actionSearch($sText);
    }

    /**
     * Перестроим сайтмап
     */
    protected function actionRebuildSitemap(){

        $aData = $this->getInData();
        if ($this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $iTask = (isset($aData['idTask']))?$aData['idTask']:0;

        $aResult = Service::makeSiteMap( $iTask );
        if ( $aResult['id'] )
            $iTask = (int)$aResult['id'];

        if ($aResult['status'] != Task::stError) {

            $sUrl = Site::httpDomainSlash()."sitemap.xml";
            $sPath = WEBPATH."sitemap.xml";

            if ( file_exists($sPath) ) {
                $sText = htmlentities(file_get_contents($sPath));
                $sText = str_replace(' ', '&nbsp;', $sText);
                $sText = preg_replace('(http://.*\.xml)', '<a target="_blank" href="$0">$0</a>', $sText);
            } else {
                $sText = '';
            }

            /** @noinspection HtmlUnknownTarget */
            $msg = nl2br(sprintf(
                '%s

                <a target="_blank" href="%s">%s</a>

                %s',
                \Yii::t('utils', 'sitemap_update_msg'),
                $sUrl,
                $sUrl,
                $sText
            ));
        }
        else
            $msg = \Yii::t('utils', 'sitemap_update_error');

        if ( $iTask and $aResult['status'] == Task::stFrozen ){
            $this->addJSListener( 'rebuildSitemap', 'rebuildSitemap' );
            $this->fireJSEvent( 'rebuildSitemap', ['idTask' => $aResult['id']] );
        }

        $this->actionSearch($msg);
    }

    /** Оптимизация таблиц БД */
    protected function actionOptimizeDB() {

        // Оптимизация таблиц типа MyISAM делает так же дефрагментацию
        $oQuery = \Yii::$app->db->createCommand("SHOW TABLE STATUS WHERE engine IN ('MyISAM')")->query();
        while($sTableName = $oQuery->readColumn(0))
            \Yii::$app->db->createCommand("OPTIMIZE TABLE `$sTableName`")->execute();

        // Таблицы innoDB не нуждаются в оптимизации. Ниже делается только дефрагментация
        $oQuery = \Yii::$app->db->createCommand("SHOW TABLE STATUS WHERE engine IN ('innoDB')")->query();
        while($sTableName = $oQuery->readColumn(0))
            \Yii::$app->db->createCommand("ALTER TABLE `$sTableName` ENGINE = InnoDB")->execute();

        $this->actionInit( \Yii::t('utils', 'optimize_db_text') );
    }
}