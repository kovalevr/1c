<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 13.02.2017
 * Time: 11:54
 */

namespace skewer\build\Tool\Utils\view;

use skewer\components\ext\view\FormView;
use skewer\components\ext\docked\Api;

class Search extends FormView
{
    public $sHeadText;
    public $sText;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build()
    {
        $this->_form
            ->button('searchDropAll', \Yii::t('utils', 'SearchDropAll'), Api::iconReinstall)
            ->button('resetActive', \Yii::t('utils', 'resetActive'), Api::iconReload)
            ->button('reindex', \Yii::t('utils', 'reindex'), Api::iconReload)
            ->button('RebuildSitemap', \Yii::t('utils', 'rebuildSitemap'), Api::iconInstall)
            ->buttonBack();

        if ($this->sHeadText) $this->_form->headText($this->sHeadText);

        if ($this->sText) {
            $this->_form
                ->field('text', 'text', 'show', array('hideLabel' => 1))
                ->setValue(array('text' => $this->sText));
        }
    }
}