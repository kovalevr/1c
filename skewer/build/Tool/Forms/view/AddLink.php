<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 15:30
 */

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\FormView;

class AddLink extends FormView
{
    public $aFormFields;
    public $aCardFields;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->fieldSelect( 'form_field', \Yii::t('forms', 'form_field'), $this->aFormFields, [], false )
            ->fieldSelect( 'card_field', \Yii::t('forms', 'card_field'), $this->aCardFields, [], false )
            ->setValue( array() )
            ->buttonSave('saveLink')
            ->buttonCancel('LinkList')
        ;
    }
}