<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 11:36
 */

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\FormView;

class EditForm extends FormView
{
    public $bIsSystemMode;
    public $aFormTypeList;
    public $sHandlerLabel;
    public $sFormHandlerSubtext;
    public $bHasFormTarget;
    public $aYandexTarget;
    public $aGoogleTarget;
    public $oFormRow;
    public $iCurrentForm;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('form_id', 'id', 'hide')
            ->field('form_title', \Yii::t('forms', 'form_title'), 'string')
            ->fieldIf($this->bIsSystemMode, 'form_name', \Yii::t('forms', 'form_name'), 'string', [])
            ->fieldSelect( 'form_handler_type', \Yii::t('forms', 'form_handler_type'), $this->aFormTypeList, [], false )
            ->field(
                'form_handler_value', $this->sHandlerLabel, 'string', [
                    'subtext' => $this->sFormHandlerSubtext
                ]
            )
            ->field('form_captcha', \Yii::t('forms', 'form_captcha'), 'check', array('margin' => '15 5 0 0'));

        if ($this->bHasFormTarget) {
            $this->_form->fieldSelect( 'form_target', \Yii::t('forms', 'form_target'), $this->aYandexTarget )
                 ->fieldSelect( 'form_target_google', \Yii::t('forms', 'form_target_google'), $this->aGoogleTarget );
        }
        $this->_form
            ->field('form_redirect', \Yii::t('forms', 'form_redirect'), 'string', array(
                'subtext' => \Yii::t('forms', 'form_redirect_default')))
            ->field('form_send_crm', \Yii::t('forms', 'form_send_crm'), 'check');

        if ($this->bIsSystemMode) $this->_form->fieldString('form_template', \Yii::t('editor', 'template'));

        $this->_form->fieldCheck('form_show_required_fields', \Yii::t('forms', 'show_phrase_required_fields'))
            ->fieldCheck('form_show_header', \Yii::t('forms', 'form_show_header'))
            ->fieldCheck('replyTo', \Yii::t('forms', 'replyTo'));

        $sNameButton = $this->oFormRow->form_button;
        if ($sNameButton) {
            $aButton = explode('.',$sNameButton);
            $this->oFormRow->form_button = \Yii::t($aButton[0], $aButton[1]);
        }

        $this->_form
            ->fieldString('form_button', \Yii::t('forms', 'form_button'))
            ->fieldWysiwyg('form_succ_answer', \Yii::t('forms', 'form_succ_answer'), 300, '', ['margin' => '40 0 0 0'])
            ->setValue($this->oFormRow)
            ->buttonSave()
            ->buttonBack($this->iCurrentForm ? 'FieldList' : 'Init');

        if ($this->iCurrentForm && $this->bIsSystemMode) {
            $this->_form
                ->buttonSeparator( '->' )
                ->buttonDelete();
        }
    }
}